/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       07 Jun 2019     Rajshree Kakane
 *
 */

/**
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @returns {Void} Any output is written via response object
 */
function wocTransactionForm(request, response) {

    if (request.getMethod() == 'GET') {
        var workOrderId = request.getParameter('woid');
        if (workOrderId) {
            var wocRecord = nlapiTransformRecord('workorder', workOrderId, 'workordercompletion');
            if (wocRecord) {
                var createdfrom = wocRecord.getFieldValue('createdfrom');
                var routing = wocRecord.getFieldValue('manufacturingrouting');
                var assemblyItem = wocRecord.getFieldValue('item');
                var location = wocRecord.getFieldValue('location');
                var woQty = wocRecord.getFieldValue('orderquantity');
                var subsidiary = wocRecord.getFieldValue('subsidiary');
                var units = wocRecord.getFieldValue('units');

                var buildQuantity = nlapiLookupField('workorder', workOrderId, 'built');
                //create a work order completion form
                var form = nlapiCreateForm('Work Order Completion(Custom)');

                form.addFieldGroup("custpage_primaryinfo", "Primary Information");
                var assemblyItemfld = form.addField('custpage_assemblyitem', 'select', 'Assembly', 'item', 'custpage_primaryinfo').setDisplayType('inline').setDefaultValue(assemblyItem);
                var backflushfld = form.addField('custpage_backflush', 'checkbox', 'Backflush', null, 'custpage_primaryinfo').setDisplayType('inline').setDefaultValue('T');
                var woQtyfld = form.addField('custpage_woqty', 'float', 'Work Order Quantity', null, 'custpage_primaryinfo').setDisplayType('inline').setDefaultValue(woQty);
                var createdFromfld = form.addField('custpage_createdfrom', 'select', 'Created From', 'workorder', 'custpage_primaryinfo').setDisplayType('inline').setDefaultValue(createdfrom);
                var unitsfld = form.addField('custpage_units', 'select', 'Units', 'unitstype', 'custpage_primaryinfo').setDisplayType('inline').setDefaultValue(units);
                var routingFld = form.addField('custpage_routing', 'select', 'Manufacturing Routing', 'manufacturingrouting', 'custpage_primaryinfo').setDisplayType('inline').setDefaultValue(routing);

                form.addFieldGroup("custpage_classification", "Classification");
                var subsidiaryfld = form.addField('custpage_subsidiary', 'select', 'Subsidiary', 'subsidiary', 'custpage_classification').setDisplayType('inline').setDefaultValue(subsidiary);
                var locationfld = form.addField('custpage_location', 'select', 'Location', 'location', 'custpage_classification');
                if (location) {
                    locationfld.setDisplayType('inline').setDefaultValue(location);
                } else {
                    locationfld.setMandatory(true);
                }
                var transactionTypefld = form.addField('custpage_transaction', 'text', 'Transaction Type', null, 'custpage_classification').setDisplayType('hidden').setDefaultValue('Work Order Completion');
                //OVERBUILDS
                //adding a OVERBUILDS field to check the Allow Overage feature for work order completion
                var accountingInfo = nlapiLoadConfiguration('accountingpreferences');
                var overBuilds = accountingInfo.getFieldValue('overbuilds');
                nlapiLogExecution('DEBUG', 'overBuilds', overBuilds);
                var overbuildFld = form.addField('custpage_overbuilds', 'checkbox', 'OverBuilds', null, 'custpage_classification').setDisplayType('disabled');
                if (overBuilds)
                    overbuildFld.setDefaultValue(overBuilds);
                var buildQtyfld = form.addField('custpage_buildqty', 'float', 'Build Quantity', null, 'custpage_classification').setDisplayType('inline').setDefaultValue(buildQuantity);

                form.addFieldGroup("custpage_operations", "Operation Completion");
                var startOperationfld = form.addField('custpage_startoperation', 'select', 'Starting Operation', null, 'custpage_operations').setMandatory(true);
                var endOperationfld = form.addField('custpage_endoperation', 'select', 'Ending Operation', null, 'custpage_operations').setMandatory(true);
                var hiddenEndOper = form.addField('custpage_hiddenendop', 'text', 'Hidden End Op', null, 'custpage_operations').setDisplayType('hidden');
                var hiddenRoutingSequence = form.addField('custpage_hiddenroutingseq', 'text', 'Hidden Routing Seq', null, 'custpage_operations').setDisplayType('hidden');

                var routingSequenceString = '';
                if (routing) {
                    startOperationfld.addSelectOption('', '', true);
                    endOperationfld.addSelectOption('', '', true);
                    var manufactroutingrec = nlapiLoadRecord('manufacturingrouting', routing);
                    if (manufactroutingrec != null && manufactroutingrec != undefined && manufactroutingrec != '') {
                        var rountingcount = manufactroutingrec.getLineItemCount('routingstep');
                        for (var count = 1; count <= rountingcount; count++) {
                            var rounOper = manufactroutingrec.getLineItemValue('routingstep', 'operationsequence', count);
                            nlapiLogExecution('DEBUG', 'rounOper', rounOper);
                            startOperationfld.addSelectOption(rounOper, rounOper);
                            endOperationfld.addSelectOption(rounOper, rounOper);

                            if (count == rountingcount) {
                                hiddenEndOper.setDefaultValue(rounOper);
                                routingSequenceString = routingSequenceString + '' + rounOper;
                            } else {
                                routingSequenceString = routingSequenceString + '' + rounOper + '|';
                            }
                        }
                    }
                    hiddenRoutingSequence.setDefaultValue(routingSequenceString);
                }
                var completedQtyfld = form.addField('custpage_compltqty', 'float', 'Completed Quantity', null, 'custpage_operations').setMandatory(true);
                completedQtyfld.setDisplayType('disabled');
                form.addFieldGroup("custpage_assemblydetail", "Assembly Detail");
                var inventoryDetailfld = form.addField('custpage_inventorydetail', 'checkbox', 'Inventory Detail', null, 'custpage_assemblydetail').setDisplayType('disabled');
                var scrapQuantityfld = form.addField('custpage_scrapqty', 'float', 'Scrap Quantity', null, 'custpage_assemblydetail').setDisplayType('disabled');

                form.addTab('custpage_tab', 'LOT Information');
                form.addSubTab('custpage_component_subtab', 'Components', 'custpage_tab');
                form.addSubTab('custpage_custom_subtab', 'Custom', 'custpage_tab');
                var lotDetailsMapfld = form.addField('custpage_lotdetailsmap', 'textarea', 'Finish LOT Details', null, 'custpage_custom_subtab').setDisplayType('entry').setDisplayType('disabled').setDisplaySize(10, 1);
                lotDetailsMapfld.setMaxLength(1000000);
                var originalQuantity = form.addField('custpage_originallotqty', 'float', 'Original Quantity', null, 'custpage_custom_subtab').setDisplayType('entry').setDisplayType('disabled');
                var componentCountryFld = form.addField('custpage_countrymap', 'textarea', 'Component Country', null, 'custpage_custom_subtab').setDisplayType('entry').setDisplayType('disabled').setDisplaySize(10, 1);
                componentCountryFld.setMaxLength(1000000);
                var componentExpiryFld = form.addField('custpage_expirationmap', 'textarea', 'Component Expiry', null, 'custpage_custom_subtab').setDisplayType('entry').setDisplayType('disabled').setDisplaySize(10, 1);
                componentCountryFld.setMaxLength(1000000);

                var componentlist = form.addSubList("custpage_componentlist", "list", "Components", 'custpage_component_subtab');
                componentlist.addField("custpage_component", "select", "Component", 'item').setDisplayType('inline');
                componentlist.addField("custpage_operation", "text", "Operation").setDisplayType('inline');
                componentlist.addField("custpage_quantityper", "text", "Quantity Per").setDisplayType('inline');
                componentlist.addField("custpage_quantity", "float", "Quantity").setMandatory(true).setDisplayType('entry');
                var componentDetail = componentlist.addField("custpage_compo_lotassignment", "checkbox", "Inventory Detail").setDisplayType('entry');
                componentlist.addField("custpage_complotdetails", "textarea", "LOT Details").setDisplayType('entry').setDisplayType('disabled').setDisplaySize(10, 1);
                componentlist.addField("custpage_islotnumbered", "checkbox", "Is LotNumbered?").setDisplayType('entry').setDisplayType('hidden');
                componentlist.addField("custpage_originalselectedqty", "float", "Original Qty").setDisplayType('entry').setDisplayType('hidden');

                var woComponentLineCount = wocRecord.getLineItemCount('component');
                var itemArray = [];
                for (var lineCnt = 1; lineCnt <= woComponentLineCount; lineCnt++) {
                    itemArray.push(wocRecord.getLineItemValue('component', 'item', lineCnt));
                    componentlist.setLineItemValue('custpage_component', lineCnt, wocRecord.getLineItemValue('component', 'item', lineCnt));
                    componentlist.setLineItemValue('custpage_operation', lineCnt, wocRecord.getLineItemValue('component', 'operationsequencenumber', lineCnt));
                    componentlist.setLineItemValue('custpage_quantityper', lineCnt, wocRecord.getLineItemValue('component', 'quantityper', lineCnt));
                }
                var lotItemIdMap = checkLotNumberedItem(itemArray);
                if (Object.keys(lotItemIdMap).length > 0) {
                    for (var cnt = 1; cnt <= woComponentLineCount; cnt++) {
                        var compoItemId = wocRecord.getLineItemValue('component', 'item', cnt);
                        if (lotItemIdMap[compoItemId]) {
                            componentlist.setLineItemValue('custpage_islotnumbered', cnt, 'T');
                        }
                    }
                }
                form.addSubmitButton("Submit");
                form.addResetButton("Reset");
                form.setScript('customscript_cntm_woc_popup_validation')
                form.addButton('custpage_backbutton', 'Back', 'history.back()');
                response.writePage(form);
            }

        }
    } else if (request.getMethod() == 'POST') {
        try {
            var woId = request.getParameter('custpage_createdfrom');
            if (woId) {
                var wocompletionRecord = nlapiTransformRecord('workorder', woId, 'workordercompletion');
                if (wocompletionRecord) {
                    var startOperation = request.getParameter('custpage_startoperation');
                    var lastOperation = request.getParameter('custpage_endoperation');
                    var completedQuantity = request.getParameter('custpage_compltqty');
                    var location = request.getParameter('custpage_location');
                    nlapiLogExecution('DEBUG', 'completedQuantity', completedQuantity);
                    var routingEndOp = request.getParameter('custpage_hiddenendop');
                    var scrapQuantity = request.getParameter('custpage_scrapqty');
                    wocompletionRecord.setFieldText('startoperation', startOperation);
                    wocompletionRecord.setFieldText('endoperation', lastOperation);
                    wocompletionRecord.setFieldValue('completedquantity', completedQuantity);
                    wocompletionRecord.setFieldValue('custbody_cntm_original_complte_qty', completedQuantity);

                    wocompletionRecord.setFieldValue('custbody_cntm_complted_qty', completedQuantity);
                    if (scrapQuantity) {
                        wocompletionRecord.setFieldValue('scrapquantity', scrapQuantity);
                    }
                    nlapiLogExecution('DEBUG', 'routingEndOp', routingEndOp);
                    if (parseInt(lastOperation) == parseInt(routingEndOp)) {
                        wocompletionRecord.setFieldValue('quantity', completedQuantity);
                    }

                    wocompletionRecord.setFieldValue('isbackflush', 'T');
                    wocompletionRecord.setFieldValue('location', location);
                    //add or update Finished Inventory Detail
                    var inventoryLOTDetails = request.getParameter('custpage_lotdetailsmap');
                    if (inventoryLOTDetails)
                        inventoryLOTDetails = (inventoryLOTDetails.length == 1) || (inventoryLOTDetails.length == 0) ? '' : inventoryLOTDetails;
                    wocompletionRecord.setFieldValue('custbody_cntm_inventorylot_mapdetail', inventoryLOTDetails);
                    if (inventoryLOTDetails) {
                        inventoryLOTDetails = JSON.parse(inventoryLOTDetails);
                        wocompletionRecord.setFieldValue('custbody_cntm_lotassignment', 'T');
                        if (Object.keys(inventoryLOTDetails).length > 0) {
                            var subrecord1 = wocompletionRecord.editSubrecord('inventorydetail');
                            if (subrecord1) {
                                var sub1LineCnt = subrecord1.getLineItemCount('inventoryassignment');
                                for (var sub1Cnt = sub1LineCnt; sub1Cnt >= 1; --sub1Cnt) {
                                    subrecord1.removeLineItem('inventoryassignment', sub1Cnt);
                                }
                                for (var lotNum in inventoryLOTDetails) {
                                    nlapiLogExecution('DEBUG', 'subrecord1+lotNum', lotNum);
                                    if (lotNum != null) {
                                        var lotnumberArray = lotNum.split('|');
                                        var maparray = [];
                                        maparray = inventoryLOTDetails[lotNum].split('__');
                                        subrecord1.selectNewLineItem('inventoryassignment');
                                        if (lotnumberArray[0])
                                            subrecord1.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', lotnumberArray[0]);
                                        if (maparray[2])
                                            subrecord1.setCurrentLineItemValue('inventoryassignment', 'quantity', parseFloat(maparray[2]));
                                        if (maparray[0])
                                            subrecord1.setCurrentLineItemValue('inventoryassignment', 'binnumber', maparray[0]);
                                        if (maparray[10])
                                            subrecord1.setCurrentLineItemValue('inventoryassignment', 'expirationdate', maparray[10]);
                                        if (maparray[1])
                                            subrecord1.setCurrentLineItemValue('inventoryassignment', 'inventorystatus', maparray[1]);
                                        subrecord1.commitLineItem('inventoryassignment');
                                        nlapiLogExecution('DEBUG', 'commit', 'commit');
                                    }
                                }
                                subrecord1.commit();
                            } else {
                                var subrecord2 = wocompletionRecord.createSubrecord('inventorydetail');
                                if (subrecord2) {
                                    for (var lotNum in inventoryLOTDetails) {
                                        nlapiLogExecution('DEBUG', 'subrecord2+lotNum', lotNum);
                                        if (lotNum != null) {
                                            var lotnumberArray = lotNum.split('|');

                                            var maparray = [];
                                            maparray = inventoryLOTDetails[lotNum].split('__');
                                            subrecord2.selectNewLineItem('inventoryassignment');
                                            if (lotnumberArray[0])
                                                subrecord2.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', lotnumberArray[0]);
                                            if (maparray[2])
                                                subrecord2.setCurrentLineItemValue('inventoryassignment', 'quantity', parseFloat(maparray[2]));
                                            if (maparray[0])
                                                subrecord2.setCurrentLineItemValue('inventoryassignment', 'binnumber', maparray[0]);
                                            if (maparray[10])
                                                subrecord2.setCurrentLineItemValue('inventoryassignment', 'expirationdate', maparray[10]);
                                            if (maparray[1])
                                                subrecord2.setCurrentLineItemValue('inventoryassignment', 'inventorystatus', maparray[1]);
                                            subrecord2.commitLineItem('inventoryassignment');
                                            nlapiLogExecution('DEBUG', '4', '4');
                                        }
                                    }
                                    subrecord2.commit();
                                }
                            }
                        }

                        //updateFinishInventoryDetails(wocompletionRecord,inventoryLOTDetails);					
                    }

                    //add/update component Inventory Details

                    var componentListCount = request.getLineItemCount('custpage_componentlist');
                    nlapiLogExecution('DEBUG', 'before componentListCount', componentListCount);
                    if (componentListCount) {
                        nlapiLogExecution('DEBUG', 'after componentListCount', componentListCount);
                        for (var compoCount = 1; compoCount <= componentListCount; compoCount++) {
                            var compoQuantity = request.getLineItemValue('custpage_componentlist', 'custpage_quantity', compoCount);
                            nlapiLogExecution('DEBUG', 'compoQuantity', compoQuantity);
                            if (compoQuantity) {
                                wocompletionRecord.setLineItemValue('component', 'quantity', compoCount, compoQuantity);
                                wocompletionRecord.selectLineItem('component', compoCount);
                                var compoLOTDetails = request.getLineItemValue('custpage_componentlist', 'custpage_complotdetails', compoCount);
                                if (compoLOTDetails)
                                    compoLOTDetails = (compoLOTDetails.length == 1) || (compoLOTDetails.length == 0) ? '' : compoLOTDetails;

                                if (compoLOTDetails) {
                                    compoLOTDetails = JSON.parse(compoLOTDetails);
                                    nlapiLogExecution('DEBUG', 'compoLOTDetails', compoLOTDetails);
                                    if (Object.keys(compoLOTDetails).length > 0) {
                                        var compoSubRecord1 = wocompletionRecord.editCurrentLineItemSubrecord('component', 'componentinventorydetail');
                                        if (compoSubRecord1) {

                                            var composubLineCnt = compoSubRecord1.getLineItemCount('inventoryassignment');
                                            for (var compsubCnt = composubLineCnt; compsubCnt >= 1; --compsubCnt) {
                                                compoSubRecord1.removeLineItem('inventoryassignment', compsubCnt);
                                            }
                                            for (var lotNum in compoLOTDetails) {
                                                var binDetails = lotNum.split('__');
                                                nlapiLogExecution('DEBUG', 'compoSubRecord1+lotNum', lotNum);
                                                if (lotNum && binDetails[0] != 'Bin') {
                                                    var quantityonhand = parseFloat(compoLOTDetails[lotNum]);
                                                    compoSubRecord1.selectNewLineItem('inventoryassignment');
                                                    compoSubRecord1.setCurrentLineItemValue('inventoryassignment', 'issueinventorynumber', lotNum);
                                                    compoSubRecord1.setCurrentLineItemValue('inventoryassignment', 'quantity', quantityonhand);
                                                    compoSubRecord1.commitLineItem('inventoryassignment');
                                                    nlapiLogExecution('DEBUG', 'commit', 'commit');
                                                } else if (lotNum && binDetails[0] == 'Bin') {
                                                    var quantityonhand = parseFloat(compoLOTDetails[lotNum]);
                                                    compoSubRecord1.selectNewLineItem('inventoryassignment');
                                                    compoSubRecord1.setCurrentLineItemValue('inventoryassignment', 'binnumber', binDetails[1]);
                                                    compoSubRecord1.setCurrentLineItemValue('inventoryassignment', 'quantity', quantityonhand);
                                                    compoSubRecord1.commitLineItem('inventoryassignment');
                                                    nlapiLogExecution('DEBUG', 'commit', 'commit');
                                                }
                                            }
                                            compoSubRecord1.commit();

                                        } else {
                                            var compoSubRecord2 = wocompletionRecord.createCurrentLineItemSubrecord('component', 'componentinventorydetail');
                                            if (compoSubRecord2) {
                                                for (var lotNum in compoLOTDetails) {
                                                    var binDetails = lotNum.split('__');
                                                    nlapiLogExecution('DEBUG', 'compoSubRecord2+lotNum', lotNum);
                                                    if (lotNum && binDetails[0] != 'Bin') {
                                                        var quantityonhand = parseFloat(compoLOTDetails[lotNum]);
                                                        compoSubRecord2.selectNewLineItem('inventoryassignment');
                                                        compoSubRecord2.setCurrentLineItemValue('inventoryassignment', 'issueinventorynumber', lotNum);
                                                        compoSubRecord2.setCurrentLineItemValue('inventoryassignment', 'quantity', quantityonhand);
                                                        compoSubRecord2.commitLineItem('inventoryassignment');
                                                        nlapiLogExecution('DEBUG', 'commit', 'commit');
                                                    } else if (lotNum && binDetails[0] == 'Bin') {
                                                        var quantityonhand = parseFloat(compoLOTDetails[lotNum]);
                                                        compoSubRecord2.selectNewLineItem('inventoryassignment');
                                                        compoSubRecord2.setCurrentLineItemValue('inventoryassignment', 'binnumber', binDetails[1]);
                                                        compoSubRecord2.setCurrentLineItemValue('inventoryassignment', 'quantity', quantityonhand);
                                                        compoSubRecord2.commitLineItem('inventoryassignment');
                                                        nlapiLogExecution('DEBUG', 'commit', 'commit');
                                                    }
                                                }
                                                compoSubRecord2.commit();
                                            }
                                        }
                                    }

                                    //updateComponentDetails(wocompletionRecord,compoLOTDetails);						
                                }
                                wocompletionRecord.commitLineItem('component');
                            } else {
                                wocompletionRecord.setLineItemValue('component', 'quantity', compoCount, '0');
                                wocompletionRecord.commitLineItem('component');
                            }
                        }
                    }

                }
                var wocReturnID = nlapiSubmitRecord(wocompletionRecord, false, true);
                nlapiSetRedirectURL('RECORD', 'workordercompletion', wocReturnID);

            }
        } catch (e) {
            var error = nlapiCreateError('121', e.message, true);
            throw error;
            nlapiLogExecution('ERROR', 'e.message', e.message);
        }

    }
}

function updateFinishInventoryDetails(wocompletionRecord, inventoryLOTDetails) {
    if (Object.keys(inventoryLOTDetails).length > 0) {
        var subrecord1 = wocompletionRecord.editSubrecord('inventorydetail');
        if (subrecord1) {
            var sub1LineCnt = subrecord1.getLineItemCount('inventoryassignment');
            for (var sub1Cnt = sub1LineCnt; sub1Cnt >= 1; --sub1Cnt) {
                subrecord1.removeLineItem('inventoryassignment', sub1Cnt);
            }
            for (var lotNum in inventoryLOTDetails) {
                nlapiLogExecution('DEBUG', 'lotNum', lotNum);
                if (lotNum != null) {
                    var maparray = [];
                    maparray = inventoryLOTDetails[lotNum].split('__');
                    subrecord1.selectNewLineItem('inventoryassignment');
                    subrecord1.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', lotNum);
                    subrecord1.setCurrentLineItemValue('inventoryassignment', 'quantity', parseFloat(maparray[2]));
                    subrecord1.setCurrentLineItemValue('inventoryassignment', 'binnumber', maparray[0]);
                    subrecord1.setCurrentLineItemValue('inventoryassignment', 'expirationdate', maparray[10]);
                    subrecord1.setCurrentLineItemValue('inventoryassignment', 'inventorystatus', maparray[1]);
                    subrecord1.commitLineItem('inventoryassignment');
                    nlapiLogExecution('DEBUG', 'commit', 'commit');
                }
            }
            subrecord1.commit();
        } else {
            var subrecord2 = wocompletionRecord.createSubrecord('inventorydetail');
            if (subrecord2) {
                for (var lotNum in inventoryLOTDetails) {
                    nlapiLogExecution('DEBUG', 'lotNum', lotNum);
                    if (lotNum != null) {

                        var maparray = [];
                        maparray = inventoryLOTDetails[lotNum].split('__');
                        subrecord2.selectNewLineItem('inventoryassignment');
                        subrecord2.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', lotNum);
                        subrecord2.setCurrentLineItemValue('inventoryassignment', 'quantity', parseFloat(maparray[2]));
                        subrecord2.setCurrentLineItemValue('inventoryassignment', 'binnumber', maparray[0]);
                        subrecord2.setCurrentLineItemValue('inventoryassignment', 'expirationdate', maparray[10]);
                        subrecord2.setCurrentLineItemValue('inventoryassignment', 'inventorystatus', maparray[1]);
                        subrecord2.commitLineItem('inventoryassignment');
                    }
                }
                subrecord2.commit();
            }
        }
    }
}

function updateComponentDetails(wocompletionRecord, compoLOTDetails) {
    if (Object.keys(compoLOTDetails).length > 0) {
        var compoSubRecord1 = wocompletionRecord.editCurrentLineItemSubrecord('component', 'componentinventorydetail');
        if (compoSubRecord1) {

            var composubLineCnt = compoSubRecord1.getLineItemCount('inventoryassignment');
            for (var compsubCnt = composubLineCnt; compsubCnt >= 1; --compsubCnt) {
                compoSubRecord1.removeLineItem('inventoryassignment', compsubCnt);
            }
            for (var lotNum in compoLOTDetails) {
                nlapiLogExecution('DEBUG', 'compoSubRecord1+lotNum', lotNum);
                if (lotNum != null) {
                    var quantityonhand = parseFloat(compoLOTDetails[lotNum]);
                    compoSubRecord1.selectNewLineItem('inventoryassignment');
                    compoSubRecord1.setCurrentLineItemValue('inventoryassignment', 'issueinventorynumber', lotNum);
                    compoSubRecord1.setCurrentLineItemValue('inventoryassignment', 'quantity', quantityonhand);
                    compoSubRecord1.commitLineItem('inventoryassignment');
                    nlapiLogExecution('DEBUG', 'commit', 'commit');
                }
            }
            compoSubRecord1.commit();

        } else {
            var compoSubRecord2 = wocompletionRecord.createCurrentLineItemSubrecord('component', 'componentinventorydetail');
            if (compoSubRecord2) {
                for (var lotNum in compoLOTDetails) {
                    nlapiLogExecution('DEBUG', 'compoSubRecord2+lotNum', lotNum);
                    if (lotNum != null) {
                        var quantityonhand = parseFloat(compoLOTDetails[lotNum]);
                        compoSubRecord1.selectNewLineItem('inventoryassignment');
                        compoSubRecord1.setCurrentLineItemValue('inventoryassignment', 'issueinventorynumber', lotNum);
                        compoSubRecord1.setCurrentLineItemValue('inventoryassignment', 'quantity', quantityonhand);
                        compoSubRecord1.commitLineItem('inventoryassignment');
                        nlapiLogExecution('DEBUG', 'commit', 'commit');
                    }
                }
                compoSubRecord2.commit();
            }
        }
    }
}

function checkLotNumberedItem(itemArray) {
    var lotNumberedItemMap = {};
    var islotitemSearch = nlapiSearchRecord("item", null,
        [
            [
                [
                    ["islotitem", "is", "T"]
                ], "OR", [
                    ["usebins", "is", "T"]
                ]
            ],
            "AND",
            [
                ["internalid", "anyof", itemArray]
            ]
        ],
        [
            new nlobjSearchColumn("itemid").setSort(false)
        ]
    );
    if (islotitemSearch) {
        for (var searchCount = 0; searchCount < islotitemSearch.length; searchCount++) {
            var itemId = islotitemSearch[searchCount].getId();
            lotNumberedItemMap[itemId] = itemId;
        }
    }
    return lotNumberedItemMap;
}