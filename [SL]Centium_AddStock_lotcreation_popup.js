/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       28 May 2019     Rajshree Kakane
 *
 */

/**
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @returns {Void} Any output is written via response object
 */
function lotcreationpopup(request, response) {

    if (request.getMethod() == 'GET') {

        var form = nlapiCreateForm("Inventory Details", true);
        form.setScript('customscript_cntm_woc_popup_validation');

        var itemname = request.getParameter('itemname');
        nlapiLogExecution('DEBUG', 'itemname', itemname);

        var orderedqty = parseFloat(request.getParameter('quantity'));
        nlapiLogExecution('DEBUG', 'orderedqty', orderedqty);
        var lotNumberDetailMap = request.getParameter('description');
        var vendorId = request.getParameter('venid');
        var vendorCode = request.getParameter('vencode');
        nlapiLogExecution('DEBUG', 'vendorCode', vendorCode);
        var location = request.getParameter('loc');
        nlapiLogExecution('DEBUG', 'location', location);
        nlapiLogExecution('DEBUG', 'lotNumberDetailMap', lotNumberDetailMap);

        var country = request.getParameter('origin');
        var expiryDateStd = request.getParameter('expstd');
        var expiryDateMR = request.getParameter('expmr');
        var wocTransaction = request.getParameter('woc');
        nlapiLogExecution('DEBUG', 'wocTransaction', wocTransaction);

        //check Country of origin on the vendor record and add vendor code to the manufacturer List
        var manufacturerId = checkVendorCodeList(vendorId, vendorCode);
        var countryOriginId;
        if (vendorId) {
            countryOriginId = nlapiLookupField('vendor', vendorId, 'custentity_cnt_country_of_origin');
            nlapiLogExecution('DEBUG', 'countryOriginId', countryOriginId);
        }
        var descriptionfield = form.addField('custpage_description', 'longtext', 'description');
        descriptionfield.setDisplayType('hidden');

        var itemnamefield = form.addField('custpage_itemname', 'select', 'Item', 'item');
        itemnamefield.setDefaultValue(itemname);
        itemnamefield.setDisplayType('inline');
        itemnamefield.setLayoutType('startrow', 'startcol');

        var itemDesc = nlapiLookupField('item', itemname, 'description');
        var itemdescfield = form.addField('custpage_itemdesc', 'text', 'Description');
        itemdescfield.setDefaultValue(itemDesc);
        itemdescfield.setDisplayType('inline');
        itemdescfield.setLayoutType('startrow', 'startcol');

        var salesunit = request.getParameter('salesunit');
        var unittxt = request.getParameter('unit');
        if (unittxt) {
            var stockunitfield = form.addField('custpage_stockunit', 'text', 'Units');
            stockunitfield.setDefaultValue(unittxt);
            stockunitfield.setDisplayType('inline');
            stockunitfield.setLayoutType('startrow', 'startcol');
        } else {
            if (itemname != null && itemname != undefined && itemname != '') {
                var stockunit = nlapiLookupField('item', itemname, 'stockunit', true);
                var stockunit_field = form.addField('custpage_stockunit', 'text', 'Units');
                stockunit_field.setDefaultValue(stockunit);
                stockunit_field.setDisplayType('inline');
                stockunit_field.setLayoutType('startrow', 'startcol');
            }
        }
        var qtyfield = form.addField('custpage_quantity', 'float', 'Quantity');
        //        Commented to allow the user to add decimal values
        //        if(parseFloat(orderedqty)%1==0)
        //        	qtyfield.setDefaultValue(parseFloat(orderedqty).toFixed(0));
        //        else
        //        	qtyfield.setDefaultValue(orderedqty);
        qtyfield.setDefaultValue(orderedqty);
        qtyfield.setDisplayType('inline');
        qtyfield.setLayoutType('startrow', 'startcol');
        var hiddenvalue = 1;
        var hiddenfield = form.addField('custpage_hiddencheckfld', 'text', 'Hidden field');
        hiddenfield.setDisplayType('hidden');

        if (wocTransaction) {
            var wocfield = form.addField('custpage_wocfld', 'text', 'WOC');
            wocfield.setDisplayType('hidden');
            wocfield.setDefaultValue(wocTransaction);
        }
        var isLOTNumbered;
        var lotNumberItemSearch = nlapiSearchRecord("item", null,
            [
                ["islotitem", "is", "T"],
                "AND",
                ["internalid", "anyof", itemname]
            ],
            [
                new nlobjSearchColumn("islotitem")
            ]);
        if (lotNumberItemSearch) {
            isLOTNumbered = lotNumberItemSearch[0].getValue('islotitem');
        } else {
            isLOTNumbered = 'F';
        }

        var subList = form.addSubList("custpage_lotnumberitem", "inlineeditor", "Inventory Detail");

        //custom field to store the total selected quantity
        /*   var totalSelectedQty = form.addField('custpage_selectedqty', 'float', 'Selected Quantity');
             totalSelectedQty.setDefaultValue(0);
             totalSelectedQty.setDisplayType('inline');
             totalSelectedQty.setLayoutType('startrow', 'startcol');
           
           var initialLOTQty = form.addField('custpage_initiallotqty', 'float', 'Initial LOT Qty');
             initialLOTQty.setDefaultValue(0);
             initialLOTQty.setDisplayType('hidden');
             initialLOTQty.setLayoutType('startrow', 'startcol');*/
        /* //custom field to store the total remaining quantity
         var totalRemainingQty = form.addField('custpage_totalremainingedqty', 'float', 'Total Remaining Qty');
         totalRemainingQty.setDefaultValue(orderedqty);*/

        //check the usebins feature on item record.
        var useBinFlag;
        if (itemname) {
            useBinFlag = nlapiLookupField('item', itemname, 'usebins');
        }
        var number_col = subList.addField("custpage_inventorynumber", "text", "Serial/LOT Number");
        if (isLOTNumbered == 'T') {
            number_col.setDisplayType('entry').setMandatory(true);
        } else if (isLOTNumbered == 'F') {
            number_col.setDisplayType('disabled');
        }

        var bin_col = subList.addField("custpage_bin", "select", "Bin").setDisplayType('entry');
        bin_col.addSelectOption('', '', true);
        var binNumberSearch = nlapiSearchRecord("bin", null,
            [
                ["location", "anyof", location]
            ],
            [
                new nlobjSearchColumn("binnumber").setSort(false),
                new nlobjSearchColumn("location"),
                new nlobjSearchColumn("memo")
            ]
        );
        if (binNumberSearch) {
            for (var binCnt = 0; binCnt < binNumberSearch.length; binCnt++) {
                bin_col.addSelectOption(binNumberSearch[binCnt].getId(), binNumberSearch[binCnt].getValue('binnumber'));
            }
        }
        if (useBinFlag == 'T')
            bin_col.setDisplayType('entry').setMandatory(true);
        else {
            bin_col.setDisplayType('disabled');
        }
        var status_col = subList.addField("custpage_status", "select", "Inventory Status", "inventorystatus").setDisplayType('entry')
        status_col.setMandatory(true);
        status_col.setDefaultValue('1');
        var qty_col = subList.addField("custpage_quantity", "float", "Quantity").setDisplayType('entry').setMandatory(true);
        var cureDate_col = subList.addField("custpage_curedate", "text", "Cure Date");
        cureDate_col.setMaxLength(4);

        var expDateMR_col = subList.addField("custpage_expdatemr", "text", "Expiration Date(MR)").setDisplayType('entry').setDisplayType('disabled');
        var expDate_col = subList.addField("custpage_expdate", "date", "Expiration Date").setDisplayType('entry').setDisplayType('disabled');
        var memo_col = subList.addField("custpage_memo", "text", "Memo").setDisplayType('entry');
        var countryOrigin_col = subList.addField("custpage_country", "select", "Country of Origin");


        countryOrigin_col.addSelectOption('', '', true);
        //load the custom country record
        var record = nlapiLoadRecord('customrecord_country_list_record', '1');
        var countryField = record.getField('custrecord_country_list');
        var countryOptions = countryField.getSelectOptions();

        for (key in countryOptions) {
            var countryId = countryOptions[key].getId(); //stored ID for that iteration at your disposal
            var countryText = countryOptions[key].getText(); //stored text for that iteration at your disposal
            //nlapiLogExecution('DEBUG', countryId, countryText);
            if (country) {
                if (countryText == country) {
                    nlapiLogExecution('DEBUG', country, countryText);
                    country = countryId;
                }
            }

            countryOrigin_col.addSelectOption(countryId, countryText);
        }

        if (wocTransaction) {
            if (country)
                countryOrigin_col.setDefaultValue(country);
            if (expiryDateMR)
                expDateMR_col.setDefaultValue(expiryDateMR);
            if (expiryDateStd)
                expDate_col.setDefaultValue(expiryDateStd);

        }
        var manufacturer_col = subList.addField("custpage_manufacturer", "select", "Manufacturer", 'customlist_cnt_manufacturers').setDisplayType('entry').setMandatory(false);

        var vendor_col = subList.addField("custpage_vendor", "select", "Vendor", 'vendor').setDisplayType('entry');

        var vendorItemNum_col = subList.addField("custpage_vendornumber", "text", "Vendor Item number").setDisplayType('entry').setMandatory(false);

        var mfgItemNum_col = subList.addField("custpage_mfgitemnum", "text", "Manufacturer Item Number").setDisplayType('entry').setMandatory(false);


        if (isLOTNumbered == 'T') {

            if (!wocTransaction) {
                cureDate_col.setDisplayType('entry');
              cureDate_col.setMandatory(true);
                countryOrigin_col.setDisplayType('entry');
              countryOrigin_col.setMandatory(true);
                vendor_col.setMandatory(true);
                if (manufacturerId)
                    manufacturer_col.setDefaultValue(manufacturerId);
                if (vendorId)
                    vendor_col.setDefaultValue(vendorId);
                if (vendorCode) {
                    vendorItemNum_col.setDefaultValue(vendorCode);
                    mfgItemNum_col.setDefaultValue(vendorCode);
                }
                if (countryOriginId) {
                    countryOrigin_col.setDefaultValue(countryOriginId);
                }

            } else {
                countryOrigin_col.setDisplayType('disabled');
                cureDate_col.setDisplayType('disabled');
             
            }

        } else if (isLOTNumbered == 'F') {
            cureDate_col.setDisplayType('disabled');
            countryOrigin_col.setDisplayType('disabled');
            manufacturer_col.setDisplayType('disabled');
            vendor_col.setDisplayType('disabled');
            vendorItemNum_col.setDisplayType('disabled');
            mfgItemNum_col.setDisplayType('disabled');
        }
        form.addSubmitButton("OK");
        form.addButton('custpage_closebtn', 'Close', 'closewindow()');
        response.writePage(form);

    }
}

function checkVendorCodeList(vendorId, vendorCode) {
    if (vendorId) {
        try {
            var countryOfOrigin = nlapiLookupField('vendor', vendorId, 'custentity_cnt_country_of_origin');
            var vendorName = nlapiLookupField('vendor', vendorId, 'entityid');
            if (countryOfOrigin) {
                //check Vendor Name exist in Manufacturer List
                var manufactureListId = 290; //internal Id of manufacturer custom List
                if (vendorName) {
                    var searchVendorName = nlapiSearchRecord('customlist_cnt_manufacturers', null,
                        [
                            ["name", "is", vendorName]
                        ],
                        [
                            new nlobjSearchColumn("name"),
                            new nlobjSearchColumn("internalid")
                        ]);
                    if (!searchVendorName) {
                        //add vendor code to Manufacturer List
                        var manufacturerList = nlapiCreateRecord('customlist_cnt_manufacturers');
                        if (manufacturerList) {
                            manufacturerList.setFieldValue('name', vendorName);
                            var returnmfgListId = nlapiSubmitRecord(manufacturerList, false, true);
                            nlapiLogExecution('DEBUG', 'returnmfgListId', returnmfgListId);
                            return returnmfgListId;
                        }
                    } else {
                        var manfacturerId = searchVendorName[0].getId();
                        nlapiLogExecution('DEBUG', 'manfacturerId', manfacturerId);
                        return manfacturerId;

                    }
                }
            }
        } catch (e) {
            nlapiLogExecution('DEBUG', 'e.message', e.message);
        }

    }
}