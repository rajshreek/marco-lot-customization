/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       28 May 2019     Rajshree Kakane
 *
 */

/**
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @returns {Void} Any output is written via response object
 */
function itemReceiptCustomPage(request, response) {

    if (request.getMethod() == 'GET') {

        var purchaseorder_id = request.getParameter('poid');
        //nlapiLogExecution('DEBUG', 'purchaseorder_id', purchaseorder_id);
        if (purchaseorder_id) {

            var item_receipt = nlapiTransformRecord('purchaseorder', purchaseorder_id, 'itemreceipt');

            //nlapiLogExecution('DEBUG', 'item_receipt', item_receipt.getId());

            var form = nlapiCreateForm('Item Receipt(Custom)');

            //adding hidden field to identify this form as IR form
            var formTypeHiddenField = form.addField('custpage_irhiddenfield', 'checkbox', 'IR Hidden Field');
            formTypeHiddenField.setDefaultValue('T');
            formTypeHiddenField.setDisplayType('hidden')
            form.addTab("custpage_tab1", "Items");
            form.setScript('customscript_cntm_woc_popup_validation');
            form.addFieldGroup("custpage_fld_first", "Primary Information");
            var vendor = form.addField("custpage_vendor", "select", "Vendor", 'vendor', 'custpage_fld_first');
            vendor.setDefaultValue(item_receipt.getFieldValue('entity'));
            vendor.setDisplayType('inline');
            vendor.setLayoutType('startrow', 'startcol');

            var createdfrom = form.addField("custpage_createdfrom", "select", "Created From", 'purchaseorder', 'custpage_fld_first');
            createdfrom.setDefaultValue(purchaseorder_id);
            createdfrom.setDisplayType('inline');
            createdfrom.setLayoutType('startrow', 'startcol');

            var date = form.addField("custpage_receiptdate", "date", "Date", null, "custpage_fld_first");
            date.setDisplayType('inline');
            date.setDefaultValue(item_receipt.getFieldValue('trandate'));
            date.setLayoutType('startrow', 'startcol');


            var postingperiod = form.addField("custpage_postingperiod", "text", "Posting Period", null, 'custpage_fld_first');
            postingperiod.setDefaultValue(item_receipt.getFieldText('postingperiod'));
            postingperiod.setDisplayType('inline');
            postingperiod.setLayoutType('startrow', 'startcol');

            var memo = form.addField("custpage_memo", "text", "Memo", null, 'custpage_fld_first');
            memo.setLayoutType('startrow', 'startcol');

            //adding a overReceipt field to check the Allow Overage feature for receipt
            var accountingInfo = nlapiLoadConfiguration('accountingpreferences');
            var overReceipts = accountingInfo.getFieldValue('overreceipts');
            //nlapiLogExecution('DEBUG', 'overReceipts', overReceipts);
            var overReceiptFld = form.addField('custpage_overreceipts', 'checkbox', 'OverReceipt', null, 'custpage_fld_first').setDisplayType('hidden').setLayoutType('startrow', 'startcol');
            if (overReceipts)
                overReceiptFld.setDefaultValue(overReceipts);


            var headerdata = form.addField("custpage_pagewisedata", "textarea", "Pagewisedata").setDisplayType('hidden');
            var sublist = form.addSubList("custpage_itemsublist", "list", "Items", 'custpage_tab1');
            sublist.addField("custpage_checkbox", "checkbox", "Receive").setDisplayType('normal');
            var item = sublist.addField("custpage_itemfield", "select", "item", "item").setDisplayType('entry').setMandatory(true).setDisplayType('inline');
            sublist.addField("custpage_itemhiddenid", "text", "hidden id").setDisplayType('entry').setDisplayType('hidden');
            sublist.addField("custpage_vendorcode", "text", "Vendor Name").setDisplayType('inline');

            sublist.addField("custpage_description", "text", "Description").setDisplayType('inline');

            sublist.addField("custpage_lineno", "text", "LineNo").setDisplayType('entry').setDisplayType('hidden');

            sublist.addField("custpage_location", "select", "Location", "location").setMandatory(true).setDisplayType('inline');
            sublist.addField("custpage_remainingqty", "float", "Remaining").setDisplayType('inline');
            var hiddensalesunit = sublist.addField("custpage_salesunitid", "text", "Units").setDisplayType('entry').setDisplayType('hidden');
            var unitdropdown = sublist.addField("custpage_salesunittext", "text", "Units").setDisplayType('entry').setDisplayType('inline');
            sublist.addField("custpage_quantity", "float", "Quantity").setMandatory(true).setDisplayType('entry');

            sublist.addField("custpage_lotassignmentcheckbox", "checkbox", "Lot Assignment").setMandatory(true).setDisplayType('entry');
            var itemwisemap = sublist.addField("custpage_itemwisepagedata", "textarea", "Itemwisedata");
            itemwisemap.setDisplaySize(10, 1);
            itemwisemap.setDisplayType('entry');
            itemwisemap.setDisplayType('disabled');
            itemwisemap.setMaxLength(1000000);

            sublist.addField("custpage_originalreceiveqty", "float", "Original Qty").setDisplayType('entry').setDisplayType('hidden');

            var sublistcounter = 1;
            var count = item_receipt.getLineItemCount('item');

            for (var i = 1; i <= count; i++) {
                //item_fulfill.getLineItemText('', '', i);
                var itemid = item_receipt.getLineItemValue('item', 'item', i);
                //nlapiLogExecution('DEBUG', 'itemid', itemid);

                var unittype = '';
                var salesunit = '';
                var newcustomqty = '';

                //nlapiLogExecution('DEBUG', 'quantity', item_receipt.getLineItemValue('item', 'quantity', i));
                sublist.setLineItemValue('custpage_itemfield', i, item_receipt.getLineItemValue('item', 'item', i));
                sublist.setLineItemValue('custpage_itemhiddenid', i, item_receipt.getLineItemValue('item', 'item', i));
                sublist.setLineItemValue('custpage_description', i, item_receipt.getLineItemValue('item', 'description', i));
                sublist.setLineItemValue('custpage_quantity', i, item_receipt.getLineItemValue('item', 'quantity', i));
                sublist.setLineItemValue('custpage_salesunitid', i, item_receipt.getLineItemValue('item', 'units', i));
                sublist.setLineItemValue('custpage_location', i, item_receipt.getLineItemValue('item', 'location', i));
                sublist.setLineItemValue('custpage_salesunittext', i, item_receipt.getLineItemValue('item', 'unitsdisplay', i));
                //nlapiLogExecution('DEBUG', 'i', i);
                sublist.setLineItemValue('custpage_lineno', i, i);

                //nlapiLogExecution('DEBUG', 'unittext', item_receipt.getLineItemValue('item', 'unitsdisplay', i));
                sublist.setLineItemValue('custpage_remainingqty', i, item_receipt.getLineItemValue('item', 'quantityremaining', i));
                sublist.setLineItemValue('custpage_vendorcode', i, item_receipt.getLineItemValue('item', 'vendorname', i));
                //nlapiLogExecution('DEBUG', '"vendorname"', item_receipt.getLineItemValue('item', 'vendorname', i));

            }

            //var ifID = nlapiSubmitRecord(item_fulfill);

            var hiddenfld = form.addField('custpage_hiddenpoid', 'text', 'poid', null, 'custpage_fld_first');
            hiddenfld.setDefaultValue(purchaseorder_id);
            hiddenfld.setDisplayType('hidden');


            form.addSubmitButton("Submit");
            form.addResetButton("Reset");
            form.addButton('custpage_backbutton', 'Back', 'history.back()');
            response.writePage(form);
        }

    } else if (request.getMethod() == 'POST') {

        var purchaseorder_id = request.getParameter('custpage_hiddenpoid');

        var item_receipt = nlapiTransformRecord('purchaseorder', purchaseorder_id, 'itemreceipt');
        //nlapiLogExecution('DEBUG', 'item_receipt(Post', item_receipt);

        var vendorid = nlapiLookupField('purchaseorder', purchaseorder_id, 'entity');

        var itemreceiptcount = item_receipt.getLineItemCount('item');
        var itemreceiptcount = item_receipt.getLineItemCount('item');
        if (itemreceiptcount) {
            for (var receiptLine = 1; receiptLine <= itemreceiptcount; receiptLine++) {
                item_receipt.selectLineItem('item', receiptLine);
                item_receipt.setCurrentLineItemValue('item', 'itemreceive', 'F');
                item_receipt.commitLineItem('item');
            }
        }
        var count = request.getLineItemCount('custpage_itemsublist');
        try {
            var map1 = [];
            var combinemap = {};
            var memo = request.getParameter('custpage_memo');
            if (memo)
                item_receipt.setFieldValue('memo', memo);
            for (var i = 1; i <= count; i++) {
                var tempcheckbox = request.getLineItemValue('custpage_itemsublist', 'custpage_checkbox', i);
                //nlapiLogExecution('DEBUG', 'tempcheckbox', tempcheckbox);
                var receiptLineNo = request.getLineItemValue('custpage_itemsublist', 'custpage_lineno', i);

                if (tempcheckbox == 'T') {

                    item_receipt.selectLineItem('item', receiptLineNo);
                    item_receipt.setCurrentLineItemValue('item', 'itemreceive', 'T');

                    //	nlapiLogExecution('DEBUG', 'inventorydetail_icon', item_receipt.getLineItemValue('item', 'inventorydetailavail', receiptLineNo));
                    var inventorydetails_icon = item_receipt.getLineItemValue('item', 'inventorydetailavail', i);

                    var quantity = request.getLineItemValue('custpage_itemsublist', 'custpage_quantity', i);
                    nlapiLogExecution('DEBUG', 'quantity', quantity);
                    var itemname = request.getLineItemValue('custpage_itemsublist', 'custpage_itemfield', i);

                    var mapdescription = request.getLineItemValue('custpage_itemsublist', 'custpage_itemwisepagedata', i);
                    item_receipt.setCurrentLineItemValue('item', 'custcol_cntm_mapdescription', mapdescription);
                    //nlapiLogExecution('DEBUG', 'mapdescription', typeof  mapdescription);
                    //	nlapiLogExecution('DEBUG', 'mapdescription.length',mapdescription.length);na


                    item_receipt.setCurrentLineItemValue('item', 'quantity', quantity);
                    item_receipt.setCurrentLineItemValue('item', 'custcol_cntm_quantity_custom', quantity);
                    item_receipt.setCurrentLineItemValue('item', 'custcol_cntm_original_item_qty', quantity);
                    if (mapdescription)
                        mapdescription = (mapdescription.length == 1) || (mapdescription.length == 0) ? "" : mapdescription;

                    if (mapdescription != undefined && mapdescription != null && mapdescription != "") {
                        //nlapiLogExecution('DEBUG', 'mapdescription1', mapdescription);
                        var mapvalue;
                        try {
                            mapvalue = JSON.parse(mapdescription);
                        } catch (e) {
                            nlapiLogExecution('DEBUG', 'e.message', e.message);
                        }


                        if (inventorydetails_icon == 'T') {

                            var subrecord = item_receipt.editCurrentLineItemSubrecord('item', 'inventorydetail');
                            if (subrecord != null && subrecord != undefined && subrecord != '') {
                                nlapiLogExecution('DEBUG', 'subrecord', subrecord);
                                var linecount = subrecord.getLineItemCount('inventoryassignment');
                                //nlapiLogExecution('DEBUG', 'linecount', linecount);
                                for (var j = linecount; j >= 1; --j) {
                                    subrecord.removeLineItem('inventoryassignment', j);
                                }
                                //	nlapiLogExecution('DEBUG', 'subrecord.getLineItemCount("inventoryassignment")', subrecord.getLineItemCount('inventoryassignment'));
                                for (var lotno in mapvalue) {
                                    //	nlapiLogExecution('DEBUG', 'lotno', lotno);
                                    var lotnumberArray = lotno.split('|');
                                    if (lotno != null) {

                                        var maparray = [];
                                        maparray = mapvalue[lotno].split('__');

                                        subrecord.selectNewLineItem('inventoryassignment');
                                        //		nlapiLogExecution('DEBUG', '4', '4');
                                        subrecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', lotnumberArray[0]);
                                        subrecord.setCurrentLineItemValue('inventoryassignment', 'quantity', parseFloat(maparray[2]));
                                        subrecord.setCurrentLineItemValue('inventoryassignment', 'binnumber', maparray[0]);
                                        subrecord.setCurrentLineItemValue('inventoryassignment', 'expirationdate', maparray[10]);

                                        subrecord.setCurrentLineItemValue('inventoryassignment', 'inventorystatus', maparray[1]);

                                        //		nlapiLogExecution('DEBUG', '5', '5');
                                        subrecord.commitLineItem('inventoryassignment');
                                        //		nlapiLogExecution('DEBUG', 'commit', 'commit');


                                    }

                                }
                                subrecord.commit();
                                //	nlapiLogExecution('DEBUG', 'subrecord.getLineItemCount("inventoryassignment")=>', subrecord.getLineItemCount('inventoryassignment'));
                                //	nlapiLogExecution('DEBUG', 'item_receipt.viewLineItemSubrecord', item_receipt.viewLineItemSubrecord('item', 'inventorydetail', 1));
                            } else {

                                var subrecord2 = item_receipt.createCurrentLineItemSubrecord('item', 'inventorydetail');
                                for (var lotno in mapvalue) {
                                    //	nlapiLogExecution('DEBUG', 'lotno', lotno);
                                    var lotnumberArray = lotno.split('|');
                                    if (lotno != null) {

                                        var maparray = [];
                                        maparray = mapvalue[lotno].split('__');
                                        subrecord2.selectNewLineItem('inventoryassignment');
                                        subrecord2.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', lotnumberArray[0]);
                                        subrecord2.setCurrentLineItemValue('inventoryassignment', 'quantity', parseFloat(maparray[2]));
                                        subrecord2.setCurrentLineItemValue('inventoryassignment', 'binnumber', maparray[0]);
                                        subrecord2.setCurrentLineItemValue('inventoryassignment', 'expirationdate', maparray[10]);

                                        subrecord2.setCurrentLineItemValue('inventoryassignment', 'inventorystatus', maparray[1]);

                                        //		nlapiLogExecution('DEBUG', '5', '5');
                                        subrecord2.commitLineItem('inventoryassignment');

                                    }

                                }
                                subrecord2.commit();
                                //nlapiSendEmail(nlapiGetContext().getUser(), nlapiGetContext().getUser(), 'JSON', JSON.stringify(subrecord2))
                                //	nlapiLogExecution('DEBUG', 'subrecord2.getLineItemCount("inventoryassignment")=>', subrecord2.getLineItemCount('inventoryassignment'));
                                //	nlapiLogExecution('DEBUG', 'item_receipt.viewLineItemSubrecord', item_receipt.viewLineItemSubrecord('item', 'inventorydetail', 1));
                            }
                        }
                    }
                    item_receipt.commitLineItem('item');
                } else {
                    //					continue;
                    item_receipt.setLineItemValue('item', 'itemreceive', i, 'F');
                }

            }

        } catch (e) {
            nlapiLogExecution('DEBUG', 'e.message', e.message);
        }
        //		nlapiLogExecution('DEBUG', 'item_receipt.getlineitemvalue', item_receipt.getLineItemValue('item', 'quantity', 1));
        try {

            var ifID = nlapiSubmitRecord(item_receipt, false, true);
            nlapiSetRedirectURL('RECORD', 'itemreceipt', ifID);
        } catch (e1) {
            nlapiLogExecution('DEBUG', 'Error while subimtting IR', e1.message);
        }

    }
}