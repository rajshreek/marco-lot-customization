/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       04 Jun 2019     Yogesh Jagdale
 *
 */

/**
 * @param {nlobjRequest} request Request object
 * @param {nlobjResponse} response Response object
 * @returns {Void} Any output is written via response object
 */
function itemWiseLotNumberedsuitelet(request, response) {
    try {
        if (request.getMethod() == 'GET') {
            var form = nlapiCreateForm("Lot Numbers", true);

            // 1743;
            var itemname = request.getParameter('itemname');
            var customer = request.getParameter('customer');
            nlapiLogExecution('DEBUG', 'itemname: ' + itemname, 'customer: ' + customer);
            var cpnSearchCols = "";
            var cooRequired = "";
            var approvedVendor = "";
            if (customer != "" && customer != undefined && customer != null) {
                var customrecord_scm_customerpartnumberSearch = nlapiSearchRecord("customrecord_scm_customerpartnumber", null,
                    [
                        ["custrecord_scm_cpn_item", "anyof", itemname],
                        "AND",
                        ["custrecord_scm_cpn_customer", "anyof", customer]
                    ],
                    [
                        new nlobjSearchColumn("custrecord_mr_coorequirement"),
                        new nlobjSearchColumn("custrecord_mr_approvedvendors")
                    ]
                );
                nlapiLogExecution('DEBUG', 'customrecord_scm_customerpartnumberSearch: ', customrecord_scm_customerpartnumberSearch);
                //nlapiLogExecution('DEBUG', 'customrecord_scm_customerpartnumberSearch.length: ', customrecord_scm_customerpartnumberSearch.length);

                if (customrecord_scm_customerpartnumberSearch) {
                    cpnSearchCols = customrecord_scm_customerpartnumberSearch[0].getAllColumns();
                    nlapiLogExecution('DEBUG', 'cpnSearchCols: ' + cpnSearchCols);
                    cooRequired = customrecord_scm_customerpartnumberSearch[0].getValue(cpnSearchCols[0]);
                    approvedVendor = customrecord_scm_customerpartnumberSearch[0].getValue(cpnSearchCols[1]);
                }
            }
            nlapiLogExecution('DEBUG', 'approvedVendor: ' + approvedVendor, 'cooRequired: ' + cooRequired);


            var location = request.getParameter('location') || request.getParameter('loc');
            var shelfLifeParam = request.getParameter('shelfLife');
            var quantity = parseFloat(request.getParameter('quantity'));
            var unittext = request.getParameter('unit');
            var description = request.getParameter('description');
            var salesunitid = request.getParameter('salesunit');


            var itemnamefield = form.addField('custpage_itemdropdown', 'select', 'Item', 'item');
            itemnamefield.setDefaultValue(itemname);
            itemnamefield.setDisplayType('inline');
            itemnamefield.setLayoutType('startrow', 'startcol');

            //added woc field to differentiate SO and WOC form popup
            var wocTran = request.getParameter('woc');
            if (wocTran) {
                var wocfield = form.addField('custpage_woctransaction', 'text', 'WOC');
                wocfield.setDefaultValue(wocTran);
                wocfield.setDisplayType('hidden');
                wocfield.setLayoutType('startrow', 'startcol');
            }
            var unittype;
            var stockunit;
            var stockunittext;
            var islotitem;
            var usebins;

            var itemType = nlapiLookupField('item', itemname, 'type');
            if (itemType == 'Assembly') {
                var assemblyItemRecord = nlapiLoadRecord('assemblyitem', itemname);
                if (assemblyItemRecord) {
                    unittype = assemblyItemRecord.getFieldValue('unitstype');
                    stockunit = assemblyItemRecord.getFieldValue('stockunit');
                    stockunittext = assemblyItemRecord.getFieldText('stockunit');
                    //nlapiLogExecution('DEBUG', 'unittype', unittype);
                    islotitem = assemblyItemRecord.getFieldValue('islotitem');
                    usebins = assemblyItemRecord.getFieldValue('usebins');
                }

            } else if (itemType == 'InvtPart') {

                var inventoryItemRecord = nlapiLoadRecord('inventoryitem', itemname);
                if (inventoryItemRecord) {
                    unittype = inventoryItemRecord.getFieldValue('unitstype');
                    stockunit = inventoryItemRecord.getFieldValue('stockunit');
                    stockunittext = inventoryItemRecord.getFieldText('stockunit');
                    //nlapiLogExecution('DEBUG', 'unittype', unittype);
                    islotitem = inventoryItemRecord.getFieldValue('islotitem');
                    usebins = inventoryItemRecord.getFieldValue('usebins');
                }
            }
            var unittextfield = form.addField('custpage_unittextfld', 'text', 'Units');
            //  var unittextfield = form.addField('custpage_unittextfld', 'select', 'UOM','units');
            if (unittext != null && unittext != undefined && unittext != '') {

                unittextfield.setDefaultValue(unittext);
            } else {
                if (itemname != null && itemname != undefined && itemname != '') {

                    unittextfield.setDefaultValue(stockunittext);
                }
            }
            unittextfield.setDisplayType('inline');
            unittextfield.setLayoutType('startrow', 'startcol');

            var locationfield = form.addField('custpage_location', 'select', 'Location', 'location');
            locationfield.setDefaultValue(location);
            locationfield.setDisplayType('inline');
            locationfield.setLayoutType('startrow', 'startcol');

            if (shelfLifeParam != undefined && shelfLifeParam != null) {
                var shelfLife = form.addField('custpage_cnt_cust_shelf_life', 'text', 'Customer Shelf Life');
                shelfLife.setDefaultValue(shelfLifeParam);
                shelfLife.setDisplayType('inline');
                shelfLife.setLayoutType('startrow', 'startcol');
            }
            if (customer != "" && customer != undefined && customer != null) {
                var cooRequiredfield = form.addField('custpage_coo_required', 'text', 'Coo Requirement');
                cooRequiredfield.setDefaultValue(cooRequired);
                cooRequiredfield.setDisplayType('inline');
                cooRequiredfield.setLayoutType('startrow', 'startcol');
                var approvedVendorfield = form.addField('custpage_approved_vendor', 'multiselect', 'Approved Vendors', 'vendor');
                approvedVendorfield.setDefaultValue(approvedVendor);
                approvedVendorfield.setDisplayType('disabled');
                approvedVendorfield.setLayoutType('startrow', 'startcol');
            }
            var qtyfield = form.addField('custpage_requirqty', 'float', 'Required Quantity');
            qtyfield.setDefaultValue(quantity);
            qtyfield.setDisplayType('inline');
            qtyfield.setLayoutType('startrow', 'startcol');

            var temp = 0;
            var selectedqtyfield = form.addField('custpage_selectedqty', 'float', 'Selected Quantity');
            var remainingQtyfield = form.addField('custpage_remainqty', 'float', 'Remaining Quantity');
            remainingQtyfield.setDisplayType('hidden');

            if (request.getParameter('selectqty')) {
                selectedqtyfield.setDefaultValue((request.getParameter('selectqty')));
            } else {
                selectedqtyfield.setDefaultValue((temp));
            }

            selectedqtyfield.setDisplayType('inline');
            selectedqtyfield.setLayoutType('startrow', 'startcol');

            var checktestvalue = 1;
            var hiddenchecktestfield = form.addField('custpage_hidden_checkfieldid', 'text', 'checkfld');
            hiddenchecktestfield.setDefaultValue(checktestvalue);
            hiddenchecktestfield.setDisplayType('hidden');

            var sublistcnt = 0;
            var subList = form.addSubList("custpage_lotnumberitem", "list", "Lots");
            var checkbox = subList.addField("custpage_checkbox", "checkbox", "Select");
            checkbox.setDisplayType('disabled');
            var number_col = subList.addField("custpage_inventorynumber", "text", "Serial/LOT Number").setDisplayType('inline');
            //	var bin_col = subList.addField("custpage_bin", "select", "Bin", 'bin').setDisplayType('inline');
            var bin_col = subList.addField("custpage_bin_new", "select", "Bin", 'bin').setDisplayType('inline');
            var status_col = subList.addField("custpage_status", "select", "Inventory Status", "inventorystatus").setDisplayType('hidden');
            var qty_col = subList.addField("custpage_quantity", "float", "On Hand Quantity").setDisplayType('inline');
            var qty_avail = subList.addField("custpage_quantity_avail", "float", "Available").setDisplayType('inline');
            var qty_selected_col = subList.addField("custpage_quantityslected", "float", "Quantity").setDisplayType('entry');
            var cureDate_col = subList.addField("custpage_curedate", "text", "Cure Date").setDisplayType('inline');
            var expDateMR_col = subList.addField("custpage_expdatemr", "text", "Expiration Date(MR)").setDisplayType('inline');
            var expDate_col = subList.addField("custpage_expdate", "text", "Expiration Date").setDisplayType('inline');

            var countryOrigin_col = subList.addField("custpage_country", "text", "Country of Origin").setDisplayType('inline');
            var manufacturer_col = subList.addField("custpage_manufacturer", "select", "Manufacturer", "customlist_cnt_manufacturers").setDisplayType('inline');
            var vendor_col = subList.addField("custpage_vendor", "select", "Vendor", 'vendor').setDisplayType('inline');
            var vendorItemNum_col = subList.addField("custpage_vendornumber", "text", "Vendor Item number").setDisplayType('inline');
            var mfgItemNum_col = subList.addField("custpage_mfgitemnum", "text", "Manufacturer Item Number").setDisplayType('inline');

            var usebinflag = false;
            if (islotitem == 'T') {
                if (usebins == 'T') {
                    usebinflag = true;
                } else if (usebins == 'F') {
                    usebinflag = false;
                }
            } else if (islotitem == 'F') {
                if (usebins == 'T') {
                    usebinflag = true;
                    mfgItemNum_col.setDisplayType('hidden');
                    vendorItemNum_col.setDisplayType('hidden');
                    vendor_col.setDisplayType('hidden');
                    manufacturer_col.setDisplayType('hidden');
                    countryOrigin_col.setDisplayType('hidden');
                    expDate_col.setDisplayType('hidden');
                    expDateMR_col.setDisplayType('hidden');
                    cureDate_col.setDisplayType('hidden');
                    status_col.setDisplayType('hidden');
                    number_col.setDisplayType('hidden');
                } else if (usebins == 'F') {
                    usebinflag = false;
                }
            }


            var conversionrate = '';

            var newcustomqty = '';

            //new code start for stockunit convertion rate
            var stockunit = nlapiLookupField('item', itemname, 'stockunit');
            var itemwiseconversionrate;
            var itemwisestockunitconversionrate = 1;
            var itemwisesalesunitconversionrate = 1;

            var checkduplicatemap = {};
            var outermaplotnumber = {};
            var innermaplotnumber = {};
            var outermapkey = 1;
            nlapiLogExecution('DEBUG', 'islotitem: ' + islotitem, 'usebins:' + usebins + " ---usebinflag" + usebinflag);
            if (usebinflag == true && islotitem == 'T') {
                var itemSearch = nlapiSearchRecord("item", null,
                    [
                        ["islotitem", "is", "T"],
                        "AND",
                        ["inventorynumber.location", "anyof", location],
                        /*	 ["sum(inventorynumberbinonhand.quantityavailable)", "greaterthan", "0"],
						 			 "AND",*/
                      /*  "AND",
                        ["sum(inventorynumber.quantityavailable)", "greaterthan", "0"],*/
                        "AND",
                        ["internalid", "anyof", itemname],
                        "AND",
                        ["formulatext: CASE WHEN {inventorynumberbinonhand.inventorynumber}={inventorynumber.inventorynumber} THEN {inventorynumberbinonhand.binnumber} END", "isnotempty", ""],
                        "AND",
                        ["inventorydetail.status", "anyof", "1"]

                    ],
                    [
                        new nlobjSearchColumn("quantityavailable", "inventoryNumber", "GROUP"),
                        new nlobjSearchColumn("inventorynumber", "inventoryNumber", "GROUP").setSort(false),
                        new nlobjSearchColumn("quantityonhand", "inventoryNumber", "GROUP"),
                        new nlobjSearchColumn("location", "inventoryNumber", "GROUP"),
                        new nlobjSearchColumn("binnumber", "inventorynumberbinonhand", "GROUP"),
                        new nlobjSearchColumn("stockunit", null, "GROUP"),
                        new nlobjSearchColumn("custitemnumber_cnt_cure_dt", "inventoryNumber", "GROUP"),
                        new nlobjSearchColumn("custitemnumber_cnt_mr_exp_dt", "inventoryNumber", "GROUP"),
                        new nlobjSearchColumn("custitemnumber_cnt_country_of_origin", "inventoryNumber", "GROUP"),
                        new nlobjSearchColumn("custitemnumber_cnt_manufacturer", "inventoryNumber", "GROUP"),
                        new nlobjSearchColumn("custitemnumber_cnt_vendor", "inventoryNumber", "GROUP"),
                        new nlobjSearchColumn("custitemnumber_cnt_vendor_it_numb", "inventoryNumber", "GROUP"),
                        new nlobjSearchColumn("custitemnumber_cnt_mfg_it_numb", "inventoryNumber", "GROUP"),
                        new nlobjSearchColumn("status", "inventoryDetail", "GROUP"),
                        new nlobjSearchColumn("quantityavailable", "inventoryNumber", "GROUP"),
                        new nlobjSearchColumn("expirationdate", "inventoryNumber", "GROUP")
                    ]
                );

                if (itemSearch != null && itemSearch != undefined) {
                    //nlapiLogExecution('DEBUG', 'itemSearch.length=>', itemSearch.length);
                    for (var i = 0; i < itemSearch.length && itemSearch[i] != undefined; i++) {
                        //nlapiLogExecution('DEBUG', 'i=>', i);
                        var resultitemSavedSearch = itemSearch[i];

                        var itemSavedSearchcolumns = resultitemSavedSearch.getAllColumns();
                        var boxname = resultitemSavedSearch.getValue(itemSavedSearchcolumns[1]);
                        var bintext = resultitemSavedSearch.getValue(itemSavedSearchcolumns[4]);

                        //nlapiLogExecution('DEBUG', 'boxname=>', boxname);
                        var tempkey = 1;
                        //nlapiLogExecution('DEBUG', i+') boxname ', boxname );
                        if (!checkduplicatemap[boxname]) {

                            checkduplicatemap[boxname] = boxname;

                            var stockunit = parseInt(resultitemSavedSearch.getValue(itemSavedSearchcolumns[5]));
                            var quantityAvailable = parseFloat(resultitemSavedSearch.getValue(itemSavedSearchcolumns[0]));

                            var newcustomqty = '';
                            //nlapiLogExecution('DEBUG', 'quantityAvailable', quantityAvailable);

                            // if (stockunit != salesunitid) 
                            {
                                newcustomqty = parseFloat(quantityAvailable) * parseFloat(itemwisestockunitconversionrate);
                                newcustomqty = parseFloat(newcustomqty) / parseFloat(itemwisesalesunitconversionrate);
                            }
                            /*else {
                                                       newcustomqty = parseFloat(quantityAvailable);
                                                   }*/
                            var warehouse = resultitemSavedSearch.getValue(itemSavedSearchcolumns[3]);
                            var binnumber = resultitemSavedSearch.getValue(itemSavedSearchcolumns[4]);
                            innermaplotnumber['checkbox'] = 'F';
                            //innermaplotnumber['bin'] = binnumber;
                            innermaplotnumber['loc'] = warehouse;
                            innermaplotnumber['box'] = boxname;
                            //	innermaplotnumber['qty'] = newcustomqty;
                            innermaplotnumber['qty'] = resultitemSavedSearch.getValue(itemSavedSearchcolumns[2]);
                            if (resultitemSavedSearch.getValue(itemSavedSearchcolumns[6]))
                                innermaplotnumber['curedt'] = resultitemSavedSearch.getValue(itemSavedSearchcolumns[6]);
                            nlapiLogExecution('DEBUG', 'curedt', resultitemSavedSearch.getValue(itemSavedSearchcolumns[6]));
                            if (resultitemSavedSearch.getValue(itemSavedSearchcolumns[7]) && resultitemSavedSearch.getValue(itemSavedSearchcolumns[7]) != '- None -')
                                innermaplotnumber['expdtMR'] = resultitemSavedSearch.getValue(itemSavedSearchcolumns[7]);
                            else
                                innermaplotnumber['expdtMR'] = '';
                            if (resultitemSavedSearch.getValue(itemSavedSearchcolumns[8])) {
                                innermaplotnumber['country'] = resultitemSavedSearch.getText(itemSavedSearchcolumns[8]);
                            } else {
                                innermaplotnumber['country'] = '';
                            }
                            innermaplotnumber['manufacturer'] = resultitemSavedSearch.getValue(itemSavedSearchcolumns[9]);
                            innermaplotnumber['vendor'] = resultitemSavedSearch.getValue(itemSavedSearchcolumns[10]);
                            if (resultitemSavedSearch.getValue(itemSavedSearchcolumns[11]) && resultitemSavedSearch.getValue(itemSavedSearchcolumns[11]) != '- None -')
                                innermaplotnumber['vendItnum'] = resultitemSavedSearch.getValue(itemSavedSearchcolumns[11]);
                            else
                                innermaplotnumber['vendItnum'] = '';
                            if (resultitemSavedSearch.getValue(itemSavedSearchcolumns[12]) && resultitemSavedSearch.getValue(itemSavedSearchcolumns[11]) != '- None -')
                                innermaplotnumber['mfgItnum'] = resultitemSavedSearch.getValue(itemSavedSearchcolumns[12]);
                            else
                                innermaplotnumber['mfgItnum'] = '';
                            //nlapiLogExecution('DEBUG', 'Status:: ', resultitemSavedSearch.getValue(itemSavedSearchcolumns[13]));
                            innermaplotnumber['status'] = resultitemSavedSearch.getValue(itemSavedSearchcolumns[13]);
                            innermaplotnumber['invavailable'] = resultitemSavedSearch.getValue(itemSavedSearchcolumns[14]);
                            innermaplotnumber['expDate'] = resultitemSavedSearch.getValue(itemSavedSearchcolumns[15]);

                            outermaplotnumber[outermapkey] = innermaplotnumber;
                            ++outermapkey;
                            innermaplotnumber = {};

                        }
                    }
                }
            } else if (usebinflag && islotitem == 'F') {

                var itemRecordSearch = nlapiSearchRecord("item", null,
                    [

                        ["internalid", "anyof", itemname],
                        "AND",
                        ["inventorydetail.quantity", "greaterthan", "0"],
                        "AND",
                        ["inventorydetail.location", "anyof", location]
                    ],
                    [

                        new nlobjSearchColumn("binnumber", "inventoryDetail", "GROUP").setSort(false),
                        new nlobjSearchColumn("quantity", "inventoryDetail", "SUM"),
                        new nlobjSearchColumn("itemcount", "inventoryDetail", "SUM"),
                    ]
                );
                nlapiLogExecution('DEBUG', 'itemRecordSearch', itemRecordSearch.length);
                var totalQty = 0;

                if (itemRecordSearch != null && itemRecordSearch != undefined) {
                    for (var i = 0; i < itemRecordSearch.length && itemRecordSearch[i] != undefined; i++) {

                        var resultitemSearch = itemRecordSearch[i];
                        //nlapiLogExecution('DEBUG', 'resultitemSearch', JSON.stringify(resultitemSearch));
                        // return all columns associated with this search
                        var itemSearchcolumns = resultitemSearch.getAllColumns();
                        var binname = resultitemSearch.getValue(itemSearchcolumns[0]);
                        nlapiLogExecution('DEBUG', 'binname', binname);
                        if (!checkduplicatemap[binname]) {

                            checkduplicatemap[binname] = binname;


                            var quantityAvailable = parseFloat(resultitemSearch.getValue(itemSearchcolumns[1]));


                            //var quantityAvailable=parseInt(resultitemSearch.getValue(itemSearchcolumns[1]));
                            innermaplotnumber['checkbox'] = 'F';
                            innermaplotnumber['bin'] = resultitemSearch.getValue(itemSearchcolumns[0]);;
                            innermaplotnumber['loc'] = '';
                            innermaplotnumber['box'] = '';
                            //innermaplotnumber['qty'] = newcustomqty;
                            innermaplotnumber['qty'] = resultitemSearch.getValue(itemSearchcolumns[2]);

                            innermaplotnumber['curedt'] = '';


                            innermaplotnumber['expdtMR'] = '';

                            innermaplotnumber['country'] = '';
                            innermaplotnumber['manufacturer'] = '';
                            innermaplotnumber['vendor'] = '';
                            innermaplotnumber['vendItnum'] = '';
                            innermaplotnumber['mfgItnum'] = '';
                            innermaplotnumber['status'] = '';
                            innermaplotnumber['invavailable'] = resultitemSearch.getValue(itemSearchcolumns[2]);
                            innermaplotnumber['expDate'] = '';


                            var updatedlotdate = [];
                            innermaplotnumber['lotdt'] = '';
                            outermaplotnumber[outermapkey] = innermaplotnumber;
                            nlapiLogExecution('DEBUG', 'Map', JSON.stringify(innermaplotnumber));
                            ++outermapkey;
                            innermaplotnumber = {};

                        }
                    }
                }

            } else if (usebinflag == false) {
                var itemRecordSearch = nlapiSearchRecord("item", null,
                    [
                        ["islotitem", "is", "T"],
                        "AND",
                        ["inventorynumber.location", "anyof", location],
                        "AND",
                        ["sum(inventorynumber.quantityavailable)", "greaterthan", "0"],
                        "AND",
                        ["internalid", "anyof", itemname]
                    ],
                    [
                        new nlobjSearchColumn("inventorynumber", "inventoryNumber", "GROUP").setSort(true),
                        new nlobjSearchColumn("quantityavailable", "inventoryNumber", "GROUP"),
                        new nlobjSearchColumn("expirationdate", "inventoryNumber", "GROUP"),
                        new nlobjSearchColumn("isonhand", "inventoryNumber", "GROUP"),
                        new nlobjSearchColumn("itemid", null, "GROUP"),
                        new nlobjSearchColumn("item", "inventoryNumber", "GROUP"),
                        new nlobjSearchColumn("quantityonhand", "inventoryNumber", "GROUP"),
                        new nlobjSearchColumn("quantityonorder", "inventoryNumber", "GROUP"),
                        new nlobjSearchColumn("location", "inventoryNumber", "GROUP"),
                        new nlobjSearchColumn("stockunit", null, "GROUP"),
                        new nlobjSearchColumn("custitemnumber_cnt_cure_dt", "inventoryNumber", "GROUP"),
                        new nlobjSearchColumn("custitemnumber_cnt_mr_exp_dt", "inventoryNumber", "GROUP"),
                        new nlobjSearchColumn("custitemnumber_cnt_country_of_origin", "inventoryNumber", "GROUP"),
                        new nlobjSearchColumn("custitemnumber_cnt_manufacturer", "inventoryNumber", "GROUP"),
                        new nlobjSearchColumn("custitemnumber_cnt_vendor", "inventoryNumber", "GROUP"),
                        new nlobjSearchColumn("custitemnumber_cnt_vendor_it_numb", "inventoryNumber", "GROUP"),
                        new nlobjSearchColumn("custitemnumber_cnt_mfg_it_numb", "inventoryNumber", "GROUP"),
                        new nlobjSearchColumn("status", "inventoryDetail", "GROUP"),
                        new nlobjSearchColumn("quantityavailable", "inventoryNumber", "GROUP")
                    ]
                );
                var totalQty = 0;

                if (itemRecordSearch != null && itemRecordSearch != undefined) {
                    for (var i = 0; i < itemRecordSearch.length && itemRecordSearch[i] != undefined; i++) {

                        var resultitemSearch = itemRecordSearch[i];
                        //nlapiLogExecution('DEBUG', 'resultitemSearch', JSON.stringify(resultitemSearch));
                        // return all columns associated with this search
                        var itemSearchcolumns = resultitemSearch.getAllColumns();
                        var boxname = resultitemSearch.getValue(itemSearchcolumns[0]);
                        if (!checkduplicatemap[boxname + '']) {

                            checkduplicatemap[boxname + ''] = boxname + '';

                            var stockunit = parseInt(resultitemSearch.getValue(itemSearchcolumns[9]));
                            var quantityAvailable = parseFloat(resultitemSearch.getValue(itemSearchcolumns[1]));

                            var newcustomqty = '';
                            if (stockunit != salesunitid) {
                                newcustomqty = parseFloat(quantityAvailable) * parseFloat(itemwisestockunitconversionrate);
                                newcustomqty = parseFloat(newcustomqty) / parseFloat(itemwisesalesunitconversionrate);

                            } else {
                                newcustomqty = parseFloat(quantityAvailable);
                            }

                            //var quantityAvailable=parseInt(resultitemSearch.getValue(itemSearchcolumns[1]));
                            var warehouse = resultitemSearch.getValue(itemSearchcolumns[8]);
                            innermaplotnumber['checkbox'] = 'F';
                            //innermaplotnumber['bin'] = '';
                            innermaplotnumber['loc'] = warehouse;
                            innermaplotnumber['box'] = boxname;
                            //innermaplotnumber['qty'] = newcustomqty;
                            innermaplotnumber['qty'] = resultitemSearch.getValue(itemSearchcolumns[6]);
                            if (resultitemSearch.getValue(itemSearchcolumns[10]))
                                innermaplotnumber['curedt'] = resultitemSearch.getValue(itemSearchcolumns[10]);
                            nlapiLogExecution('DEBUG', 'expdtMR', resultitemSearch.getValue(itemSearchcolumns[11]));
                            if (resultitemSearch.getValue(itemSearchcolumns[11]) && resultitemSearch.getValue(itemSearchcolumns[11]) != '- None -')
                                innermaplotnumber['expdtMR'] = resultitemSearch.getValue(itemSearchcolumns[11]);
                            else
                                innermaplotnumber['expdtMR'] = '';
                            if (resultitemSearch.getValue(itemSearchcolumns[12])) {
                                innermaplotnumber['country'] = resultitemSearch.getText(itemSearchcolumns[12]);
                            } else {
                                innermaplotnumber['country'] = '';
                            }
                            innermaplotnumber['manufacturer'] = resultitemSearch.getValue(itemSearchcolumns[13]);
                            innermaplotnumber['vendor'] = resultitemSearch.getValue(itemSearchcolumns[14]);
                            if (resultitemSearch.getValue(itemSearchcolumns[15]) && resultitemSearch.getValue(itemSearchcolumns[15]) != '- None -')
                                innermaplotnumber['vendItnum'] = resultitemSearch.getValue(itemSearchcolumns[15]);
                            else
                                innermaplotnumber['vendItnum'] = '';
                            if (resultitemSearch.getValue(itemSearchcolumns[16]) && resultitemSearch.getValue(itemSearchcolumns[16]) != '- None -')
                                innermaplotnumber['mfgItnum'] = resultitemSearch.getValue(itemSearchcolumns[16]);
                            else
                                innermaplotnumber['mfgItnum'] = '';
                            //nlapiLogExecution('DEBUG', 'Status:: ', resultitemSearch.getValue(itemSearchcolumns[17]));
                            innermaplotnumber['status'] = resultitemSearch.getValue(itemSearchcolumns[17]);
                            innermaplotnumber['invavailable'] = resultitemSearch.getValue(itemSearchcolumns[18]);
                            innermaplotnumber['expDate'] = resultitemSearch.getValue(itemSearchcolumns[2]);


                            var updatedlotdate = [];
                            innermaplotnumber['lotdt'] = '';
                            outermaplotnumber[outermapkey] = innermaplotnumber;
                            ++outermapkey;
                            innermaplotnumber = {};

                        }
                    }
                }
            }

            if (usebinflag == true && islotitem == 'F') {

                var mapkeys = Object.keys(outermaplotnumber);
                var totalrecord = mapkeys.length;
                //nlapiLogExecution('DEBUG', 'totalrecord', totalrecord);



                for (var i = 0; i < totalrecord; i++) {
                    if (typeof outermaplotnumber[mapkeys[i]] == 'object') {

                        var innermap = outermaplotnumber[mapkeys[i]];
                        if (parseFloat(innermap['invavailable']) == 0 && parseFloat(innermap['qty']) > 0) {
                            //nlapiLogExecution('DEBUG', 'Lot Skipped', 'Lot wth name: '+innermap['box']);
                        } else {

                            sublistcnt = sublistcnt + 1;
                            subList.setLineItemValue('custpage_checkbox', sublistcnt, innermap['checkbox']);

                            subList.setLineItemValue('custpage_bin_new', sublistcnt, innermap['bin']);
                            //subList.setLineItemValue('custpage_status', sublistcnt,  innermap['status']);
                            //subList.setLineItemValue('custpage_inventorynumber', sublistcnt, innermap['box']);
                            subList.setLineItemValue('custpage_quantity', sublistcnt, innermap['qty']);
                            /*				subList.setLineItemValue('custpage_curedate', sublistcnt, innermap['curedt']);
                            				subList.setLineItemValue('custpage_expdatemr', sublistcnt, innermap['expdtMR']);
                            				subList.setLineItemValue('custpage_expdate', sublistcnt, innermap['expDate']);
                            				subList.setLineItemValue('custpage_country', sublistcnt, innermap['country']);
                            				subList.setLineItemValue('custpage_manufacturer', sublistcnt, innermap['manufacturer']);
                            				subList.setLineItemValue('custpage_vendor', sublistcnt, innermap['vendor']);
                            				subList.setLineItemValue('custpage_vendornumber', sublistcnt, innermap['vendItnum']);
                            				subList.setLineItemValue('custpage_mfgitemnum', sublistcnt, innermap['mfgItnum']);*/
                            subList.setLineItemValue('custpage_quantity_avail', sublistcnt, innermap['invavailable']);
                        }
                    }

                }

            } else {
                var mapkeys = Object.keys(outermaplotnumber);
                var totalrecord = mapkeys.length;
                //nlapiLogExecution('DEBUG', 'totalrecord', totalrecord);

                bin_col.setDisplayType('hidden');
                for (var i = 0; i < totalrecord; i++) {
                    if (typeof outermaplotnumber[mapkeys[i]] == 'object') {

                        var innermap = outermaplotnumber[mapkeys[i]];
                        if (parseFloat(innermap['invavailable']) == 0 && parseFloat(innermap['qty']) > 0) {
                            //nlapiLogExecution('DEBUG', 'Lot Skipped', 'Lot wth name: '+innermap['box']);
                        } else {

                            sublistcnt = sublistcnt + 1;
                            subList.setLineItemValue('custpage_checkbox', sublistcnt, innermap['checkbox']);

                            //subList.setLineItemValue('custpage_bin', sublistcnt, innermap['bin']);
                            subList.setLineItemValue('custpage_status', sublistcnt, innermap['status']);
                            subList.setLineItemValue('custpage_inventorynumber', sublistcnt, innermap['box']);
                            subList.setLineItemValue('custpage_quantity', sublistcnt, innermap['qty']);
                            subList.setLineItemValue('custpage_curedate', sublistcnt, innermap['curedt']);
                            subList.setLineItemValue('custpage_expdatemr', sublistcnt, innermap['expdtMR']);
                            subList.setLineItemValue('custpage_expdate', sublistcnt, innermap['expDate']);
                            subList.setLineItemValue('custpage_country', sublistcnt, innermap['country']);
                            subList.setLineItemValue('custpage_manufacturer', sublistcnt, innermap['manufacturer']);
                            subList.setLineItemValue('custpage_vendor', sublistcnt, innermap['vendor']);
                            subList.setLineItemValue('custpage_vendornumber', sublistcnt, innermap['vendItnum']);
                            subList.setLineItemValue('custpage_mfgitemnum', sublistcnt, innermap['mfgItnum']);
                            subList.setLineItemValue('custpage_quantity_avail', sublistcnt, innermap['invavailable']);
                        }
                    }

                }
            }


            form.addSubmitButton("Submit");
            //form.addResetButton('Clear');
            form.addButton('custpage_clearbtn', 'Clear', 'page_reset();');
            //form.addResetButton('Clear');
            //form.addButton('custpage_closebtn','Clearncel',' ');
            form.addButton('custpage_closebtn', 'Cancel', 'closewindow()');
            form.setScript('customscript_cntm_woc_popup_validation');
            response.writePage(form);
        }
    } catch (e) {
        nlapiLogExecution('ERROR', 'Error Catched', e.message);
    }

}