/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       10 Jun 2019     Yogesh Jagdale
 *
 */



/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 *   
 * @param {String} type Operation types: create, edit, view, copy, print, email
 * @param {nlobjForm} form Current form
 * @param {nlobjRequest} request Request object
 * @returns {Void}
 */
function userEventBeforeLoad(type, form, request){
/*	if(type=='copy'){

		var count=nlapiGetLineItemCount('item');

		for(var i=1;i<=count;i++){
			debugger;
			nlapiSelectLineItem('item',i); 	
			nlapiLogExecution('DEBUG',i,nlapiGetCurrentLineItemValue('item', 'custpage_lotdetailsmap'))
			nlapiSetCurrentLineItemValue('item','custpage_lotdetailsmap',"",false,false);
			nlapiCommitLineItem('item');

		}
	}*/
}




/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit
 *                      approve, reject, cancel (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF)
 *                      markcomplete (Call, Task)
 *                      reassign (Case)
 *                      editforecast (Opp, Estimate)
 * @returns {Void}
 */
function userEventBeforeSubmit(type){

}

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit,
 *                      approve, cancel, reject (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF only)
 *                      dropship, specialorder, orderitems (PO only) 
 *                      paybills (vendor payments)
 * @returns {Void}
 */
function userEventAfterSubmit(type){
	try{
		var recId= nlapiGetRecordId();
		var recType= nlapiGetRecordType();
		var salesOrder = nlapiLoadRecord(recType,recId);
		var count=salesOrder.getLineItemCount('item');	
		for (var linecount = 1; linecount <= count; linecount++) {
			salesOrder.selectLineItem('item', linecount);
			var mapdescription=salesOrder.getLineItemValue('item', 'custcol_cntm_mapdescription', linecount);
			nlapiLogExecution('DEBUG', 'linecount', linecount);
			nlapiLogExecution('DEBUG', 'mapdescription', mapdescription);
			var inventorydetailavail=salesOrder.getLineItemValue('item', 'custcol_cntm_lotassignment', linecount);
			nlapiLogExecution('DEBUG', 'inventorydetailavail', inventorydetailavail);
			if(mapdescription!=null && mapdescription!=undefined && mapdescription!='')
			{
				var subrecord = salesOrder.editCurrentLineItemSubrecord('item', 'inventorydetail');
				nlapiLogExecution('DEBUG', 'invDetailSubrecord', subrecord);
				
			/*	if(invDetailSubrecord){
					salesOrder.removeCurrentLineItemSubrecord('item', 'inventorydetail');
					salesOrder.commitLineItem('item');
					nlapiLogExecution('DEBUG', 'subrecord removed', invDetailSubrecord);
				}*/
				var mapvalue=JSON.parse(mapdescription);
				nlapiLogExecution('DEBUG', 'mapvalue', mapvalue);
				var islotnameavailable={};
			//	var subrecord=salesOrder.createCurrentLineItemSubrecord('item', 'inventorydetail');
				nlapiLogExecution('DEBUG', 'subrecord', subrecord);
				if(subrecord)
				{
					var cnt=subrecord.getLineItemCount('inventoryassignment');
					var updatedSuccess=[];
					nlapiLogExecution('DEBUG', 'cnt', cnt);
					for(var i=cnt;i>=1;--i)
					{
						var tempCounter=0;
						
						subrecord.selectLineItem('inventoryassignment',i);
						var lotno =	subrecord.getCurrentLineItemValue('inventoryassignment', 'issueinventorynumber');
						var invQty =	subrecord.getCurrentLineItemValue('inventoryassignment', 'quantity');
						subrecord.commitLineItem('inventoryassignment');
						 nlapiLogExecution('DEBUG', ' lotno'+ lotno, 'invQty: '+invQty+" i="+i);
							nlapiLogExecution('DEBUG', 'lotno', lotno);
							if(mapvalue[lotno] && parseFloat(invQty)==parseFloat(mapvalue[lotno]))
							{
								++tempCounter
								updatedSuccess.push(lotno);
								 nlapiLogExecution('DEBUG', ' Lot skipped containing same Qty', 'lotno: '+lotno);
								nlapiLogExecution('DEBUG', 'Done for Line: ',tempCounter);
								continue;
							}else if(mapvalue[lotno] && parseFloat(invQty)!=parseFloat(mapvalue[lotno])){
								++tempCounter
								updatedSuccess.push(lotno);
								subrecord.selectLineItem('inventoryassignment',i);
								 nlapiLogExecution('DEBUG', '  parseFloat(invQty)'+ parseFloat(invQty), 'parseFloat(mapvalue[lotno]): '+parseFloat(mapvalue[lotno]));
								 nlapiLogExecution('DEBUG', ' in Else if -- issueinventorynumber',lotno);
								 nlapiLogExecution('DEBUG', ' parseFloat(mapvalue[lotno])', parseFloat(mapvalue[lotno]));
								
								//subrecord.setCurrentLineItemValue('inventoryassignment', 'issueinventorynumber',lotno);
								subrecord.setCurrentLineItemValue('inventoryassignment', 'quantity',parseFloat(mapvalue[lotno]));
								subrecord.commitLineItem('inventoryassignment'); 
								nlapiLogExecution('DEBUG', 'Done for Line: ',tempCounter);
							}else{
								 nlapiLogExecution('DEBUG', ' in Else','line Number '+i);
								subrecord.removeLineItem('inventoryassignment',i);
								subrecord.selectNewLineItem('inventoryassignment');
							}
						/*	else{
								++tempCounter
							//	subrecord.selectLineItem('inventoryassignment',i);
								nlapiLogExecution('DEBUG', '  parseFloat(invQty)'+ parseFloat(invQty), 'parseFloat(mapvalue[lotno]): '+parseFloat(mapvalue[lotno]));
								 nlapiLogExecution('DEBUG', ' in Else -- issueinventorynumber',lotno);
								subrecord.selectNewLineItem('inventoryassignment');
								nlapiLogExecution('DEBUG', 'issueinventorynumber: '+lotno, 'quantity: '+parseFloat(mapvalue[lotno]));
								subrecord.setCurrentLineItemValue('inventoryassignment', 'issueinventorynumber',lotno);
								subrecord.setCurrentLineItemValue('inventoryassignment', 'quantity',parseFloat(mapvalue[lotno]));
								subrecord.commitLineItem('inventoryassignment'); 
								nlapiLogExecution('DEBUG', 'Done for Line: ',tempCounter);
							}*/
						
					}	
					
					for(var lotno in mapvalue)
					{
						var filterArray=updatedSuccess.filter(function(o) { return o==lotno?o:"";  });
						nlapiLogExecution('DEBUG', 'filterArray: '+filterArray, 'filterArray.length: '+filterArray.length);
						if(filterArray.length==0){
							subrecord.selectNewLineItem('inventoryassignment');
							nlapiLogExecution('DEBUG', 'issueinventorynumber: '+lotno, 'quantity: '+parseFloat(mapvalue[lotno]));
							subrecord.setCurrentLineItemValue('inventoryassignment', 'issueinventorynumber',lotno);
							subrecord.setCurrentLineItemValue('inventoryassignment', 'quantity',parseFloat(mapvalue[lotno]));
							subrecord.commitLineItem('inventoryassignment'); 
						}
					}
					
					
					subrecord.commit();
				}else{
					subrecord=salesOrder.createCurrentLineItemSubrecord('item', 'inventorydetail');
					if(subrecord!=null && subrecord!=undefined && subrecord!='')
					{
						for(var lotno in mapvalue)
						{
							nlapiLogExecution('DEBUG', 'lotno', lotno);
							if(lotno!=null)
							{
								
								subrecord.selectNewLineItem('inventoryassignment');
								nlapiLogExecution('DEBUG', 'issueinventorynumber: '+lotno, 'quantity: '+parseFloat(mapvalue[lotno]));
								subrecord.setCurrentLineItemValue('inventoryassignment', 'issueinventorynumber',lotno);
								subrecord.setCurrentLineItemValue('inventoryassignment', 'quantity',parseFloat(mapvalue[lotno]));
								subrecord.commitLineItem('inventoryassignment'); 
							}
						}
						subrecord.commit();
					}
				}
			}else{
				salesOrder.removeCurrentLineItemSubrecord('item', 'inventorydetail');
			}
			salesOrder.commitLineItem('item')
		}
		nlapiSubmitRecord(salesOrder);
	}catch(e){
		var error=nlapiCreateError('123',e.message,true);
		throw error;
		nlapiLogExecution('ERROR', 'Error Catched', e.message);
	}
}


