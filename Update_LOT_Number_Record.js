/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       03 Jun 2019     Rajshree Kakane
 *
 */
function updateLOT(lotdata,item)
{
	var lotArray=[];
	if(Object.keys(lotdata).length>0)
	{
     
		for ( var lotno in lotdata) {
          try{
			if(lotno)
			{
              var lotnumberArray=lotno.split('|');
              var maparray=lotdata[lotno].split('__');
				var lotNumberid=getRecordByCriteria("inventorynumber", [new nlobjSearchFilter('item',null,'is',item),new nlobjSearchFilter('inventorynumber',null,'is',lotnumberArray[0])]);
               nlapiLogExecution('DEBUG', 'lotNumberid',lotNumberid);  
              if (lotNumberid) {
                    var serial_rec = nlapiLoadRecord('inventorynumber', lotNumberid);
                    serial_rec.setFieldValue("memo", maparray[11]);
                    serial_rec.setFieldValue("custitemnumber_cnt_cure_dt", maparray[3]);
                    serial_rec.setFieldValue("custitemnumber_cnt_mr_exp_dt",  maparray[9]);
                    serial_rec.setFieldValue("custitemnumber_cnt_country_of_origin", maparray[4]);
                    serial_rec.setFieldValue("custitemnumber_cnt_manufacturer", maparray[5]);
                    serial_rec.setFieldValue("custitemnumber_cnt_vendor", maparray[6]);
                    serial_rec.setFieldValue("custitemnumber_cnt_vendor_it_numb", maparray[7]);
                    serial_rec.setFieldValue("custitemnumber_cnt_mfg_it_numb", maparray[8]);
                    var id = nlapiSubmitRecord(serial_rec);
                    nlapiLogExecution('DEBUG', 'return id', id);
                    lotArray.push(id);
                }
			}
        }catch(e){
          nlapiLogExecution('ERROR', 'e.message', e.message);
        }
		}
		return lotArray;
	}
}
function getRecordByCriteria(type, criteria) {
    var search = nlapiSearchRecord(type, null, criteria, [new nlobjSearchColumn('internalid')]);
    if (search) {

        var result = search[0];
        if (result) {
            return result.getId();
        }
    }
    return "";
}
