/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       04 Jun 2019     Yogesh Jagdale
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType 
 * 
 * @param {String} type Access mode: create, copy, edit
 * @returns {Void}
 */
var keyCodeOfQ = 81;//81 being the KeyCode of the Q

//quarter month array
var quarterMonth={'1':'1','2':'1','3':'1','4':'2','5':'2','6':'2','7':'3','8':'3','9':'3','10':'4','11':'4','12':'4'}//here key as quarter month and value a quarter
function FieldValidator(id, name, value) {
	this.id = id;
	this.name = name;
	this.value = value;
	this.validated = false;
	this.errorMsg = '';
	this.quarterLastDate='';
	return this;
}

/* object method to verify a String format*/
FieldValidator.prototype.verifyStringFormat = function(minLength, maxLength,
		failureMsg) {
	minLength = minLength || 1;
	maxLength = maxLength || 300;
	nlapiLogExecution('DEBUG', 'FieldValidator',
			'CALLED verifyStringFormat for ' + this.name);
	var field_string = String(this.value);
	var field_len = field_string.length;

	// check MIN length
	if (minLength >= 0 && minLength <= field_len) {
		// check MAX length
		if (maxLength >= 0 && maxLength >= field_len) {
			// Add Extra Validation For Text Fields
			this.validated = true;
		} else {
			this.errorMsg=this.name+ ' must have a length of not greater than ' + maxLength;
		}
	} else {
		this.errorMsg=this.name + ' must have a length of not less than '+ minLength;
	}
	return this;
};

/* object method to verify a Number format*/
FieldValidator.prototype.verifyNumberFormat = function(minValue, maxValue,
		failureMsg) {
	nlapiLogExecution('DEBUG', 'FieldValidator',
			'CALLED verifyNumberFormat for ' + this.name);
	minValue = minValue || 0;
	maxValue = maxValue || 9999999999999999999;
	var field_integer = parseFloat(this.value);

	// check MIN value
	if (minValue >= 0 && minValue <= field_integer) {
		// check MAX value
		if (maxValue >= 0 && maxValue >= field_integer) {
			// Add Extra Validation For Number Fields
			this.validated = true;

		} else {
			this.errorMsg=this.name	+ ' must have a value of not greater than ' + maxValue;
		}
	} else {
		this.errorMsg=this.name + ' must have a value of not less than '+ minValue;
	}
	return this;
};

/* object method to verify a Select Field Value*/
FieldValidator.prototype.verifySelectOption = function(failureMsg) {
	nlapiLogExecution('DEBUG', 'FieldValidator',
			'CALLED verifySelectOption for ' + this.name);

	var field_value = this.value
	// check MIN value
	if (field_value) {
		// Add Extra Validation For select field
		this.validated = true;
	} else {
		if (failureMsg) {
			this.errorMsg=failureMsg;
		} else {
			this.errorMsg=' Please enter a value for ' + this.name;
		}
	}
	return this;
};

/* object method to verify a CureDate format*/
FieldValidator.prototype.verifyCureDate = function(failureMsg) {
	minLength = 4;
	maxLength = 4;
	nlapiLogExecution('DEBUG', 'FieldValidator', 'CALLED verifyCureDate for '
			+ this.name);
	var field_string = String(this.value);
	var field_len = field_string.length;

	// check MIN length
	if (minLength >= 0 && minLength <= field_len) {
		// check MAX length
		if (maxLength >= 0 && maxLength >= field_len) {
			// Add Extra Validation For Text Fields
			var quarterString = field_string.substring(0, 2);
			var containsQ = quarterString[1].charCodeAt() == keyCodeOfQ;
			var selectedQuarter=quarterString[0];
			var currentMonth=new Date().getMonth()+1;
			var currentQuater;
			if(quarterMonth[currentMonth]){
				currentQuater=quarterMonth[currentMonth];
			}
			var correctQuaters = quarterString[0] == '1'
				|| quarterString[0] == '2' || quarterString[0] == '3'
					|| quarterString[0] == '4';
			var validateYear = parseInt(new Date().getYear().toString()
					.substring(1, 3)) >= parseInt(field_string.substring(2, 4));
					this.validated = (containsQ && correctQuaters && validateYear) ? true : false;
					this.errorMsg = "Please enter correct value for "+this.name;
		} else {
			this.errorMsg=this.name	+ ' must have a length of not greater than ' + maxLength;
		}
	} else {
		this.errorMsg=this.name + ' must have a length of not less than ' + minLength;
	}
	
	return this;
};

/* object method to verify a CureDate format*/
FieldValidator.prototype.getQuarterLastDate = function(cureDate) {
		var currentYear=2000+parseInt(cureDate.substring(2,5));
			var quartersDates=[currentYear+"/03/20",currentYear+"/06/20",currentYear+"/09/20",currentYear+"/12/20"];
			var quarter=parseInt(cureDate.substring(0,1));
		if(quarter>=1 && quarter<=4){
			var date = new Date(quartersDates[(quarter-1)]);
			this.quarterLastDate = nlapiDateToString(new Date(date.getFullYear(), date.getMonth() + 1, 0));
			}else{
				this.errorMsg="Invalid Cure Date."
			}
	return this;
};






