/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       28 May 2019     Rajshree Kakane
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 *   
 * @param {String} type Operation types: create, edit, view, copy, print, email
 * @param {nlobjForm} form Current form
 * @param {nlobjRequest} request Request object
 * @returns {Void}
 */
function userEventBeforeLoad(type, form, request) {
    var recType = nlapiGetRecordType();
    //nlapiLogExecution('DEBUG', 'recType', recType);
    if (type == 'edit') {
        if (recType == 'itemreceipt') {
            //form.addField('custpage_inlinefld', 'inlinehtml', 'Validation Fld').setDefaultValue("<script>window.onload=function(){var rows=jQuery('#item_splits tr');jQuery(rows[0]).children()[10].remove();for(i=1;i<rows.length;i++){jQuery(rows[i]).children()[24].remove();}}</script>")
            var receiptCount = nlapiGetLineItemCount('item');
            //	nlapiLogExecution('DEBUG', 'receiptCount', receiptCount);
            for (var lineCount = 1; lineCount <= receiptCount; lineCount++) {
                var quantityFld = nlapiGetLineItemField('item', "quantity", lineCount);
                var quantity = nlapiGetLineItemValue('item', "quantity", lineCount)
                if (quantityFld) {
                    quantityFld.setDisplayType('hidden');
                }
            }

        } else if (recType == 'workordercompletion') {
            var isBackFlush = nlapiGetFieldValue('isbackflush');
            if (isBackFlush == 'T') {
                var assemblyItem = nlapiGetFieldValue('item');
                if (assemblyItem) {
                    var isLOTNum = nlapiLookupField('item', assemblyItem, 'islotitem');
                    if (isLOTNum == 'T' || isLOTNum == true) {
                        //create a field to validate the ending operation on WOC edit mode.
                        var routingSeqMapFld = form.addField('custpage_routingsequencemap', 'text', 'Routing Map').setDisplayType('hidden');
                        var hiddenEndingOp = form.addField('custpage_hiddenendoperation', 'text', 'Hidden End Operation').setDisplayType('hidden');
                        var lastRoutingOperationFld = form.addField('custpage_lastroutingoperation', 'text', 'Last Routing Operation').setDisplayType('hidden');
                        var routingSeqArrayMap = {};

                        var inventoryDetailFld = form.getField('inventorydetail');
                        //if (inventoryDetailFld)
                        //inventoryDetailFld.setDisplayType('hidden');
                        var completedQuantityFld = nlapiGetField('completedquantity');
                        if (completedQuantityFld)
                            completedQuantityFld.setDisplayType('disabled');

                        nlapiSetFieldValue('completedquantity', '');
                        var endingoperation = nlapiGetFieldText('endoperation');
                        hiddenEndingOp.setDefaultValue(endingoperation);
                        var manufact_routid = nlapiGetFieldValue('manufacturingrouting');
                        if (manufact_routid) {
                            var manufactroutingrec = nlapiLoadRecord('manufacturingrouting', manufact_routid);
                            if (manufactroutingrec != null && manufactroutingrec != undefined && manufactroutingrec != '') {
                                var rountingcount = manufactroutingrec.getLineItemCount('routingstep');
                                for (var routLine = 1; routLine <= rountingcount; routLine++) {
                                    var opSeq = manufactroutingrec.getLineItemValue('routingstep', 'operationsequence', routLine);
                                    //nlapiLogExecution('DEBUG', 'opSeq', opSeq);
                                    routingSeqArrayMap[routLine] = opSeq;
                                }
                                // nlapiLogExecution('DEBUG', 'routingSeqArrayMap', JSON.stringify(routingSeqArrayMap));
                                routingSeqMapFld.setDefaultValue(JSON.stringify(routingSeqArrayMap));
                                var roun_oper = manufactroutingrec.getLineItemValue('routingstep', 'operationsequence', rountingcount);
                                lastRoutingOperationFld.setDefaultValue(roun_oper);
                                var startOperationFld = nlapiGetField('startoperation');
                                if (startOperationFld) {
                                    startOperationFld.setDisplayType('inline');
                                }
                                if (parseInt(roun_oper) == parseInt(endingoperation)) {
                                    var endOperationFld = nlapiGetField('endoperation');
                                    if (endOperationFld) {
                                        endOperationFld.setDisplayType('inline');
                                    }

                                    var inventoryDetails = nlapiGetFieldValue('custbody_cntm_inventorylot_mapdetail');
                                    if (inventoryDetails) {
                                        inventoryDetails = (inventoryDetails.length == 1) || (inventoryDetails.length == 0) ? "" : inventoryDetails;
                                        if (inventoryDetails) {
                                            var lotAssignmentFld = nlapiGetField('custbody_cntm_lotassignment');
                                            if (lotAssignmentFld) {
                                                lotAssignmentFld.setDisplayType('normal');
                                            }

                                            var completedQuantityFld = nlapiGetField('completedquantity');
                                            if (completedQuantityFld)
                                                completedQuantityFld.setDisplayType('hidden');
                                        } else {
                                            var lotAssignmentFld = nlapiGetField('custbody_cntm_lotassignment');
                                            if (lotAssignmentFld) {
                                                lotAssignmentFld.setDisplayType('disabled');
                                            }
                                        }
                                    } else {
                                        var lotAssignmentFld = nlapiGetField('custbody_cntm_lotassignment');
                                        if (lotAssignmentFld) {
                                            lotAssignmentFld.setDisplayType('disabled');
                                        }
                                    }
                                } else {
                                    var lotAssignmentFld = nlapiGetField('custbody_cntm_lotassignment');
                                    if (lotAssignmentFld) {
                                        lotAssignmentFld.setDisplayType('disabled');
                                    }
                                }
                            }
                        }

                    } else {
                        var lotAssignmentFld = nlapiGetField('custbody_cntm_lotassignment');
                        if (lotAssignmentFld) {
                            lotAssignmentFld.setDisplayType('hidden');
                        }
                        var completedQuantityFld = nlapiGetField('custbody_cntm_complted_qty');
                        if (completedQuantityFld)
                            completedQuantityFld.setDisplayType('hidden');
                    }
                }
            }

        }
    } else if (type == 'view') {

        if (recType == 'itemreceipt') {
            var receiptCount = nlapiGetLineItemCount('item');
            nlapiLogExecution('DEBUG', 'receiptCount', receiptCount);
            for (var lineCount = 1; lineCount <= receiptCount; lineCount++) {
                var customQuantityFld = nlapiGetLineItemField('item', "custcol_cntm_quantity_custom", lineCount);
                var lotAssignmentCheckbox = nlapiGetLineItemField('item', "custcol_cntm_lotassignment", lineCount);
                if (customQuantityFld) {
                    customQuantityFld.setDisplayType('hidden');
                }
                if (lotAssignmentCheckbox)
                    lotAssignmentCheckbox.setDisplayType('hidden');

            }

        } else if (recType == 'workordercompletion') {
            var lotAssignmentFld = nlapiGetField('custbody_cntm_lotassignment');
            if (lotAssignmentFld) {
                lotAssignmentFld.setDisplayType('hidden');
            }
            var inventoryDetailFld = nlapiGetField('inventorydetail');
            if (inventoryDetailFld) {
                inventoryDetailFld.setDisplayType('normal');
            }
        }

    } else if (type == 'create') {

        if (recType == 'workordercompletion') {
            var isBackFlush = nlapiGetFieldValue('isbackflush');
            if (isBackFlush == 'T') {
                var lotAssignmentFld = nlapiGetField('custbody_cntm_lotassignment');
                if (lotAssignmentFld) {
                    lotAssignmentFld.setDisplayType('hidden');
                }
                var completedQuantityFld = nlapiGetField('custbody_cntm_complted_qty');
                if (completedQuantityFld)
                    completedQuantityFld.setDisplayType('hidden');
            }
        }
    }

    //For WOC transaction, create a field to store the isLOTItem flag on Standard WOC form 
    if (recType == 'workordercompletion') {
        var isBackFlush = nlapiGetFieldValue('isbackflush');
        if (isBackFlush == 'T') {
            var assemblyItem = nlapiGetFieldValue('item');
            if (assemblyItem) {
                var isLOTNum = nlapiLookupField('item', assemblyItem, 'islotitem');
                var islotFlag = form.addField('custpage_islotflag', 'text', 'IsLOT');
                islotFlag.setDefaultValue(isLOTNum);
                islotFlag.setDisplayType('hidden');
            }
        }
    }
}

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit
 *                      approve, reject, cancel (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF)
 *                      markcomplete (Call, Task)
 *                      reassign (Case)
 *                      editforecast (Opp, Estimate)
 * @returns {Void}
 */
function userEventBeforeSubmit(type) {
    //nlapiLogExecution('DEBUG', 'context', nlapiGetContext().getExecutionContext());

    if (nlapiGetContext().getExecutionContext() == 'userinterface') {
        if (type == 'edit') {

            var rectype = nlapiGetRecordType();
            nlapiLogExecution('DEBUG', 'rectype beforesubmit', rectype);

            if (rectype == 'itemreceipt') {
                var itemReceipt = nlapiGetNewRecord();
                if (itemReceipt) {
                    var count = itemReceipt.getLineItemCount('item');
                    //	nlapiLogExecution('DEBUG', 'count', count);
                    var lotNumberItemArrayLength = 0;

                    for (var linecount = 1; linecount <= count; linecount++) {
                        itemReceipt.selectLineItem('item', linecount);
                        var customQuantity = itemReceipt.getLineItemValue('item', 'custcol_cntm_quantity_custom', linecount);
                        var mapdescription = itemReceipt.getLineItemValue('item', 'custcol_cntm_mapdescription', linecount);
                        //	nlapiLogExecution('DEBUG', 'linecount', linecount);
                        //	nlapiLogExecution('DEBUG', 'mapdescription', mapdescription);
                        //	nlapiLogExecution('DEBUG', 'customQuantity', customQuantity);

                        itemReceipt.setCurrentLineItemValue('item', 'quantity', customQuantity);
                        itemReceipt.setCurrentLineItemValue('item', 'custcol_cntm_original_item_qty', customQuantity);
                        var inventorydetailavail = itemReceipt.getLineItemValue('item', 'inventorydetailavail', linecount);
                        //	nlapiLogExecution('DEBUG', 'inventorydetailavail', inventorydetailavail);
                        if (mapdescription)
                            mapdescription = (mapdescription.length == 1) || (mapdescription.length == 0) ? '' : mapdescription;
                        if (inventorydetailavail == 'T') {
                            if (mapdescription != null && mapdescription != undefined && mapdescription != '') {
                                var mapvalue = JSON.parse(mapdescription);
                                //	nlapiLogExecution('DEBUG', 'mapvalue', mapvalue);

                                var islotnameavailable = {};
                                var subrecord = itemReceipt.editCurrentLineItemSubrecord('item', 'inventorydetail');
                                //	nlapiLogExecution('DEBUG', 'subrecord', subrecord);
                                if (subrecord != null && subrecord != undefined && subrecord != '') {
                                    var flag = false;
                                    var subReclinecount = subrecord.getLineItemCount('inventoryassignment');
                                    for (var i = subReclinecount; i >= 1; --i) {
                                        subrecord.removeLineItem('inventoryassignment', i);

                                    }

                                    for (var lotno in mapvalue) {
                                        nlapiLogExecution('DEBUG', 'lotno', lotno);
                                        var lotnumberArray = lotno.split('|');
                                        if (lotno != null) {

                                            var maparray = [];
                                            maparray = mapvalue[lotno].split('__');

                                            subrecord.selectNewLineItem('inventoryassignment');
                                            //		nlapiLogExecution('DEBUG', '4', '4');
                                            subrecord.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', lotnumberArray[0]);
                                            subrecord.setCurrentLineItemValue('inventoryassignment', 'quantity', parseFloat(maparray[2]));
                                            subrecord.setCurrentLineItemValue('inventoryassignment', 'binnumber', maparray[0]);
                                            subrecord.setCurrentLineItemValue('inventoryassignment', 'expirationdate', maparray[10]);

                                            subrecord.setCurrentLineItemValue('inventoryassignment', 'inventorystatus', maparray[1]);

                                            //		nlapiLogExecution('DEBUG', '5', '5');
                                            subrecord.commitLineItem('inventoryassignment');
                                            //		nlapiLogExecution('DEBUG', 'commit', 'commit');


                                        }

                                    }
                                    subrecord.commit();

                                } //end of if subrecord
                                else {

                                    var subrecord2 = itemReceipt.createCurrentLineItemSubrecord('item', 'inventorydetail');
                                    //	nlapiLogExecution('DEBUG', 'subrecord2', subrecord2);

                                    for (var lotno in mapvalue) {
                                        //		nlapiLogExecution('DEBUG', 'lotno', lotno);
                                        var lotnumberArray = lotno.split('|');
                                        if (lotno != null) {

                                            var maparray = [];
                                            maparray = mapvalue[lotno].split('__');
                                            subrecord2.selectNewLineItem('inventoryassignment');
                                            subrecord2.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', lotnumberArray[0]);
                                            subrecord2.setCurrentLineItemValue('inventoryassignment', 'quantity', parseFloat(maparray[2]));
                                            subrecord2.setCurrentLineItemValue('inventoryassignment', 'binnumber', maparray[0]);
                                            subrecord2.setCurrentLineItemValue('inventoryassignment', 'expirationdate', maparray[10]);

                                            subrecord2.setCurrentLineItemValue('inventoryassignment', 'inventorystatus', maparray[1]);

                                            //			nlapiLogExecution('DEBUG', '5', '5');
                                            subrecord2.commitLineItem('inventoryassignment');

                                        }

                                    }
                                    subrecord2.commit();
                                }

                            } //end of if (mapdescription)
                            else {
                                var subrecord = itemReceipt.editCurrentLineItemSubrecord('item', 'inventorydetail');
                                //nlapiLogExecution('DEBUG', 'subrecord', subrecord);
                                if (subrecord != null && subrecord != undefined && subrecord != '') {
                                    subrecord.selectLineItem('inventoryassignment', 1);
                                    subrecord.setCurrentLineItemValue('inventoryassignment', 'quantity', parseFloat(customQuantity));
                                    subrecord.commitLineItem('inventoryassignment');
                                }
                                subrecord.commit();
                            }
                            itemReceipt.commitLineItem('item');
                        }

                    }


                }

            } else if (rectype == 'workordercompletion') {
                var wocRecord = nlapiGetNewRecord();
                if (wocRecord) {
                    try {
                        var isbackflush = wocRecord.getFieldValue('isbackflush');
                        nlapiLogExecution('DEBUG', 'isbackflush', isbackflush);
                        if (isbackflush == 'T') {
                            var endOperation = wocRecord.getFieldText('endoperation');
                            var manufactureRouting = wocRecord.getFieldValue('manufacturingrouting');
                            var lastOperation = getRoutingLastOperation(manufactureRouting);
                            nlapiLogExecution('DEBUG', 'lastOperation', lastOperation);
                            var compltedqty = wocRecord.getFieldValue('custbody_cntm_complted_qty');
                            nlapiLogExecution('DEBUG', 'compltedqty', compltedqty);
                            if (compltedqty) {
                                try {
                                    wocRecord.setFieldValue('completedquantity', compltedqty);
                                    if (lastOperation) {
                                        if (parseInt(endOperation) == parseInt(lastOperation))
                                            wocRecord.setFieldValue('quantity', compltedqty);
                                    }
                                } catch (e) {
                                    nlapiLogExecution('ERROR', 'e.message', e.message);
                                }

                                var mapdata = wocRecord.getFieldValue('custbody_cntm_inventorylot_mapdetail');
                                if (mapdata)
                                    mapdata = (mapdata.length == 1) || (mapdata.length == 0) ? '' : mapdata;
                                if (mapdata) {

                                    mapdata = JSON.parse(mapdata);
                                    if (Object.keys(mapdata).length > 0) {
                                        var woSubrecord1 = wocRecord.editSubrecord('inventorydetail');
                                        if (woSubrecord1) {
                                            var linecount = woSubrecord1.getLineItemCount('inventoryassignment');
                                            for (var i = linecount; i >= 1; --i) {
                                                woSubrecord1.removeLineItem('inventoryassignment', i);

                                            }

                                            for (var lotno in mapdata) {
                                                try {
                                                    nlapiLogExecution('DEBUG', 'woSubrecord1->lotno', lotno);
                                                    var lotnumberArray = lotno.split('|');
                                                    if (lotno != null) {

                                                        var maparray = [];
                                                        maparray = mapdata[lotno].split('__');
                                                        woSubrecord1.selectNewLineItem('inventoryassignment');
                                                        //		nlapiLogExecution('DEBUG', '4', '4');
                                                        woSubrecord1.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', lotnumberArray[0]);
                                                        woSubrecord1.setCurrentLineItemValue('inventoryassignment', 'quantity', parseFloat(maparray[2]));
                                                        woSubrecord1.setCurrentLineItemValue('inventoryassignment', 'binnumber', maparray[0]);
                                                        woSubrecord1.setCurrentLineItemValue('inventoryassignment', 'expirationdate', maparray[10]);

                                                        woSubrecord1.setCurrentLineItemValue('inventoryassignment', 'inventorystatus', maparray[1]);

                                                        nlapiLogExecution('DEBUG', '5', '5');
                                                        woSubrecord1.commitLineItem('inventoryassignment');

                                                    }
                                                } catch (e) {
                                                    nlapiLogExecution('ERROR', 'e.message', e.message);
                                                }
                                            }
                                            woSubrecord1.commit();
                                        } else {
                                            var woSubrecord2 = wocRecord.createSubrecord('inventorydetail');
                                            for (var lotno in mapdata) {
                                                try {
                                                    nlapiLogExecution('DEBUG', 'woSubrecord2->lotno', lotno);
                                                    var lotnumberArray = lotno.split('|');
                                                    if (lotno != null) {

                                                        var maparray = [];
                                                        maparray = mapdata[lotno].split('__');
                                                        woSubrecord2.selectNewLineItem('inventoryassignment');
                                                        woSubrecord2.setCurrentLineItemValue('inventoryassignment', 'receiptinventorynumber', lotnumberArray[0]);
                                                        woSubrecord2.setCurrentLineItemValue('inventoryassignment', 'quantity', parseFloat(maparray[2]));
                                                        woSubrecord2.setCurrentLineItemValue('inventoryassignment', 'binnumber', maparray[0]);
                                                        woSubrecord2.setCurrentLineItemValue('inventoryassignment', 'expirationdate', maparray[10]);

                                                        woSubrecord2.setCurrentLineItemValue('inventoryassignment', 'inventorystatus', maparray[1]);

                                                        //		nlapiLogExecution('DEBUG', '5', '5');
                                                        woSubrecord2.commitLineItem('inventoryassignment');

                                                    }
                                                } catch (e) {
                                                    nlapiLogExecution('ERROR', 'e.message', e.message);
                                                }


                                            }
                                            woSubrecord2.commit();
                                        }
                                    }
                                }
                            }
                        }
                    } catch (e) {
                        nlapiLogExecution('ERROR', 'e.message', e.message);
                    }

                }
            }
        }
    }

}

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Operation types: create, edit, delete, xedit,
 *                      approve, cancel, reject (SO, ER, Time Bill, PO & RMA only)
 *                      pack, ship (IF only)
 *                      dropship, specialorder, orderitems (PO only) 
 *                      paybills (vendor payments)
 * @returns {Void}
 */
function itemreceiptLothistoryAfter(type) {
    nlapiLogExecution('DEBUG', 'context', nlapiGetContext().getExecutionContext());
    if (type == 'create' || type == 'edit') {
        var rectype = nlapiGetRecordType();
        //nlapiLogExecution('DEBUG', 'rectype', rectype);
        if (rectype == 'itemreceipt') {

            var id = nlapiGetRecordId();
            nlapiLogExecution('DEBUG', 'After submit(id)', id);
            var record;

            record = nlapiLoadRecord('itemreceipt', id);

            if (record != null && record != undefined) {
                var vendor = record.getFieldValue('entity');
                //	nlapiLogExecution('DEBUG', 'vendor', vendor);

                var getlotnumbers = {};
                var lineCount = record.getLineItemCount('item');
                for (var ctr = 1; ctr <= lineCount; ctr++) {
                    try {

                        record.selectLineItem('item', ctr);
                        var itemname = record.getCurrentLineItemValue('item', 'item');
                        //	nlapiLogExecution('DEBUG', 'itemname', itemname);
                        var unit = record.getCurrentLineItemValue('item', 'units');
                        var unittype = nlapiLookupField('item', itemname, 'unitstype');
                        var stockunit = nlapiLookupField('item', itemname, 'stockunit');
                        //	nlapiLogExecution('DEBUG', 'stockunit', stockunit);

                        var data = record.getCurrentLineItemValue('item', 'custcol_cntm_mapdescription');
                        if (data)
                            data = (data.length == 1) || (data.length == 0) ? '' : data;
                        //	nlapiLogExecution('DEBUG', 'data', data);
                        if (data != null && data != '' && data != undefined) {
                            var mapvalue = JSON.parse(data);
                            var updatedLOTArray = updateLOT(mapvalue, itemname);
                            //        nlapiLogExecution('DEBUG', 'Library Response', updatedLOTArray);
                        } //


                    } catch (e) {
                        nlapiLogExecution('DEBUG', 'e.message', e.message);
                    }
                } //end of outer for loop

            } //end of if(record)

        } //end of rectype=itemreceipt
        else if (rectype == 'workordercompletion') {
            var wocId = nlapiGetRecordId();
            //	nlapiLogExecution('DEBUG', 'After WOC submit(id)', wocId);
            try {
                var wocRecord = nlapiLoadRecord('workordercompletion', wocId);

                if (wocRecord != null && wocRecord != undefined) {
                    var itemname = wocRecord.getFieldValue('item');
                    //		nlapiLogExecution('DEBUG', 'itemname', itemname);
                    var inventoryDetailMap = wocRecord.getFieldValue('custbody_cntm_inventorylot_mapdetail');
                    if (inventoryDetailMap)
                        inventoryDetailMap = (inventoryDetailMap.length == 1) || (inventoryDetailMap.length == 0) ? '' : inventoryDetailMap;
                    if (inventoryDetailMap) {
                        inventoryDetailMap = JSON.parse(inventoryDetailMap);
                        if (Object.keys(inventoryDetailMap).length > 0) {
                            var updatedLOTArray = updateLOT(inventoryDetailMap, itemname);
                            nlapiLogExecution('DEBUG', 'Library Response', updatedLOTArray);
                        }
                    }
                }
            } catch (e) {
                nlapiLogExecution('DEBUG', 'e.message', e.message);
            }

        }
    } //end of if(type=create)
}

function getRoutingLastOperation(manufactureRouting) {
    if (manufactureRouting) {
        var manufactroutingrec = nlapiLoadRecord('manufacturingrouting', manufactureRouting);
        if (manufactroutingrec != null && manufactroutingrec != undefined && manufactroutingrec != '') {
            var rountingcount = manufactroutingrec.getLineItemCount('routingstep');
            if (rountingcount) {
                var roun_oper = manufactroutingrec.getLineItemValue('routingstep', 'operationsequence', rountingcount);
                return roun_oper;
            }
        }
    }
    return '';
}