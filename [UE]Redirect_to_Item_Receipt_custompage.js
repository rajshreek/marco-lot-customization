/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       28 May 2019     Rajshree Kakane
 *
 */

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 *   
 * @param {String} type Operation types: create, edit, view, copy, print, email
 * @param {nlobjForm} form Current form
 * @param {nlobjRequest} request Request object
 * @returns {Void}
 */
function redirectToItemReceipt(type, form, request) {

    if (type == 'create' &&
        nlapiGetContext().getExecutionContext() == "userinterface" &&
        nlapiGetFieldValue("ordertype") == "PurchOrd") {
        var po = nlapiGetFieldValue('createdfrom');
        if (po) {
            var item_array = [];
            var polinecnt = nlapiGetLineItemCount('item');
            for (var itr = 1; itr <= polinecnt; itr++) {
                var itemid = nlapiGetLineItemValue('item', 'item', itr);
                item_array.push(itemid);
            }
            /*var checklotitemSearch = nlapiSearchRecord("item",null,
            		[
            		   ["internalid","anyof",item_array], 
            		   "AND", 
            		   ["islotitem","is","T"]
            		], 
            		[
            		   new nlobjSearchColumn("internalid")
            		]
            		);
            if(checklotitemSearch)*/
            {
                nlapiSetRedirectURL('SUITELET',
                    'customscript_cntm_receipt_customform',
                    "customdeploy_cntm_receipt_customform", null, {
                        "poid": nlapiGetFieldValue('createdfrom')
                    });
            }
        }

    } else if (type == 'create' &&
        (nlapiGetContext().getExecutionContext() == "userinterface") &&
        (nlapiGetFieldValue("dbstrantype") == "WorkOrd") && (nlapiGetFieldValue("isbackflush") == 'T')) {
        var wo = nlapiGetFieldValue('createdfrom');
        nlapiLogExecution('DEBUG', 'wo', wo);
        var assemblyItem = nlapiGetFieldValue('item');
        nlapiLogExecution('DEBUG', 'assemblyItem', assemblyItem);
        var checklotitemSearch = nlapiSearchRecord("item", null,
            [
                ["internalid", "anyof", assemblyItem],
                "AND",
                ["islotitem", "is", "T"]
            ],
            [
                new nlobjSearchColumn("internalid")
            ]
        );
        if (checklotitemSearch) {

            nlapiSetRedirectURL('SUITELET',
                'customscript_cntm_woc_customform',
                "customdeploy_cntm_woc_customform", null, {
                    "woid": wo
                });
        }
    }

}