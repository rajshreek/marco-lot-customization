/**
 * Module Description
 * 
 * Version    Date            Author           Remarks
 * 1.00       04 Jun 2019     Yogesh Jagdale
 *
 */
var checkSelfLifeFlag = 'F';
var childwin = null;

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType 
 * 
 * @param {String} type Access mode: create, copy, edit
 * @returns {Void}
 */

function clientPageInit(type) {

	if(type=="copy" && nlapiGetRecordType()=='salesorder'){
      var count=nlapiGetLineItemCount('item');

for(var i=1;i<=count;i++){
debugger;
nlapiSelectLineItem('item',i); 
nlapiSetCurrentLineItemValue('item','custcol_cntm_mapdescription',"");
nlapiCommitLineItem('item');

}
    }
    debugger;
    var itemId = new URL(location.href).searchParams.get('itemname');
    if (localStorage.getItem('windowOpened') && itemId) {
    	
        var lotSelectionMap = sessionStorage.getItem('lotSelectionMap');
        //if(lotSelectionMap && lotSelectionMap!='undefined'){
        if (confirm('It seems you have already opened this Popup in background. Click ok to close this Popup or Cancel to continue working.')) {
            window.close();
            localStorage.removeItem('windowOpened');
            //}
        }
    } else if (localStorage.getItem('windowOpened') && itemId == undefined) {
        localStorage.removeItem('windowOpened');
    }
    var textArea=document.getElementsByName('custpage_approved_vendor_display');
    if(textArea && textArea.length>0){
		if(document.getElementsByName('custpage_approved_vendor_display')[0].defaultValue=="<Type & tab for single value>"){
    		document.getElementsByName('custpage_approved_vendor_display')[0].defaultValue='';
    	}
	}
    if (itemId) {
        jQuery('#custpage_closebtn').click(function() {
            localStorage.removeItem('windowOpened');
           // window.close();
        });
        if (jQuery('#custpage_clearbtn').length > 0) {
            try {
                showInformationTitle('Information', '1. Do not refresh this pop-up. <br/>2. Hit the Cancel or Submit button to close this pop-up.<br/>', 30000);
            } catch (e2) {
                nlapiLogExecution('ERROR', 'Loading error', e2.message);
            }
        }
        localStorage.setItem('windowOpened', true);
    }
    var checkfld = nlapiGetField('custpage_itemname');
    var hiddencheckfld = nlapiGetField('custpage_hidden_checkfieldid');
    if (checkfld != null && checkfld != undefined && checkfld != '') {
        addStockPageInitFunction();
    } else if (hiddencheckfld != null && hiddencheckfld != undefined && hiddencheckfld != '') {
        //selectLOTPopupPageInit();
    }

    //added code for refreshing Item Receipt Custom Page
    refreshItemReceiptCustomPage();

    debugger;
    var itemId = new URL(location.href).searchParams.get('itemname');
    var lotSelectionMap = sessionStorage.getItem('lotSelectionMap');
    if (lotSelectionMap && lotSelectionMap != 'undefined') {
        lotSelectionMap = JSON.parse(lotSelectionMap);
        var qtyselected = 0;
       //Added a new code to resolve the usage Limit Exceeded Error
        var lotNumerLineCount=nlapiGetLineItemCount('custpage_lotnumberitem');
        var filterArray=[];
        var inventoryNumberIndex={};
        if(lotNumerLineCount!=0 && lotNumerLineCount!=-1){
        	for(lineIndex1=1;lineIndex1<=lotNumerLineCount;lineIndex1++){
            	var lotNumber = nlapiGetLineItemValue("custpage_lotnumberitem", "custpage_inventorynumber", lineIndex1);
                if(lotNumber){
                	inventoryNumberIndex[lotNumber]=lineIndex1;
                	filterArray.push(["inventorynumber","is",lotNumber]);
    				filterArray.push("OR");
                }
            }
            filterArray.pop("OR");
        }
        if(filterArray.length>0){
        	var lotNumberSearch=getAllLOTNumberID(filterArray, itemId);
        	if(lotNumberSearch){
        		for (var cnt = 0; cnt < lotNumberSearch.length; cnt++) {
					var lotNumberID=lotNumberSearch[cnt].getId();
					var inventorynumber=lotNumberSearch[cnt].getValue('inventorynumber');					
					if (lotSelectionMap[lotNumberID]) {
	                    var temp = lotSelectionMap[lotNumberID];
	                    var quantity = lotSelectionMap[lotNumberID];
	                    if (quantity) {
	                        qtyselected += parseFloat(quantity);
	                        nlapiSetLineItemValue("custpage_lotnumberitem", "custpage_quantityslected", inventoryNumberIndex[inventorynumber], quantity);
	                        nlapiSetLineItemValue('custpage_lotnumberitem', 'custpage_checkbox', inventoryNumberIndex[inventorynumber], 'T');

	                    }
	                }
        		}
        	}
        }
        //end of the code
        for (i = 1; i <= nlapiGetLineItemCount('custpage_lotnumberitem'); i++) {
            var lotNumberText = nlapiGetLineItemValue("custpage_lotnumberitem", "custpage_inventorynumber", i);
            var binText= nlapiGetLineItemValue("custpage_lotnumberitem", "custpage_bin_new", i);
            if(lotNumberText){
            	//commented the code for Usage Limit Exceeded Error.
            	/*var lotNumber = updatePopup(lotNumberText, itemId);
                if (lotSelectionMap[lotNumber]) {
                    var temp = lotSelectionMap[lotNumber];
                    var quantity = lotSelectionMap[lotNumber];
                    if (quantity) {
                        qtyselected += parseFloat(quantity);
                        nlapiSetLineItemValue("custpage_lotnumberitem", "custpage_quantityslected", i, quantity);
                        nlapiSetLineItemValue('custpage_lotnumberitem', 'custpage_checkbox', i, 'T');

                    }
                }*/
            }else if(binText){
            	if (lotSelectionMap['Bin__'+binText]) {
                    var quantity = lotSelectionMap['Bin__'+binText];
                    if (quantity) {
                        qtyselected += parseFloat(quantity);
                        nlapiSetLineItemValue("custpage_lotnumberitem", "custpage_quantityslected", i, quantity);
                        nlapiSetLineItemValue('custpage_lotnumberitem', 'custpage_checkbox', i, 'T');

                    }
                }
            }
            
        }
        var requiredQty = parseFloat(nlapiGetFieldValue('custpage_requirqty'))
       /* if (requiredQty >= qtyselected) {
            nlapiSetFieldValue('custpage_selectedqty', qtyselected);
        }*/
        nlapiSetFieldValue('custpage_selectedqty', qtyselected);
        sessionStorage.setItem('lotSelectionMap', '');
    }
}


function updatePopup(lotnumber, itemId) {
    var itemSearch = nlapiSearchRecord("inventorynumber", null,
        [
            ["inventorynumber", "startswith", lotnumber],
            "AND",
            ["item", 'anyof', itemId]
        ],
        [new nlobjSearchColumn("internalid")]
    );
    if (itemSearch) {
        return itemSearch[0].getId();
    } else {
        return "";
    }
}
function getAllLOTNumberID(lotNumberFilterArray,itemId){
	if(lotNumberFilterArray){
		var lotnumberNetsuiteRecord=nlapiSearchRecord('inventorynumber', null, 
				[
				 	["item","is",itemId],
				 	"AND",
				 	lotNumberFilterArray], 
				 	[new nlobjSearchColumn("internalid"),
				 	new nlobjSearchColumn("inventorynumber")]);
		if(lotnumberNetsuiteRecord)
		{
			return lotnumberNetsuiteRecord;
		}else{
			return '';
		}
	}
}
/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 * 
 * @param {String} type Sublist internal id
 * @param {String} name Field internal id
 * @param {Number} linenum Optional line item number, starts from 1
 * @returns {Void}
 */
function clientFieldChanged(type, name, linenum) {
    debugger;
    if (name == "custbody_cntm_lotassignment") {
        var isBackFlush = nlapiGetFieldValue('isbackflush');
        if (isBackFlush == 'T') {
            var itemname = nlapiGetFieldValue('item');
            var compltedqty = nlapiGetFieldValue('custbody_cntm_complted_qty');
            var createdfrom = nlapiGetFieldValue('createdfrom');
            var location = nlapiGetFieldValue('location');
            var transactionType='';
            if(nlapiGetRecordType()=='workordercompletion'){
            	transactionType=nlapiGetRecordType();
            }
            if (compltedqty != null && compltedqty != undefined && compltedqty != '') {
                var lotassignment = nlapiGetFieldValue('custbody_cntm_lotassignment');
                if (lotassignment == 'T') {
                    var salesunitId = nlapiGetFieldValue('units');
                    var salesunitText = nlapiGetFieldText('units');

                    var url = nlapiResolveURL('SUITELET', 'customscript_cntm_addstock_lot_popup', 'customdeploy_cntm_addstock_lot_popup') + '&itemname=' + itemname + '&quantity=' + compltedqty + '&salesunit=' + salesunitId + '&unit=' + salesunitText + '&loc=' + location +'&woc='+transactionType+ '&description=';
                    childwin = window.open(url, "_blank", "top=1,left=1");
                    if (!childwin || childwin.closed || typeof childwin.closed == 'undefined') {
                        alert('Pop-ups window is blocked in your browser.');
                        nlapiSetFieldValue('custbody_cntm_lotassignment', false);
                    } else {
                        childwin.resizeTo(1380, 730);
                    }
                    nlapiSetFieldValue('custbody_cntm_lotassignment', 'F', false);
                }
            } else {
                alert("Please enter the completed quantity");
            }
        }
    }
    if (name == 'custbody_cntm_inventorylot_mapdetail') {
        var wocLotDetails = nlapiGetFieldValue('custbody_cntm_inventorylot_mapdetail');
        wocLotDetails=(wocLotDetails.length==1)||(wocLotDetails.length==0)?'':wocLotDetails;
        
        if (wocLotDetails) {
        	wocLotDetails = JSON.parse(wocLotDetails);
            if (Object.keys(wocLotDetails).length > 0) {
                nlapiSetFieldValue('custbody_cntm_lotassignment', true, false);
            }
        }
    }
    if (name == 'custpage_scrapqty' || name == 'custpage_compltqty') {
        var scrapqty = nlapiGetFieldValue('custpage_scrapqty');
        var completedqty = nlapiGetFieldValue('custpage_compltqty');
        if(completedqty){
        	var woBuildQty=nlapiGetFieldValue('custpage_buildqty');
        	var woQty=nlapiGetFieldValue('custpage_woqty');
        	var overBuilds=nlapiGetFieldValue('custpage_overbuilds');
        	if(overBuilds=='T'){
        		completedQtyFunctionality(completedqty,scrapqty,name);
        	}else if(overBuilds=='F'){
        		var remainingBuildQty=parseFloat(woQty)-parseFloat(woBuildQty);
        		if(parseFloat(remainingBuildQty)>0 && (parseFloat(completedqty)<=parseFloat(remainingBuildQty)))
        		{
        			completedQtyFunctionality(completedqty,scrapqty,name);
        		}else{
        			alert('You can not enter complted quantity greater than the WO quantity.');
        			nlapiSetFieldValue('custpage_compltqty', '', false);
        		}
        	}
        	
        }
        
    }
    if (name == 'custpage_endoperation') {
        var endingoperation = nlapiGetFieldValue('custpage_endoperation');
        var lastOperation = nlapiGetFieldValue('custpage_hiddenendop');
        var startOperation = nlapiGetFieldValue('custpage_startoperation');
        if (endingoperation) {
	    	 if(parseInt(startOperation)>parseInt(endingoperation)){
	         	alert('The start operation must be less than or equal to the end operation');
	         	//nlapiSetFieldValue('custpage_startoperation', '', false);
	         	nlapiSetFieldValue('custpage_endoperation', '',false);
	         }else{
	        	 var compltedQtyfield = nlapiGetField('custpage_compltqty');
	             if (compltedQtyfield != null && compltedQtyfield != undefined && compltedQtyfield != '') {
	                 compltedQtyfield.setDisplayType('normal');
	             }
	         }
            
        } else {
        	var completedQty=nlapiGetFieldValue('custpage_compltqty');
        	if(completedQty){
        		nlapiSetFieldValue('custpage_compltqty', '',false);
        		nlapiSetFieldValue('custpage_scrapqty', '',false);
        		var linecount = nlapiGetLineItemCount('custpage_componentlist');
                if (linecount != 0 && linecount != '' && linecount != null) {
                	for (var compCount = 1; compCount <= linecount; compCount++) {
                		 nlapiSetLineItemValue('custpage_componentlist', 'custpage_quantity', compCount, '');
                		 nlapiSetLineItemValue('custpage_componentlist', 'custpage_compo_lotassignment', compCount, 'F');
                		 nlapiSetLineItemValue('custpage_componentlist', 'custpage_complotdetails', compCount, '');
                	}   
                }
        	}
        		
            var compltedQtyfield = nlapiGetField('custpage_compltqty');
            if (compltedQtyfield != null && compltedQtyfield != undefined && compltedQtyfield != '') {
                compltedQtyfield.setDisplayType('disabled');
            }
        }
        var finishedLOTDetails=nlapiGetFieldValue('custpage_lotdetailsmap');
        finishedLOTDetails = finishedLOTDetails ? finishedLOTDetails.trim() : finishedLOTDetails;
        if(finishedLOTDetails){
        	if(confirm('The INVENTORY DETAIL will be lost. Do you want to continue?')){
                nlapiSetFieldValue('custpage_inventorydetail', 'F', false);
                nlapiSetFieldValue('custpage_lotdetailsmap', '', false);
        	}else{
        		nlapiSetFieldValue('custpage_endoperation', lastOperation, false);
        	}
        }
        
      
        if (parseInt(endingoperation) == parseInt(lastOperation)) {
            nlapiGetField('custpage_scrapqty').setDisplayType('normal');
            var lotassignfield = nlapiGetField('custpage_inventorydetail');
            if (lotassignfield != null && lotassignfield != undefined && lotassignfield != '') {
                lotassignfield.setDisplayType('normal');
            }

        } else {
        	nlapiSetFieldValue('custpage_scrapqty', '',false);
            nlapiGetField('custpage_scrapqty').setDisplayType('disabled');
            var lotassignfield = nlapiGetField('custpage_inventorydetail');
            if (lotassignfield != null && lotassignfield != undefined && lotassignfield != '') {
                lotassignfield.setDisplayType('disabled');
            }
        }
        checkComponetOperationSequence(endingoperation);

    }
    if (name == 'custpage_startoperation') {
    	var startOperation=nlapiGetFieldValue('custpage_startoperation');
        var endingop = nlapiGetFieldValue('custpage_endoperation');
        if(parseInt(startOperation)>parseInt(endingop)){
        	alert('The start operation must be less than or equal to the end operation');
        	nlapiSetFieldValue('custpage_startoperation', '', false);
        }else{
        	checkComponetOperationSequence(endingop);
        }
        
    }
    if (type == 'custpage_lotnumberitem' && name == 'custpage_quantity') {
        var compltedqty = nlapiGetCurrentLineItemValue('custpage_lotnumberitem', 'custpage_quantity');
        if (parseFloat(compltedqty) <= 0) {
            alert('Quantity cannot be negative or 0');
            nlapiSetCurrentLineItemValue('custpage_lotnumberitem', 'custpage_quantity', '', false);
        }else{
        	if (compltedqty.toString().indexOf('.') != -1) {
                if (compltedqty.toString().split('.')[1].length > 5) {
                    alert('Quantity can not have more than 5 decimal places.');
                    nlapiSetCurrentLineItemValue('custpage_lotnumberitem', 'custpage_quantity', '', false);
                }
            }
        }
    }
    if (type == 'custpage_lotnumberitem' && name == "custpage_quantityslected") {
        var onHandQty = parseFloat(nlapiGetCurrentLineItemValue("custpage_lotnumberitem", "custpage_quantityslected"));
        var availQty = parseFloat(nlapiGetLineItemValue("custpage_lotnumberitem", "custpage_quantity_avail", linenum));
        var Qty = parseFloat(nlapiGetLineItemValue("custpage_lotnumberitem", "custpage_quantity", linenum));
        if (isNaN(parseFloat(onHandQty)) || parseFloat(parseFloat(onHandQty)) < 1 || (parseFloat(parseFloat(onHandQty)) <= Qty)) {
            if (parseFloat(onHandQty) <= 0) {
               // nlapiSetLineItemValue('custpage_lotnumberitem', 'custpage_checkbox', linenum, 'F');
                nlapiSetCurrentLineItemValue("custpage_lotnumberitem", "custpage_quantityslected", "",false);
              nlapiSetCurrentLineItemValue("custpage_lotnumberitem", "custpage_checkbox", "F",false);
                
                alert('Invalid quantity entered.');
            }else{
            	var checkDecimalValues=false;
            	if (onHandQty.toString().indexOf('.') != -1) {
            		if (onHandQty.toString().split('.')[1].length > 5) {
            			checkDecimalValues=true;
                        alert('Quantity can not have more than 5 decimal places.');
                        nlapiSetCurrentLineItemValue("custpage_lotnumberitem", "custpage_quantityslected", "",false);
                    }else{
                    	checkDecimalValues=false;
                    }
                }else{
                	checkDecimalValues=false;
                }
            	if(checkDecimalValues==false){
            		 if (parseFloat(onHandQty) > availQty) {
                         nlapiSetLineItemValue('custpage_lotnumberitem', 'custpage_checkbox', linenum, 'F');
                         nlapiSetCurrentLineItemValue("custpage_lotnumberitem", "custpage_quantityslected", "");
                         alert('Selected Quantity must be less than the Available quantity.');
                     } else {
                         //alert('Invalid On Hand Quantity, please re-enter the quantity.');
                         var qtyselected = 0;
                         for (i = 1; i <= nlapiGetLineItemCount('custpage_lotnumberitem'); i++) {
                             var selectedQty = nlapiGetLineItemValue("custpage_lotnumberitem", "custpage_quantityslected", i);
                             if (selectedQty != "") {
                                 qtyselected += parseFloat(selectedQty);
                             }
                         }
                         var requiredQty = parseFloat(nlapiGetFieldValue('custpage_requirqty'))
                         if (requiredQty >= qtyselected) {
                             if (onHandQty) {
                                 nlapiSetLineItemValue('custpage_lotnumberitem', 'custpage_checkbox', linenum, 'T');
                             } else {
                                 nlapiSetLineItemValue('custpage_lotnumberitem', 'custpage_checkbox', linenum, 'F');
                             }
                             nlapiSetFieldValue('custpage_selectedqty', parseFloat(qtyselected));
                         } else {
                         	var wocField=nlapiGetField('custpage_woctransaction');
                             if(wocField){
                             	alert('Selected Quantity must match with Required Quantity.');
                             }else{
                             	 alert('Selected Quantity must be lesser than the Required Quantity.');
                             }
                             nlapiSetLineItemValue('custpage_lotnumberitem', 'custpage_checkbox', linenum, 'F');
                             nlapiSetCurrentLineItemValue("custpage_lotnumberitem", "custpage_quantityslected", "");
                         }
                     }
            	}
            	
            }
           
        } else {
            onHandQty == "" ? nlapiSetLineItemValue('custpage_lotnumberitem', 'custpage_checkbox', linenum, 'F') : nlapiSetLineItemValue('custpage_lotnumberitem', 'custpage_checkbox', linenum, 'T')
            var qtyselected = 0;
            for (i = 1; i <= nlapiGetLineItemCount('custpage_lotnumberitem'); i++) {
                var selectedQty = nlapiGetLineItemValue("custpage_lotnumberitem", "custpage_quantityslected", i);
                if (selectedQty != "") {
                    qtyselected += parseFloat(selectedQty);
                }
            }
            var requiredQty = parseFloat(nlapiGetFieldValue('custpage_requirqty'));
            var Qty = parseFloat(nlapiGetLineItemValue("custpage_lotnumberitem", "custpage_quantity", linenum));
            if (Qty < onHandQty) {
                nlapiSetLineItemValue('custpage_lotnumberitem', 'custpage_checkbox', linenum, 'F');
                nlapiSetCurrentLineItemValue("custpage_lotnumberitem", "custpage_quantityslected", "");
                alert("Quantity entered can't be more than Available Quantity.");
            } else if (requiredQty >= qtyselected) {
                nlapiSetFieldValue('custpage_selectedqty', parseFloat(qtyselected));
            } else {
                nlapiSetLineItemValue('custpage_lotnumberitem', 'custpage_checkbox', linenum, 'F');
                nlapiSetCurrentLineItemValue("custpage_lotnumberitem", "custpage_quantityslected", "");
                alert('Please enter quantity to match the Required Quantity.');
            }
        }

    }
    /*if (type=='custpage_lotnumberitem' && name == "custpage_checkbox") {
		//	nlapiSelectLineItem('custpage_lotnumberitem', linenum);
		var checkbox = nlapiGetLineItemValue('custpage_lotnumberitem', 'custpage_checkbox', linenum);
		//  var checkbox = nlapiGetCurrentLineItemValue('custpage_lotnumberitem', 'custpage_checkbox');
		if (checkbox == 'T') {
			var qtyselected = parseFloat(nlapiGetCurrentLineItemValue("custpage_lotnumberitem", "custpage_quantityslected"));
			if(isNaN(qtyselected)){
				alert('Please enter Quantity before checking the checkbox.');
				nlapiSetLineItemValue('custpage_lotnumberitem', 'custpage_checkbox', linenum,'F');
			}else{

				var selectedQty = parseFloat(nlapiGetFieldValue('custpage_selectedqty'));
				var onHandQty = parseFloat(nlapiGetLineItemValue("custpage_lotnumberitem", "custpage_quantityslected", linenum));
				var Qty = parseFloat(nlapiGetLineItemValue("custpage_lotnumberitem", "custpage_quantity", linenum));
				var requiredQty = parseFloat(nlapiGetFieldValue('custpage_requirqty'));
				if(onHandQty>Qty){
					alert('Quantity on hand must be lesser than the Total Quantity.');
					nlapiSetLineItemValue('custpage_lotnumberitem', 'custpage_checkbox', linenum,'F');
				}else{
					if(requiredQty<(selectedQty+qtyselected)){
						nlapiSetLineItemValue('custpage_lotnumberitem', 'custpage_checkbox', linenum,'F');
						nlapiSetLineItemValue("custpage_lotnumberitem", "custpage_quantityslected", linenum,'');
						alert('Quantity must be lesser than the Required Quantity.');

					}else{
						nlapiSetFieldValue('custpage_selectedqty', (selectedQty + qtyselected));
						var remainQty = parseFloat(nlapiGetFieldValue('custpage_remainqty'));
						nlapiSetFieldValue('custpage_remainqty', (remainQty - qtyselected));
					}
				}
			}
		} else {
			var qtyselected = parseFloat(nlapiGetLineItemValue("custpage_lotnumberitem", "custpage_quantityslected", linenum));
			var selectedQty = parseFloat(nlapiGetFieldValue('custpage_selectedqty'));

			nlapiSetFieldValue('custpage_selectedqty', (selectedQty - qtyselected));
			var remainQty = parseFloat(nlapiGetFieldValue('custpage_remainqty'));
			nlapiSetFieldValue('custpage_remainqty', (remainQty + qtyselected));
		}
	} //end of if(name == "custpage_checkbox")
*/
    if (type == 'item' && name == "quantity") {
        /*
              debugger;
              var rectype = nlapiGetRecordType();
              
              	if (rectype == 'itemreceipt') {
                var qty=nlapiGetCurrentLineItemValue('item','quantity');
                  var qty=nlapiSetCurrentLineItemValue('item','custcol_cntm_quantity_custom',qty);
                }
            */
    }
    if (type == 'item' && name == "custcol_cntm_quantity_custom") {
        var quantity = nlapiGetCurrentLineItemValue('item', 'quantity');
        var customQuantity = nlapiGetCurrentLineItemValue('item', 'custcol_cntm_quantity_custom');
        if (customQuantity.toString().indexOf('.') != -1) {
            if (customQuantity.toString().split('.')[1].length > 5) {
                alert('Quantity can not have more than 5 decimal places.');
                nlapiSetCurrentLineItemValue('item', 'custcol_cntm_quantity_custom', quantity, false);
            }
        }
    }
    if (type == 'custpage_itemsublist' && name == "custpage_lotassignmentcheckbox") {

        var rectype = nlapiGetRecordType();
        var location;
        var locationfld = nlapiGetField('location');
        var createdfrom = nlapiGetFieldValue('custpage_createdfrom');
        var vendorId = nlapiGetFieldValue('custpage_vendor');
        var compltedqty = nlapiGetLineItemValue('custpage_itemsublist', 'custpage_quantity', linenum);
        var vendorCode = nlapiGetLineItemValue('custpage_itemsublist', 'custpage_vendorcode', linenum);

        if (locationfld != null && locationfld != '' && locationfld != undefined)
            location = nlapiGetFieldValue('location');
        else
            location = nlapiGetLineItemValue('custpage_itemsublist', 'custpage_location', linenum);

        if (location != null && location != '' && location != undefined) {
            var itemname = nlapiGetLineItemValue('custpage_itemsublist', 'custpage_itemfield', linenum);
            if (itemname != null && itemname != '' && itemname != undefined) {
            	var useBinFlag=nlapiLookupField('item', itemname, 'usebins');
            	var isLotItem=nlapiLookupField('item', itemname, 'islotitem');
            	if(useBinFlag=='T' || isLotItem=='T'){
            		 var lotassignment = nlapiGetLineItemValue('custpage_itemsublist', 'custpage_lotassignmentcheckbox', linenum);
                     if (lotassignment == 'T') {
                         if (compltedqty > 0) {
                             var lotassignmentmap = nlapiGetLineItemValue('custpage_itemsublist', 'custpage_itemwisepagedata', linenum);
                             if(lotassignmentmap){
                            	 lotassignmentmap=(lotassignmentmap.length==1)||(lotassignmentmap.length==0)?"":lotassignmentmap;                	
                             }
                             if(!lotassignmentmap){
                            	setupWarningForShelfLife(itemname);
                             }
                             var salesunitId = nlapiGetLineItemValue('custpage_itemsublist', 'custpage_salesunitid', linenum);
                             var salesunitText = nlapiGetLineItemValue('custpage_itemsublist', 'custpage_salesunittext', linenum);
                             var url = nlapiResolveURL('SUITELET', 'customscript_cntm_addstock_lot_popup', 'customdeploy_cntm_addstock_lot_popup') + '&itemname=' + itemname + '&quantity=' + compltedqty + '&salesunit=' + salesunitId + '&unit=' + salesunitText + '&venid=' + vendorId + '&vencode=' + vendorCode + '&loc=' + location + '&description=' + lotassignmentmap;

                             childwin = window.open(url, "_blank", "top=1,left=1");

                             if (!childwin || childwin.closed || typeof childwin.closed == 'undefined') {
                                 alert("Popup blocked");
                             } else {
                                 childwin.resizeTo(1380, 730);
                             }
                             nlapiSetCurrentLineItemValue('custpage_itemsublist', 'custpage_lotassignmentcheckbox', false, false);
                         } else {
                             alert('Quantity should not be 0');
                             nlapiSetCurrentLineItemValue('custpage_itemsublist', 'custpage_lotassignmentcheckbox', false, false);
                         }
                     }
            	}else{
            		alert('You cannot configure the Inventory details for this selected Item.');
            		nlapiSetCurrentLineItemValue('custpage_itemsublist', 'custpage_lotassignmentcheckbox', false, false);
            	}
               
            } else {
                alert('Please select Item.');
                nlapiSetCurrentLineItemValue('custpage_itemsublist', 'custpage_lotassignmentcheckbox', false, false);
            }
        } else {
            alert('Please select Location field');
            nlapiSetCurrentLineItemValue('custpage_itemsublist', 'custpage_lotassignmentcheckbox', false, false);
            //nlapiSetCurrentLineItemValue('item', 'custcol_cntm_lotassignment', false);
        }


    } //end of item receipt lot assignment checkbox
    if (type == 'custpage_itemsublist' && name == "custpage_itemwisepagedata") {
        nlapiSetCurrentLineItemValue('custpage_itemsublist', 'custpage_checkbox', 'T', false);
        nlapiSetCurrentLineItemValue('custpage_itemsublist', 'custpage_lotassignmentcheckbox', 'T', false);
    }
    if (type == 'item' && name == "custcol_cntm_lotassignment") {
    	var rectype = nlapiGetRecordType();
        //  alert(rectype);
        var location;
        var locationfld = nlapiGetField('location');
        if (locationfld != null && locationfld != '' && locationfld != undefined) {
            location = nlapiGetFieldValue('location');
            //  alert(location);
        }

        var vendorId = nlapiGetFieldValue('entity');
        var vendorCode = nlapiGetLineItemValue('item', 'vendorname', linenum);
        if (location != null && location != '' && location != undefined) {
            var itemname = nlapiGetLineItemValue('item', 'item', linenum);
            if (itemname != null && itemname != '' && itemname != undefined) {
                var islotitemSearch = nlapiSearchRecord("item", null,
                    [
                        [[["islotitem","is","T"]],"OR",[["usebins","is","T"]]], 
                        "AND",
                        [["internalid", "anyof", itemname]]
                    ],
                    [
                        new nlobjSearchColumn("itemid").setSort(false)
                    ]
                );
                
                if (islotitemSearch != null && islotitemSearch != '' && islotitemSearch != undefined) {
                    var description = '';

                    description = nlapiGetLineItemValue('item', 'custcol_cntm_mapdescription', linenum);
                    var lotassignment = nlapiGetCurrentLineItemValue('item', 'custcol_cntm_lotassignment');
                    if (lotassignment == 'T') {
                        var lotSelectionMap = nlapiGetLineItemValue('item', 'custcol_cntm_mapdescription', linenum);
                        if (lotSelectionMap) {
                            sessionStorage.setItem('lotSelectionMap', lotSelectionMap);
                        }
                        var customer = nlapiGetFieldValue('entity');
                        var quantity;
                        var salesunit;
                        var unittext;
                        if (rectype == 'itemreceipt') {
                        	quantity = nlapiGetLineItemValue('item', 'custcol_cntm_quantity_custom', linenum);
                            unittext = nlapiGetLineItemValue('item', 'unitsdisplay', linenum);
                            salesunit = nlapiGetLineItemValue('item', 'units', linenum);

                            var url = nlapiResolveURL('SUITELET', 'customscript_cntm_addstock_lot_popup', 'customdeploy_cntm_addstock_lot_popup') + '&itemname=' + itemname + '&quantity=' + quantity + '&salesunit=' + salesunit + '&unit=' + unittext + '&venid=' + vendorId + '&vencode=' + vendorCode + '&loc=' + location + '&description=' + description;

                            childwin = window.open(url, "_blank", "top=1,left=1");

                            if (!childwin || childwin.closed || typeof childwin.closed == 'undefined') {
                                alert("Popup blocked");
                            } else {
                                childwin.resizeTo(1380, 730);
                            }

                        } else {
                        	var isLotItem=nlapiLookupField('item', itemname, 'islotitem');
                        	if(isLotItem=='T'){
                        		var recid = nlapiGetRecordId();
                                if (lotSelectionMap == "" && recid != "") {
                                    nlapiSelectLineItem('item', linenum);
                                    var subrecord = nlapiViewCurrentLineItemSubrecord('item', 'inventorydetail');
                                    if (subrecord != null && subrecord != undefined && subrecord != '') {
                                        var map = {};
                                        var lineCount = subrecord.getLineItemCount('inventoryassignment');
                                        for (s = 1; s <= lineCount; s++) {

                                            subrecord.selectLineItem('inventoryassignment', s);
                                            var lotnumberid = subrecord.getCurrentLineItemValue('inventoryassignment', 'issueinventorynumber');
                                            var qtyOnHand = subrecord.getCurrentLineItemValue('inventoryassignment', 'quantity');
                                            map[lotnumberid] = qtyOnHand || 0;
                                        }
                                        //description=;
                                    }
                                    sessionStorage.setItem('lotSelectionMap', JSON.stringify(map));
                                }
                                var inventoryLocation = nlapiGetLineItemValue('item', 'location', linenum);
                                //alert(inventoryLocation);
                                if (inventoryLocation) {
                                    quantity = nlapiGetLineItemValue('item', 'quantity', linenum);
                                    unittext = nlapiGetLineItemValue('item', 'units_display', linenum);
                                    salesunit = nlapiGetLineItemValue('item', 'units', linenum);
                                    shelfLife = nlapiGetLineItemValue('item', 'custcol_mr_customer_shelf_life', linenum);
                                    var customer=nlapiGetFieldValue('entity');

                                    var url = nlapiResolveURL('SUITELET', 'customscript_cntm_lotnumbers_selection', 'customdeploy_cntm_lotnumbers_selection') + '&itemname=' + itemname + '&customer='+customer+ '&quantity=' + quantity + '&salesunit=' + salesunit + '&unit=' + unittext + '&venid=' + vendorId + '&vencode=' + vendorCode + '&loc=' + inventoryLocation + '&description=' + description + '&shelfLife=' + shelfLife;
                                    debugger;

                                    childwin = window.open(url, "_blank", "top=1,left=1");

                                    if (!childwin || childwin.closed || typeof childwin.closed == 'undefined') {
                                        alert("Popup blocked");
                                    } else {
                                        childwin.resizeTo(1380, 730);
                                    }
                                } else {
                                    alert('Please select Location on line ' + linenum);
                                }
                        	}else{
                        		 alert("You can not assign lots to this selected item. Please select lot numbered item for Lot Assignment");
                                 nlapiSetCurrentLineItemValue('item', 'custcol_cntm_lotassignment', false, false);
                        	}
                        }
                        nlapiSetCurrentLineItemValue('item', 'custcol_cntm_lotassignment', 'F');
                    }
                } else {
                    alert("You can not assign lots to this selected item. Please select lot numbered item for Lot Assignment");
                    nlapiSetCurrentLineItemValue('item', 'custcol_cntm_lotassignment', false, false);
                }
            } else {
                alert('Please select Item.');
                nlapiSetCurrentLineItemValue('item', 'custcol_cntm_lotassignment', false, false);
            }
        } else {
            alert('Please select Location field');
            nlapiSetCurrentLineItemValue('item', 'custcol_cntm_lotassignment', false, false);
        }

    }

    if (name == "custpage_inventorydetail") {
        var location = nlapiGetFieldValue('custpage_location');
        var createdfrom = nlapiGetFieldValue('custpage_createdfrom');
        var vendorId = '';
        var compltedqty = nlapiGetFieldValue('custpage_compltqty');
        var woctransaction=nlapiGetFieldValue('custpage_transaction');
        
        var vendorCode = '';
        if (location != null && location != '' && location != undefined) {
            var itemname = nlapiGetFieldValue('custpage_assemblyitem');
            if (itemname != null && itemname != '' && itemname != undefined) {
                var lotassignment = nlapiGetFieldValue('custpage_inventorydetail');
                if (lotassignment == 'T') {
                    if (compltedqty) {
                        if (compltedqty > 0) {
                        	var componentFlag=checkComponentsLOTInformation();
                        	if(componentFlag==true){
                        		var lotassignmentmap = nlapiGetFieldValue('custpage_lotdetailsmap');
                                var salesunitId = nlapiGetFieldValue('custpage_units');
                                var salesunitText = nlapiGetFieldText('custpage_units');
                                
                                //find the components country
                                var assemblyLOTCountry='';
                                var checkduplicateCountry={};
                                var componentCountryMap=nlapiGetFieldValue('custpage_countrymap');
                                if(componentCountryMap)
                                	componentCountryMap=(componentCountryMap.length==1)||(componentCountryMap.length==0)?'':componentCountryMap;
                                if(componentCountryMap){
                                	componentCountryMap=JSON.parse(componentCountryMap);
                                	for ( var compkey in componentCountryMap) {
                                		var itemCountryMap=componentCountryMap[compkey];
                                		for ( var itemcontrykey in itemCountryMap) {
                                			if(!checkduplicateCountry[itemcontrykey]){
                                				checkduplicateCountry[itemcontrykey]=itemCountryMap[itemcontrykey];
                                			}
                                		}
                                	}
                                	if(Object.keys(checkduplicateCountry).length==1){
                                		for ( var countrykey in checkduplicateCountry) {
                                			assemblyLOTCountry=checkduplicateCountry[countrykey];
										}
                                	}	
                                }
                                //get the less expiry date from components
                                var expirationDate='';
                                var expirationDateMR='';
                                var expiryArray=[];
                                var checkduplicateExpDate={};
                                var expiryMap=nlapiGetFieldValue('custpage_expirationmap');
                              	if(expiryMap)
                                	expiryMap=(expiryMap.length==1)||(expiryMap.length==0)?'':expiryMap;
                                if(expiryMap){
                                	expiryMap=JSON.parse(expiryMap);
                                	for ( var expkey in expiryMap) {
                                		var itemExpirationMap=expiryMap[expkey];
                                		for ( var itemexpkey in itemExpirationMap) {
                                			if(!checkduplicateExpDate[itemexpkey]){
                                				checkduplicateExpDate[itemexpkey]=itemExpirationMap[itemexpkey];
                                				expiryArray.push(itemexpkey);
                                			}
                                		}
                                	}
                                	if(expiryArray.length>0){
                                		var expiryDateStd=expiryArray[0];
                                		for (var arrCount = 1; arrCount < expiryArray.length; ++arrCount) {
											if(new Date(expiryArray[arrCount])<new Date(expiryDateStd)){
												expiryDateStd=expiryArray[arrCount];
											}
										}
                                		expirationDateMR=checkduplicateExpDate[expiryDateStd];
                            			expirationDate=expiryDateStd;
                                     // alert(expirationDateMR+','+expirationDate);
                                	}
                                	
                                }
                                var url = nlapiResolveURL('SUITELET', 'customscript_cntm_addstock_lot_popup', 'customdeploy_cntm_addstock_lot_popup') + '&itemname=' + itemname + '&quantity=' + compltedqty + '&salesunit=' + salesunitId + '&unit=' + salesunitText + '&venid=' + vendorId + '&vencode=' + vendorCode + '&loc=' + location +'&woc='+woctransaction+'&origin='+assemblyLOTCountry+'&expstd='+expirationDate+'&expmr='+expirationDateMR+ '&description=' + lotassignmentmap;

                                var childwin = window.open(url, "_blank", "top=1,left=1");

                                if (!childwin || childwin.closed || typeof childwin.closed == 'undefined') {
                                    alert("Popup blocked");
                                } else {
                                    childwin.resizeTo(1380, 730);
                                }
                                nlapiSetFieldValue('custpage_inventorydetail', 'F', false);
                        	}else if(componentFlag==false){
                        		alert('Please configure the inventory details for components before configuration of assembly item.');
                                nlapiSetFieldValue('custpage_inventorydetail', 'F', false);
                        	}
                            
                        } else {
                            alert('Completed Quantity should be positive non-zero integer.');
                            nlapiSetFieldValue('custpage_inventorydetail', 'F', false);
                        }
                    } else {
                        alert('Please enter the completed quantity');
                        nlapiSetFieldValue('custpage_inventorydetail', 'F', false);
                    }

                }
            } else {
                alert('Please select Item.');
                nlapiSetCurrentLineItemValue('custpage_itemsublist', 'custpage_lotassignmentcheckbox', false, false);
                //nlapiSetCurrentLineItemValue('item', 'custcol_cntm_lotassignment', false);
            }
        } else {
            alert('Please select Location field');
            nlapiSetCurrentLineItemValue('custpage_itemsublist', 'custpage_lotassignmentcheckbox', false, false);
            //nlapiSetCurrentLineItemValue('item', 'custcol_cntm_lotassignment', false);
        }


    }
    if (name == 'custpage_lotdetailsmap') {
        var lotDetails = nlapiGetFieldValue('custpage_lotdetailsmap');
        lotDetails=(lotDetails.length==1)||(lotDetails.length==0)?'':lotDetails;
        if (lotDetails) {
            lotDetails = JSON.parse(lotDetails);
            if (Object.keys(lotDetails).length > 0) {
                nlapiSetFieldValue('custpage_inventorydetail', true, false);
            }
        }
    }
    if (type == 'custpage_componentlist' && name == 'custpage_compo_lotassignment') {
    	var woctransaction=nlapiGetFieldValue('custpage_transaction');
        
        var location = nlapiGetFieldValue('custpage_location');
        if (location != null && location != '' && location != undefined) {
            var itemname = nlapiGetLineItemValue('custpage_componentlist', 'custpage_component', linenum);
            if (itemname != null && itemname != '' && itemname != undefined) {
            	var usebins=nlapiLookupField('item', itemname, 'usebins');
            	var islot=nlapiLookupField('item', itemname, 'islotitem');
                var islotitemSearch = nlapiSearchRecord("item", null,
                    [
                       
                        ["internalid", "anyof", itemname]
                    ],
                    [
                        new nlobjSearchColumn("itemid").setSort(false),
                        new nlobjSearchColumn("stockunit")
                    ]
                );
                if (islot=='T' || usebins=='T') {

                    var description = nlapiGetLineItemValue('custpage_componentlist', 'custpage_complotdetails', linenum);
                    description = description ? description.trim() : description;
                    if (description) {
                        sessionStorage.setItem('lotSelectionMap', description);
                    }

                    var lotassignment = nlapiGetCurrentLineItemValue('custpage_componentlist', 'custpage_compo_lotassignment');
                    if (lotassignment == 'T') {
                        var quantity = nlapiGetLineItemValue('custpage_componentlist', 'custpage_quantity', linenum);
                        var unittext = islotitemSearch[0].getText('stockunit');
                        var salesunit = islotitemSearch[0].getValue('stockunit');

                        var url = nlapiResolveURL('SUITELET', 'customscript_cntm_lotnumbers_selection', 'customdeploy_cntm_lotnumbers_selection') + '&location=' + location + '&itemname=' + itemname + '&quantity=' + quantity + '&salesunit=' + salesunit + '&unit=' + unittext +'&woc='+woctransaction+ '&description=' + description;

                        if (quantity > 0) {
                            nlapiSetCurrentLineItemValue('custpage_componentlist', 'custpage_compo_lotassignment', 'F');
                            var childwin = window.open(url, "_blank", "top=1,left=1");
                            if (!childwin || childwin.closed || typeof childwin.closed == 'undefined') {
                                alert("Popup blocked");
                            } else {
                                childwin.resizeTo(1380, 730);
                            }
                            if(description){
                            	//nlapiSetCurrentLineItemValue('custpage_componentlist', 'custpage_compo_lotassignment', 'T', false);
                            }
                        } else {
                            nlapiSetCurrentLineItemValue('custpage_componentlist', 'custpage_compo_lotassignment', 'F');
                            alert('Please enter Quantity to specify Inventory Detail.');
                        }
                    }
                } else {
                    alert("You can not assign lots to this selected item.Please select lot numbered item for Lot Assignment");
                    nlapiSetCurrentLineItemValue('custpage_componentlist', 'custpage_compo_lotassignment', false, false);
                }
            } else {
                alert('Please select Item.');
                nlapiSetCurrentLineItemValue('custpage_componentlist', 'custpage_compo_lotassignment', false, false);
            }
        } else {
            alert('Please select Location.');
            nlapiSetCurrentLineItemValue('custpage_componentlist', 'custpage_compo_lotassignment', false, false);
        }
    }
    if (type == 'custpage_componentlist' && name == 'custpage_complotdetails') {
        var componetLOTDetails = nlapiGetLineItemValue('custpage_componentlist', 'custpage_complotdetails', linenum);
        if (componetLOTDetails) {
            componetLOTDetails = JSON.parse(componetLOTDetails);
            if (Object.keys(componetLOTDetails).length > 0) {
                nlapiSetCurrentLineItemValue('custpage_componentlist', 'custpage_compo_lotassignment', 'T', false);
            }
        }else{
        	 nlapiSetCurrentLineItemValue('custpage_componentlist', 'custpage_compo_lotassignment', 'F', false);
        }
    }
    if (type == 'custpage_itemsublist' && name == 'custpage_quantity') {
        var receiptItemQty = nlapiGetLineItemValue('custpage_itemsublist', 'custpage_quantity', linenum);
        if (receiptItemQty > 0) {
        	var checkDecimalValues=false;
        	if (receiptItemQty.toString().indexOf('.') != -1) {
        		if (receiptItemQty.toString().split('.')[1].length > 5) {
        			checkDecimalValues=true;
                    alert('Quantity can not have more than 5 decimal places.');
                    nlapiSetCurrentLineItemValue('custpage_itemsublist', 'custpage_quantity', "",false);
                }else{
                	checkDecimalValues=false;
                }
            }else{
            	checkDecimalValues=false;
            }
        	if(checkDecimalValues==false){
        		var receiptRemaminingItemQty = nlapiGetLineItemValue('custpage_itemsublist', 'custpage_remainingqty', linenum);
                //alert(receiptRemaminingItemQty);
                var overreceipt=nlapiGetFieldValue('custpage_overreceipts');
                if(overreceipt=='F'){
                	if (parseFloat(receiptItemQty) > parseFloat(receiptRemaminingItemQty)) {
                        alert('You can not entered quantity greater than the PO quantity.');
                        nlapiSetCurrentLineItemValue('custpage_itemsublist', 'custpage_quantity', '', false);
                    }else{
                    	var receiveMap=nlapiGetCurrentLineItemValue('custpage_itemsublist', 'custpage_itemwisepagedata');
                    	receiveMap=(receiveMap.length==1)||(receiveMap.length==0)?'':receiveMap;
                    	if(receiveMap){
                    		alert('Please configure the inventory details for item to be received.');
                    		nlapiSetCurrentLineItemValue('custpage_itemsublist', 'custpage_lotassignmentcheckbox', 'F', false);
                    	}
                    }
                }else{
                	var originalReceiveQty=nlapiGetCurrentLineItemValue('custpage_itemsublist', 'custpage_originalreceiveqty');
                	if(parseFloat(originalReceiveQty)!=parseFloat(receiptItemQty)){
                		var receiveMap=nlapiGetCurrentLineItemValue('custpage_itemsublist', 'custpage_itemwisepagedata');
                    	receiveMap=(receiveMap.length==1)||(receiveMap.length==0)?'':receiveMap;
                    	if(receiveMap){
                    		alert('Please configure the inventory details for item to be received.');
                    		nlapiSetCurrentLineItemValue('custpage_itemsublist', 'custpage_lotassignmentcheckbox', 'F', false);
                    	}
                	}
                }
        	}
        }
        else{
    			alert('Quantity should be positive non-zero integer');
    			nlapiSetCurrentLineItemValue('custpage_itemsublist', 'custpage_quantity','',false);
        	}

    }
    if (type == 'custpage_lotnumberitem' && name == 'custpage_curedate') {
        var cureDate = nlapiGetCurrentLineItemValue('custpage_lotnumberitem', 'custpage_curedate');
        if (cureDate) {

            if (cureDate.length == 4) {

                var quarterString = cureDate.substring(0, 2);
                var updatedQuater = quarterString[1].toUpperCase();
                cureDate = quarterString[0] + '' + updatedQuater + '' + cureDate.substring(2, 4);

            }
            var validator = new FieldValidator('custpage_curedate', 'Cure Date', cureDate);
            if (!validator.verifyCureDate().validated) {
                alert(validator.verifyCureDate().errorMsg);
                nlapiSetCurrentLineItemValue('custpage_lotnumberitem', 'custpage_curedate', '', false);
            } else {
            	
                nlapiSetCurrentLineItemValue('custpage_lotnumberitem', 'custpage_curedate', cureDate, false);
            }
        }
    }
    if (type == 'custpage_lotnumberitem' && name == 'custpage_inventorynumber') {
        // alert('inside');
        var itemID = nlapiGetFieldValue('custpage_itemname');
        //alert(itemID);
        var serialLOTNum = nlapiGetCurrentLineItemValue('custpage_lotnumberitem', 'custpage_inventorynumber');
        //var serialLOTNum=nlapiGetLineItemValue('custpage_lotnumberitem', 'custpage_inventorynumber', linenum);
        //  alert(serialLOTNum);
        if (serialLOTNum) {
            var checkindex = serialLOTNum.indexOf('|');
            if (checkindex == -1) {
                var inventorynumberSearch = nlapiSearchRecord("inventorynumber", null,
                    [

                        ["inventorynumber", "is", serialLOTNum],
                        "AND",
                        ["item", 'anyof', itemID]
                    ],
                    [
                        new nlobjSearchColumn("quantityavailable"),
                        new nlobjSearchColumn("custitemnumber_cnt_country_of_origin"),
                        new nlobjSearchColumn("custitemnumber_cnt_cure_dt"),
                        new nlobjSearchColumn("expirationdate"),
                        new nlobjSearchColumn("custitemnumber_cnt_mr_exp_dt"),
                        new nlobjSearchColumn("custitemnumber_cnt_manufacturer"),
                        new nlobjSearchColumn("custitemnumber_cnt_mfg_it_numb"),
                        new nlobjSearchColumn("memo"),
                        new nlobjSearchColumn("inventorynumber").setSort(false),
                        new nlobjSearchColumn("custitemnumber_cnt_vendor"),
                        new nlobjSearchColumn("custitemnumber_cnt_vendor_it_numb")
                    ]
                );
                if (inventorynumberSearch && inventorynumberSearch.length > 0) {

                    jQuery('#custpage_lotnumberitem_custpage_vendor_display').prop('disabled', 'disabled');
                    jQuery('#custpage_curedate').prop('disabled', 'disabled');
                    jQuery('#inpt_custpage_country3').prop('disabled', true);
                    jQuery('#inpt_custpage_manufacturer4').prop('disabled', true);
                    jQuery('#custpage_vendornumber').prop('disabled', 'disabled');
                    jQuery('#custpage_mfgitemnum').prop('disabled', 'disabled');
                    jQuery('#inpt_custpage_bin1').prop('disabled', 'disabled');

                    nlapiSetCurrentLineItemValue('custpage_lotnumberitem', 'custpage_country', inventorynumberSearch[0].getValue('custitemnumber_cnt_country_of_origin'), false);
                    nlapiSetCurrentLineItemValue('custpage_lotnumberitem', 'custpage_curedate', inventorynumberSearch[0].getValue('custitemnumber_cnt_cure_dt'), false);
                    nlapiSetCurrentLineItemValue('custpage_lotnumberitem', 'custpage_expdate', inventorynumberSearch[0].getValue('expirationdate'), false);
                    nlapiSetCurrentLineItemValue('custpage_lotnumberitem', 'custpage_expdatemr', inventorynumberSearch[0].getValue('custitemnumber_cnt_mr_exp_dt'), false);
                    nlapiSetCurrentLineItemValue('custpage_lotnumberitem', 'custpage_manufacturer', inventorynumberSearch[0].getValue('custitemnumber_cnt_manufacturer'), false);
                    nlapiSetCurrentLineItemValue('custpage_lotnumberitem', 'custpage_mfgitemnum', inventorynumberSearch[0].getValue('custitemnumber_cnt_mfg_it_numb'), false);
                    nlapiSetCurrentLineItemValue('custpage_lotnumberitem', 'custpage_vendor', inventorynumberSearch[0].getValue('custitemnumber_cnt_vendor'), false);
                    nlapiSetCurrentLineItemValue('custpage_lotnumberitem', 'custpage_vendornumber', inventorynumberSearch[0].getValue('custitemnumber_cnt_vendor_it_numb'), false);
                    var binNumberId = getBinId(serialLOTNum, itemID);
                    //  alert(binNumberId);
                    if (binNumberId)
                        nlapiSetCurrentLineItemValue('custpage_lotnumberitem', 'custpage_bin', binNumberId, false);

                } else {
                    jQuery('#custpage_lotnumberitem_custpage_vendor_display').prop('disabled', false);
                    var wocTranFld=nlapiGetField('custpage_wocfld');
                    if(wocTranFld){
                    	jQuery('#custpage_curedate').prop('disabled', 'disabled');
                        jQuery('#inpt_custpage_country3').prop('disabled', 'disabled');
                    }else{
                    	jQuery('#custpage_curedate').prop('disabled', false);
                        jQuery('#inpt_custpage_country3').prop('disabled', false);
                    }
                    
                    jQuery('#inpt_custpage_manufacturer4').prop('disabled', false);
                    jQuery('#custpage_vendornumber').prop('disabled', false);
                    jQuery('#custpage_mfgitemnum').prop('disabled', false);
                    jQuery('#inpt_custpage_bin1').prop('disabled', false);
                }
            } else {
                alert('You can enter alphabets,numbers,all special characters except "|"');
                nlapiSetCurrentLineItemValue('custpage_lotnumberitem', 'custpage_inventorynumber', '', false);
            }

        }
    }
    if (type == 'custpage_itemsublist' && name == 'custpage_checkbox') {
        var receiveCheckvalue = nlapiGetLineItemValue('custpage_itemsublist', 'custpage_checkbox', linenum);
        // alert(receiveCheckvalue);
        if (receiveCheckvalue == 'F') {
            var itemMapArray = nlapiGetLineItemValue('custpage_itemsublist', 'custpage_itemwisepagedata', linenum);
            itemMapArray = (itemMapArray.length == 1 || itemMapArray.length == 0) ? "" : itemMapArray;
            if (itemMapArray) {
                if (confirm('The details entered for LOT Assignment would be lost.Are you sure you want to discard them?')) {
                    nlapiSetCurrentLineItemValue('custpage_itemsublist', 'custpage_itemwisepagedata', '', false);
                    nlapiSetCurrentLineItemValue('custpage_itemsublist', 'custpage_lotassignmentcheckbox', 'F', false)
                }
            }
        }
    }
    if (name == 'scrapquantity' || name == 'custbody_cntm_complted_qty') {
        var isBackFlush = nlapiGetFieldValue('isbackflush');
        if (isBackFlush == 'T') {
        	var isLOTNUmber=nlapiGetFieldValue('custpage_islotflag');
        	//alert(isLOTNUmber);
        	if(isLOTNUmber=='T'){
        		var scrapqty = nlapiGetFieldValue('scrapquantity');
                
                var completedqty = nlapiGetFieldValue('custbody_cntm_complted_qty');
                if (completedqty != null && completedqty != undefined && completedqty != '') {
                	if(completedqty>0){
                        var totalqty = 0;
                        if (scrapqty != null && scrapqty != undefined && scrapqty != '') {
                        	
                            totalqty = parseFloat(scrapqty) + parseFloat(completedqty);
                        } else {
                            totalqty = parseFloat(completedqty);
                        }
                        if(name=='custbody_cntm_complted_qty'){
                       	 var finishedLOTDetails=nlapiGetFieldValue('custbody_cntm_inventorylot_mapdetail');
                            finishedLOTDetails = finishedLOTDetails ? finishedLOTDetails.trim() : finishedLOTDetails;
                            if(finishedLOTDetails){
                            	alert('Please configure the inventory detail for the assembly item.');
                            	nlapiSetFieldValue('custbody_cntm_lotassignment', 'F', false);
                                //nlapiSetFieldValue('custpage_lotdetailsmap', '', false);
                            }
                       }
                        var linecount = nlapiGetLineItemCount('component');
                        if (linecount != 0 && linecount != '' && linecount != null) {
                            for (var compCount = 1; compCount <= linecount; compCount++) {
                                var quantityper = nlapiGetLineItemValue('component', 'quantityper', compCount);

                                var componentqty = parseFloat(totalqty) * quantityper;
                                nlapiSetLineItemValue('component', 'quantity', compCount, parseFloat(componentqty));
                            }
                        }
                	}else{
                		alert('Invalid Number(must be greater than 0)');
                	}

                } else {
                    alert('Please enter Completed Quantity(Custom).');
                    nlapiSetFieldValue('scrapquantity', '', false);
                }
        	}
            
        }
    }
    if (name == 'endoperation') {
        var isBackFlush = nlapiGetFieldValue('isbackflush');
        if (isBackFlush == 'T') {
        	var isLOTNUmber=nlapiGetFieldValue('custpage_islotflag');
        	//alert(isLOTNUmber);
        	if(isLOTNUmber=='T'){
        		 var endingoperation = nlapiGetFieldText('endoperation');
        		 var hiddenEndOperation=nlapiGetFieldValue('custpage_hiddenendoperation');
        		 var routingSeqMap=nlapiGetFieldValue('custpage_routingsequencemap');
        		 var lastRoutingOperation=nlapiGetFieldValue('custpage_lastroutingoperation');
        		 
        		 var previousEndOpKey,newEndOpKey;
        		 if(endingoperation!=hiddenEndOperation){
        			 if(routingSeqMap){
            			 routingSeqMap=(routingSeqMap.length==1)||(routingSeqMap.length==0)?"":routingSeqMap;
            			 if(routingSeqMap){
            				 routingSeqMap=JSON.parse(routingSeqMap);
            				 for ( var routKey in routingSeqMap) {
    							if(routingSeqMap[routKey]==endingoperation){
    								newEndOpKey=routKey;
    							}
    							else if(routingSeqMap[routKey]==hiddenEndOperation){
    								previousEndOpKey=routKey;
    							}
    								
    						}
            			 }
            		 }
        			 if(newEndOpKey>=previousEndOpKey){
        				 var manufact_routid = nlapiGetFieldValue('manufacturingrouting');
                         if (manufact_routid) {
                             var manufactroutingrec = nlapiLoadRecord('manufacturingrouting', manufact_routid);
                             if (manufactroutingrec != null && manufactroutingrec != undefined && manufactroutingrec != '') {
                                 var rountingcount = manufactroutingrec.getLineItemCount('routingstep');
                                 var roun_oper = manufactroutingrec.getLineItemValue('routingstep', 'operationsequence', rountingcount);
                                 if (parseInt(roun_oper) == parseInt(endingoperation)) {
                                     var lotassignfield = nlapiGetField('custbody_cntm_lotassignment');
                                     if (lotassignfield != null && lotassignfield != undefined && lotassignfield != '') {
                                         lotassignfield.setDisplayType('normal');
                                     }
                                     nlapiSetFieldValue('completedquantity', '0', false);
                                     var completedQuantityFld = nlapiGetField('completedquantity');
                                     if (completedQuantityFld)
                                         completedQuantityFld.setDisplayType('disabled');
                                     var customComplteQty=nlapiGetFieldValue('custbody_cntm_complted_qty');
                                     //alert(customComplteQty);
                                     var linecount = nlapiGetLineItemCount('component');
                                     if (linecount != 0 && linecount != '' && linecount != null) {
                                         for (var compCount = 1; compCount <= linecount; compCount++) {
                                             var quantityper = nlapiGetLineItemValue('component', 'quantityper', compCount);

                                             var componentqty = parseFloat(customComplteQty) * quantityper;
                                             nlapiSetLineItemValue('component', 'quantity', compCount, parseFloat(componentqty));
                                         }
                                     }
                                     
                                 } else {
                                     var lotassignfield = nlapiGetField('custbody_cntm_lotassignment');
                                     if (lotassignfield != null && lotassignfield != undefined && lotassignfield != '') {
                                         lotassignfield.setDisplayType('disabled');
                                     }
                                     var completedQuantityFld = nlapiGetField('completedquantity');
                                     if (completedQuantityFld)
                                         completedQuantityFld.setDisplayType('disabled');
                                    
                                 }
                             }

                         }
        			 }else{
        				 alert('Ending Operation cannot be changed to preceding operation');
        				 nlapiSetFieldText('endoperation', hiddenEndOperation, false);
        				 if(lastRoutingOperation!=endingoperation){
        					 var lotassignfield = nlapiGetField('custbody_cntm_lotassignment');
                             if (lotassignfield != null && lotassignfield != undefined && lotassignfield != '') {
                                 lotassignfield.setDisplayType('disabled');
                             }
        				 }
        			 }
        		 }else{
        			 if(lastRoutingOperation!=endingoperation){
    					 var lotassignfield = nlapiGetField('custbody_cntm_lotassignment');
                         if (lotassignfield != null && lotassignfield != undefined && lotassignfield != '') {
                             lotassignfield.setDisplayType('disabled');
                         }
    				 }
        		 }     		 
                 
        	}
        }

    }
    if(type == 'custpage_componentlist' && name == 'custpage_quantity'){
    	var totalComponentQuantity=0;
    	var completedQuantity=nlapiGetFieldValue('custpage_compltqty');
    	if(completedQuantity)
    		totalComponentQuantity=parseFloat(totalComponentQuantity)+parseFloat(completedQuantity);
    	var scrapQuantity=nlapiGetFieldValue('custpage_scrapqty');
    	if(scrapQuantity)
    		totalComponentQuantity=parseFloat(totalComponentQuantity)+parseFloat(scrapQuantity);
    	var componentQuantity=nlapiGetCurrentLineItemValue('custpage_componentlist', 'custpage_quantity');
    	//alert(componentQuantity);
    	if(componentQuantity){
	    		if(parseFloat(componentQuantity)>=0){
	    			var checkDecimalValues=false;
	            	if (componentQuantity.toString().indexOf('.') != -1) {
	            		if (componentQuantity.toString().split('.')[1].length > 5) {
	            			checkDecimalValues=true;
	                        alert('Quantity can not have more than 5 decimal places.');
	                        nlapiSetCurrentLineItemValue('custpage_componentlist', 'custpage_quantity', "",false);
	                    }else{
	                    	checkDecimalValues=false;
	                    }
	                }else{
	                	checkDecimalValues=false;
	                }
	            	if(checkDecimalValues==false){
	            		 //	if(parseFloat(componentQuantity)==parseFloat(totalComponentQuantity)){
	    	       		var componentLOTMap=nlapiGetCurrentLineItemValue('custpage_componentlist', 'custpage_complotdetails');
	    	       		if(componentLOTMap)
	    	       			componentLOTMap=(componentLOTMap.length==1)||(componentLOTMap.length==0)?'':componentLOTMap;
	    	           	if(componentLOTMap){
	    	           		alert('Please configure the inventory detail in line ' + linenum+' of the component list');
	    	           		nlapiSetCurrentLineItemValue('custpage_componentlist', 'custpage_compo_lotassignment', 'F', false);
	    	           		//nlapiSetCurrentLineItemValue('custpage_componentlist', 'custpage_complotdetails', '', false);
	    	           	}
		    	       	/*}else{
		    	       		alert('Component Quantity should be equal to the total of Completed Quantity and Scrap Quantity.');
		    	       		nlapiSetCurrentLineItemValue('custpage_componentlist', 'custpage_quantity', totalComponentQuantity, false);
		    	       	}*/
	            	}
	       		
	       	}else{
	       		alert("Invalid number (must be at least 0)");
	       		nlapiSetCurrentLineItemValue('custpage_componentlist', 'custpage_quantity', '', false);
	       	}
    	}
    }
}

function getBinId(inventoryNumber, itemId) {
    if (inventoryNumber && itemId) {
        var binSearch = nlapiSearchRecord("item", null,
            [
                ["islotitem", "is", "T"],
                "AND",
                ["inventorynumberbinonhand.inventorynumber", "is", inventoryNumber],
                "AND",
                ["internalid", "anyof", itemId]
            ],
            [new nlobjSearchColumn("binnumber", "inventorynumberbinonhand", "GROUP").setSort(false)]);
        if (binSearch) {
            var binId = binSearch[0].getValue("binnumber", "inventorynumberbinonhand", "GROUP");
            return binId;
        } else
            return '';
    }

}
function completedQtyFunctionality(completedqty,scrapqty,name){
	if (completedqty != null && completedqty != undefined && completedqty != '') {
		if(completedqty>0){
			var totalqty = 0;
	        if (scrapqty != null && scrapqty != undefined && scrapqty != '') {
	        	 if(parseFloat(scrapqty)<0){
	             	alert('Invalid number (must be at least 0)');
	             	nlapiSetFieldValue('custpage_scrapqty', '', false);
	             	totalqty = parseFloat(completedqty);
	             }else{
	            	  totalqty = parseFloat(scrapqty) + parseFloat(completedqty);
	             }
	          
	        } else {
	            totalqty = parseFloat(completedqty);
	        }
	        if(name=='custpage_compltqty'){
	        	 var finishedLOTDetails=nlapiGetFieldValue('custpage_lotdetailsmap');
	             finishedLOTDetails = finishedLOTDetails ? finishedLOTDetails.trim() : finishedLOTDetails;
	             if(finishedLOTDetails){
	             	alert('Please configure the inventory detail for the assembly item.');
	             	nlapiSetFieldValue('custpage_inventorydetail', 'F', false);
	                 //nlapiSetFieldValue('custpage_lotdetailsmap', '', false);
	             }
	        }
	       
	        //alert(totalqty);
	        if(name=='custpage_scrapqty'){
	        	if(parseFloat(scrapqty) && parseFloat(scrapqty)>0){
		        	validateComponentInventoryDetails(totalqty);
		        }
	        }else if(name=='custpage_compltqty'){
	        	if(parseFloat(completedqty) && parseFloat(completedqty)>0){
		        	validateComponentInventoryDetails(totalqty);
		        }
	        } 
	        
		}else{
			 alert('Invalid number (must be greater than 0)');
		     nlapiSetFieldValue('custpage_scrapqty', '', false);
		     nlapiSetFieldValue('custpage_compltqty', '', false);
		}
        
    } else {
        alert('Please enter Completed Quantity.');
        nlapiSetFieldValue('custpage_scrapqty', '', false);
    }
}
function validateComponentInventoryDetails(totalqty){
	var linecount = nlapiGetLineItemCount('custpage_componentlist');
    if (linecount != 0 && linecount != '' && linecount != null) {
        var sequenceArray = getValidOperationSequence();
        for (var compCount = 1; compCount <= linecount; compCount++) {
        	nlapiSelectLineItem('custpage_componentlist', compCount);
            var operation = nlapiGetLineItemValue('custpage_componentlist', 'custpage_operation', compCount);
            var compoLOTdetails=nlapiGetCurrentLineItemValue('custpage_componentlist', 'custpage_complotdetails');
            compoLOTdetails=(compoLOTdetails.length==1)||(compoLOTdetails.length==0)?'':compoLOTdetails;
            if(compoLOTdetails){
            	alert('Please configure the inventory detail in line ' + compCount+' of the component list');
            	nlapiSetCurrentLineItemValue('custpage_componentlist', 'custpage_compo_lotassignment', 'F', false);
            	//nlapiSetCurrentLineItemValue('custpage_componentlist', 'custpage_complotdetails', '', false);
            }
            if (sequenceArray[operation]) {
                var quantityper = nlapiGetLineItemValue('custpage_componentlist', 'custpage_quantityper', compCount);
                var componentqty = parseFloat(totalqty) * quantityper;
                nlapiSetLineItemValue('custpage_componentlist', 'custpage_quantity', compCount, parseFloat(componentqty));
            }
        }
    }
}
function clientSaveRecord() {
    if (localStorage.getItem('windowOpened')) {
        localStorage.removeItem('windowOpened');
    }
    debugger;
    var isCustomIRform = nlapiGetField('custpage_irhiddenfield');
    //	var purchOrdId = nlapiGetFieldValue('custpage_createdfrom');
    var isWOC = nlapiGetFieldValue('custpage_transaction') == 'Work Order Completion' ? true : false;
    if (window.opener != null && !(window.opener.closed)) {
        //alert('Inside window opener');
        var checkfld = nlapiGetField('custpage_itemname');
        var hiddencheckfld = nlapiGetField('custpage_hidden_checkfieldid');
        if (checkfld != null && checkfld != undefined && checkfld != '') {
            addStockPopupSaveRecord();
        } else if (hiddencheckfld != null && hiddencheckfld != undefined && hiddencheckfld != '') {
            selectLOTPopupSaveRecord();
        } else {
            //woc custom page 
            var wocTransactionfld = nlapiGetField('custpage_transaction');
            if (wocTransactionfld != null && wocTransactionfld != undefined && wocTransactionfld != '') {
                //alert('Inside Save');
                var saveFlag = wocCustomFormSaverecord();
                if (saveFlag == true)
                    return true;
                else
                    return false;
            } else{
            	var recordType=nlapiGetRecordType();
            	if(recordType=='workordercompletion'){
            		var wocFlag=wocStandardFormSave();
            		 if (wocFlag == true)
                         return true;
                     else
                         return false;
            	}else{
            		if(recordType=='itemreceipt'){
            			var receiptSaveFlag=itemReceiptStandardRecordSave();
            			if (receiptSaveFlag == true)
                            return true;
                        else
                            return false;
            		}else{
            			return true;
            		}
            		
            	}
            }
        }
    } else if (isCustomIRform != undefined && isCustomIRform != null && isCustomIRform != '' && !isWOC) {
        //		itemReceiptSaveRecord();
        var lineCnt = nlapiGetLineItemCount('custpage_itemsublist');
        var selectedLineCnt = 0;
        for (var lineIndex = 1; lineIndex <= lineCnt; lineIndex++) {
        	nlapiSelectLineItem('custpage_itemsublist', lineIndex);
            var isLineSelected = nlapiGetLineItemValue("custpage_itemsublist", "custpage_checkbox", lineIndex);
            var isInvAdded = nlapiGetLineItemValue("custpage_itemsublist", "custpage_itemwisepagedata", lineIndex);
            isInvAdded = isInvAdded ? isInvAdded.trim() : isInvAdded;
            if (isLineSelected == 'T') {
            	var receiveQty=nlapiGetCurrentLineItemValue('custpage_itemsublist', 'custpage_quantity');
                var receiveItem=nlapiGetCurrentLineItemValue('custpage_itemsublist', 'custpage_itemhiddenid');
               // alert(receiveItem);
                var fieldArray=[];
            	fieldArray.push('usebins');
            	fieldArray.push('islotitem');
            	var fieldValues=nlapiLookupField('item', receiveItem, fieldArray);
            	var useBinFlag=fieldValues['usebins'];
            	var isLotItem=fieldValues['islotitem'];
            	// alert(useBinFlag+'->'+isLotItem);
            	if(useBinFlag=='T' || isLotItem=='T'){
            		if (isInvAdded == undefined || isInvAdded == null || isInvAdded == '') {
                        alert("Please configure the inventory details for items to be received.");
                        return false;
                    } else {
                    	if(receiveQty){
                        	var originalreceiveQty=nlapiGetCurrentLineItemValue('custpage_itemsublist', 'custpage_originalreceiveqty');
                            if(parseFloat(receiveQty)!=parseFloat(originalreceiveQty)){
                            	alert("Please configure the inventory details for items to be received.");
                            	return false;
                            }else{
                            	selectedLineCnt++;
                            }
                        }else{
                        	alert('Please enter the quantity for items to be received.');
                        	return false;
                        }
                        
                    }
            	}else if(useBinFlag=='F' && isLotItem=='F'){
            		if(!receiveQty){
            			alert('Please enter the quantity for items to be received.');
                    	return false;
            		}else{
            			selectedLineCnt++;
            		}
            	}
                
            }
        }
        if (selectedLineCnt == 0) {
            alert('Please select atleast one line to receive.');
            return false;
        } else {
            return true;
        }
    } //end of 
    else {
        var wocTransactionfld = nlapiGetField('custpage_transaction');
        if (wocTransactionfld != null && wocTransactionfld != undefined && wocTransactionfld != '') {
            //alert('Inside Save');
            var saveFlag = wocCustomFormSaverecord();
            if (saveFlag == true)
                return true;
            else
                return false;
        } else{
        	var recordType=nlapiGetRecordType();
        	if(recordType=='workordercompletion'){
        		var wocFlag=wocStandardFormSave();
        		 if (wocFlag == true)
                     return true;
                 else
                     return false;
        	}else{
        		if(recordType=='itemreceipt'){
        			var receiptSaveFlag=itemReceiptStandardRecordSave();
        			if (receiptSaveFlag == true)
                        return true;
                    else
                        return false;
        		}else{
        			return true;
        		}
        		
        	}
        }
            
    }
}

function getfieldvalue(text) {
    nlapiSetFieldValue('custbody_cntm_lotassignmap', text);
    nlapiSetCurrentLineItemValue('custpage_itemsublist', 'custpage_itemwisepagedata', text);
    nlapiSetCurrentLineItemValue('item', 'custcol_cntm_mapdescription', text);
    nlapiCommitLineItem('item');
    nlapiSetFieldValue('custpage_lotdetailsmap', text);
    nlapiSetFieldValue('custbody_cntm_inventorylot_mapdetail', text);

    return false;
}

function getComponentfieldvalue(text) {
    var recType = nlapiGetRecordType();
    if (recType) {
        if (recType = 'salesorder') {
            nlapiSetCurrentLineItemValue('item', 'custcol_cntm_mapdescription', text);
            nlapiSetCurrentLineItemValue('item', 'commitinventory', 1);
            nlapiCommitLineItem('item');
            return false;
        }
    } else {
        //alert('inside');
        nlapiSetCurrentLineItemValue('custpage_componentlist', 'custpage_complotdetails', text);
        return false;
    }
}

function setComponentQuantity(compoQty) {
    nlapiSetCurrentLineItemValue('custpage_componentlist', 'custpage_originalselectedqty', compoQty);
    return false;
}

function closewindow() {
    if (confirm('Changes you made may not be saved. Are you sure you want to close the transaction?')) {
        if (window.onbeforeunload) {
            window.onbeforeunload = function() {
                null;
            };
        };
        //Below code is for Components List Map on custom suitelet
        var compLineCount=window.opener.nlapiGetLineItemCount('custpage_componentlist');
        if(compLineCount!=-1){
        	var componentDetails=window.opener.nlapiGetCurrentLineItemValue('custpage_componentlist', 'custpage_complotdetails');
            componentDetails=(componentDetails.length==1)||(componentDetails.length==0)?'':componentDetails;
            if(componentDetails){
            	window.opener.nlapiSetCurrentLineItemValue('custpage_componentlist', 'custpage_compo_lotassignment','T',false);
            }
        }
        
        //below is for Inventory Details Map on Custom suitelet
        var customMapFld=window.opener.nlapiGetField('custpage_lotdetailsmap');
        if(customMapFld){
        	 var finishedLOTDetails=window.opener.nlapiGetFieldValue('custpage_lotdetailsmap');
             finishedLOTDetails=(finishedLOTDetails.length==1)||(finishedLOTDetails.length==0)?'':finishedLOTDetails;
             
             if(finishedLOTDetails){
             	window.opener.nlapiSetFieldValue('custpage_inventorydetail', 'T', false);
             }
        }
       
        //below is for standard WOC form LOT assignment
        var mapFld=window.opener.nlapiGetField('custbody_cntm_inventorylot_mapdetail');
        if(mapFld){
        	var wocLotMap=window.opener.nlapiGetFieldValue('custbody_cntm_inventorylot_mapdetail');
        	wocLotMap=(wocLotMap.length==1)||(wocLotMap.length==0)?'':wocLotMap;
            if(wocLotMap){
            	window.opener.nlapiSetFieldValue('custbody_cntm_lotassignment', 'T', false);
            } 
        }
        
       
        jQuery('#custpage_closebtn').click(function() {
            localStorage.removeItem('windowOpened');
           // window.close();
        });
        if (localStorage.getItem('windowOpened')) {
            localStorage.removeItem('windowOpened');
            //window.close();
        }
        window.close();
    }
}

function clientValidateLine(type) {
    debugger;
    if (type == 'custpage_lotnumberitem') {
        //validate -Cure Date Field
        var curedate = nlapiGetCurrentLineItemValue(type, 'custpage_curedate');
        //alert(curedate);
        if (curedate) {
            var validator = new FieldValidator('custpage_curedate', 'Cure Date', curedate);
            if (!validator.verifyCureDate().validated) {
                alert(validator.verifyCureDate().errorMsg);
                return false;
            } else {
                //alert(validator.verifyCureDate().value);
                var expDateMR = updateExpirationDate(validator.verifyCureDate().value);
                if (expDateMR == 'UNLIMITED') {
                    nlapiSetCurrentLineItemValue('custpage_lotnumberitem', 'custpage_expdate', '2/22/2222');
                } else {
                    var expDate = validator.getQuarterLastDate(expDateMR).quarterLastDate;
                    nlapiSetCurrentLineItemValue('custpage_lotnumberitem', 'custpage_expdate', expDate);
                }


            }
        }
        //validate- Quantity Field
        var quantity = nlapiGetCurrentLineItemValue(type, 'custpage_quantity');
        if (quantity) {
            var validator = new FieldValidator('custpage_quantity', 'Quantity', quantity);
            if (!validator.verifyNumberFormat().validated) {
                alert(validator.verifyNumberFormat().errorMsg);
                return false;
            }
        }


        //validate- all text field
        var itemId=nlapiGetFieldValue('custpage_itemname');
        if(itemId){
        	var isLOtItem=nlapiLookupField('item', itemId, 'islotitem');
        	if(isLOtItem=='T'){
        		var inventorynumber = nlapiGetCurrentLineItemValue(type, 'custpage_inventorynumber');
                var vendornumber = nlapiGetCurrentLineItemValue(type, 'custpage_vendornumber');
                var mfgitemnum = nlapiGetCurrentLineItemValue(type, 'custpage_mfgitemnum');
                var fieldDetails = {
                    'custpage_inventorynumber': 'Serial/LOT Number' + '__' + inventorynumber
                    //				'custpage_vendornumber':'Vendor Item Number'+'__'+vendornumber,
                    //				'custpage_mfgitemnum':'Manufacturer Item Number'+'__'+mfgitemnum
                }
                for (var lineFieldId in fieldDetails) {
                    var validator = new FieldValidator(lineFieldId, fieldDetails[lineFieldId].split('__')[0], fieldDetails[lineFieldId].split('__')[1]);
                    if (!validator.verifyStringFormat().validated) {
                        alert(validator.verifyStringFormat().errorMsg);
                        return false;
                    }
                }
        	}
        }
      //added new code for CR before UAT-  new lot line to default to the balance to complete the receipt
        /*var requiredQuantity = nlapiGetFieldValue('custpage_quantity');
        var lotQuantity = nlapiGetCurrentLineItemValue(type, 'custpage_quantity');
        var selectedQuantity = nlapiGetFieldValue('custpage_selectedqty');
        var initialLOTQuantity = nlapiGetFieldValue('custpage_initiallotqty');
        
        if (parseFloat(requiredQuantity) >= parseFloat(lotQuantity)) {
        	if(parseFloat(initialLOTQuantity)>0 && parseFloat(selectedQuantity)>0){
            	selectedQuantity = parseFloat(selectedQuantity) - parseFloat(initialLOTQuantity);        
            }
        	selectedQuantity = parseFloat(selectedQuantity) + parseFloat(lotQuantity);
            if (parseFloat(requiredQuantity) >= parseFloat(selectedQuantity)) {
            	nlapiSetFieldValue('custpage_initiallotqty', 0, false);
                nlapiSetFieldValue('custpage_selectedqty', parseFloat(selectedQuantity.toFixed(5)), false);
                
            } else {
                alert('**The total Inventory detail Quantity must be ' + parseFloat(requiredQuantity) + '.');
                return false;
            }
        } else {
            alert('The total Inventory detail Quantity must be ' + parseFloat(requiredQuantity) + '.');
            return false;
        }*/

    }

    return true;
}

function updateExpirationDate(curedate) {
    var itemId = nlapiGetFieldValue('custpage_itemname');
    if (itemId) {
        var shelfLife = nlapiLookupField('item', itemId, 'custitem_mr_shelflifeyears');
        //		var shelfLife=99;
        var expirationDateMR, expireYear;
        if (shelfLife != 'NA' && shelfLife != 99 && shelfLife != '') {
            expireYear = parseInt(curedate.substring(2, 4), 10) + parseInt(shelfLife, 10);
            expirationDateMR = curedate.substring(0, 2) + '' + expireYear;
            nlapiSetCurrentLineItemValue('custpage_lotnumberitem', 'custpage_expdatemr', expirationDateMR);
        } else {
            nlapiSetCurrentLineItemValue('custpage_lotnumberitem', 'custpage_expdatemr', 'UNLIMITED');
            //			alert('The selected item does not have \'Shelf Life\' set on it. ')
            expirationDateMR = 'UNLIMITED';
        }
        return expirationDateMR;
    }
}

function addStockPopupSaveRecord() {

    //alert("Inside add Stock Function");
    var count = nlapiGetLineItemCount("custpage_lotnumberitem");

    var item = nlapiGetFieldValue("custpage_itemname");
    var requiredQuantity = nlapiGetFieldValue("custpage_quantity");
    var totalLOTQuantity = 0;
    var map = {};
    var innermap = {};
    for (var k = 1; k <= count; k++) {
        var lotnumber = nlapiGetLineItemValue("custpage_lotnumberitem", "custpage_inventorynumber", k);
        var bin = nlapiGetLineItemValue("custpage_lotnumberitem", "custpage_bin", k);
        var status = nlapiGetLineItemValue("custpage_lotnumberitem", "custpage_status", k);
        var qtyOnHand = nlapiGetLineItemValue("custpage_lotnumberitem", "custpage_quantity", k);
        var curedt = nlapiGetLineItemValue("custpage_lotnumberitem", "custpage_curedate", k);
        var country = nlapiGetLineItemValue("custpage_lotnumberitem", "custpage_country", k);
        var manufacturer = nlapiGetLineItemValue('custpage_lotnumberitem', 'custpage_manufacturer', k);
        var vendor = nlapiGetLineItemValue('custpage_lotnumberitem', 'custpage_vendor', k);
        var vendItnum = nlapiGetLineItemValue('custpage_lotnumberitem', 'custpage_vendornumber', k);
        var mfgItnum = nlapiGetLineItemValue('custpage_lotnumberitem', 'custpage_mfgitemnum', k);
        var expDateMR = nlapiGetLineItemValue('custpage_lotnumberitem', 'custpage_expdatemr', k);
        var expDateLNR = nlapiGetLineItemValue('custpage_lotnumberitem', 'custpage_expdate', k);
        var memo = nlapiGetLineItemValue('custpage_lotnumberitem', 'custpage_memo', k);

        totalLOTQuantity = parseFloat(totalLOTQuantity) + parseFloat(qtyOnHand);
        var completestring = '';
        completestring = completestring + bin;
        completestring = completestring + '__';
        completestring = completestring + status;
        completestring = completestring + '__';
        completestring = completestring + qtyOnHand;
        completestring = completestring + '__';
        completestring = completestring + curedt;
        completestring = completestring + '__';
        completestring = completestring + country;
        completestring = completestring + '__';
        completestring = completestring + manufacturer;
        completestring = completestring + '__';
        completestring = completestring + vendor;
        completestring = completestring + '__';
        completestring = completestring + vendItnum;
        completestring = completestring + '__';
        completestring = completestring + mfgItnum;
        completestring = completestring + '__';
        completestring = completestring + expDateMR;
        completestring = completestring + '__';
        completestring = completestring + expDateLNR;
        completestring = completestring + '__';
        completestring = completestring + memo;

        map[lotnumber + '|' + k] = completestring;
    }


    var text = JSON.stringify(map);
    //alert(totalLOTQuantity);
    nlapiLogExecution('DEBUG', 'text', map);
    if (parseFloat(totalLOTQuantity).toFixed(5) == parseFloat(requiredQuantity).toFixed(5)) {
        window.opener.getfieldvalue(text);
        window.opener.getTotalLOTQuantity(totalLOTQuantity);
        if (window.onbeforeunload) {
            window.onbeforeunload = function() {
                null;
            };
        };
        window.close();
        return true;
    } else {
        alert('The total Inventory detail Quantity must be ' + parseFloat(requiredQuantity) + '.');
        return false;
    }

}
function getTotalLOTQuantity(totalLOTQuantity){
	nlapiSetFieldValue('custpage_originallotqty', totalLOTQuantity, false);
	if(nlapiGetField('custbody_cntm_original_complte_qty')){
		nlapiSetFieldValue('custbody_cntm_original_complte_qty', totalLOTQuantity, false);
	}
	var receiptLineCount=nlapiGetLineItemCount('custpage_itemsublist');
	if(receiptLineCount!=-1){
		nlapiSetCurrentLineItemValue('custpage_itemsublist', 'custpage_originalreceiveqty', totalLOTQuantity, false);
	}
	var receiptStdLineCnt=nlapiGetLineItemCount('item');
	if(receiptStdLineCnt!=-1 && receiptStdLineCnt!=0){
		nlapiSetCurrentLineItemValue('item', 'custcol_cntm_original_item_qty', totalLOTQuantity, false);
	}
}
function selectLOTPopupSaveRecord() {
    //alert('Inside Select LOT Popup');
    var selectedQty = parseFloat(nlapiGetFieldValue('custpage_selectedqty'));
    var requiredQty = parseFloat(nlapiGetFieldValue('custpage_requirqty'));
    if (selectedQty <= requiredQty) {
        var count = nlapiGetLineItemCount("custpage_lotnumberitem");

        var item = nlapiGetFieldValue('custpage_itemdropdown');
        var location = nlapiGetFieldValue('custpage_location');

        var linenum = nlapiGetFieldValue('custpage_linenum');
        // alert(linenum);
        //nlapiLogExecution('DEBUG', 'count,item,location,linenum', count+','+item+','+location+','+linenum);

        var map = {};
        var outermap = {};
        var innermap = {};
        var countryOfOrigin={};
        var expiryDate={};
        var itemWiseExpirationDetails={};
        var itemWiseCountryOrigin={};
        for (var k = 1; k <= count; k++) {

            var dd = nlapiGetLineItemValue("custpage_lotnumberitem", "custpage_checkbox", k);

            //			custpage_itemwisepagedata
            if (dd == 'T') {


                var lotnumber = nlapiGetLineItemValue("custpage_lotnumberitem", "custpage_inventorynumber", k);
                var bin = nlapiGetLineItemValue("custpage_lotnumberitem", "custpage_bin_new", k);
                var status = nlapiGetLineItemValue("custpage_lotnumberitem", "custpage_status", k);
                var qtyOnHand = nlapiGetLineItemValue("custpage_lotnumberitem", "custpage_quantityslected", k);
                var curedt = nlapiGetLineItemValue("custpage_lotnumberitem", "custpage_curedate", k);
                var country = nlapiGetLineItemValue("custpage_lotnumberitem", "custpage_country", k);
                var manufacturer = nlapiGetLineItemValue('custpage_lotnumberitem', 'custpage_manufacturer', k);
                var vendor = nlapiGetLineItemValue('custpage_lotnumberitem', 'custpage_vendor', k);
                var vendItnum = nlapiGetLineItemValue('custpage_lotnumberitem', 'custpage_vendornumber', k);
                var mfgItnum = nlapiGetLineItemValue('custpage_lotnumberitem', 'custpage_mfgitemnum', k);
                var expDateMR=nlapiGetLineItemValue('custpage_lotnumberitem', 'custpage_expdatemr', k);
                var expDate=nlapiGetLineItemValue('custpage_lotnumberitem', 'custpage_expdate', k);
                
                //Populating the country on assembly item LOT popup
                if(country)
                	countryOfOrigin[country]=country;
               // alert(expDate+','+expDateMR);
                if(expDate && expDateMR!='NA')
                	expiryDate[expDate]=expDateMR;
                else
                	expiryDate['2/22/2222']='UNLIMITED';
                nlapiLogExecution('DEBUG', 'lotnumber,qtyOnHand', lotnumber + ',' + qtyOnHand);
                if(lotnumber && item){
                	var itemSearch = nlapiSearchRecord("inventorynumber", null,
                            [
                                ["inventorynumber", "is", lotnumber],
                                "AND",
                                ["item", 'anyof', item]
                            ],
                            [new nlobjSearchColumn("internalid")]
                        );
                        if (itemSearch) {
                            var lotnumberid = itemSearch[0].getId();
                            map[lotnumberid] = qtyOnHand;
                        }
                }else if(bin){
                	map['Bin__'+bin] = qtyOnHand;
                }
            }
        }
        var text = "";
        if (Object.keys(map).length > 0) {
            text = JSON.stringify(map);
        }
        //alert('text'+text);
        var wocField=nlapiGetField('custpage_woctransaction');
        if(wocField){
        	var wocTransaction=nlapiGetFieldValue('custpage_woctransaction');
        	if(parseFloat(selectedQty).toFixed(8) == parseFloat(requiredQty).toFixed(8)){
        		window.opener.getComponentfieldvalue(text);
                window.opener.setComponentQuantity(requiredQty); //set Qty to hidden field for WOC validation
                
                //Logic for populating the Expiry date on finished LOT 
                
                itemWiseExpirationDetails[item]=expiryDate;
                itemWiseCountryOrigin[item]=countryOfOrigin;
                window.opener.setCountryOfOrigin(JSON.stringify(itemWiseCountryOrigin));
                window.opener.setExpirationDateWithMRdate(JSON.stringify(itemWiseExpirationDetails));
                if (window.onbeforeunload) {
                    window.onbeforeunload = function() {
                        null;
                    };
                };

                window.close();
                return true;
        	}else{
        		alert("The total inventory detail quantity must be " + requiredQty);
        	}
        }else{
        	window.opener.getComponentfieldvalue(text);
            window.opener.setComponentQuantity(requiredQty); //set Qty to hidden field for WOC validation
            if (window.onbeforeunload) {
                window.onbeforeunload = function() {
                    null;
                };
            };

            window.close();
            return true;
        }
        
    } else if (!isNaN(selectedQty)) {
        alert("The total inventory detail quantity cannot be greater than " + requiredQty);
        return false;
    } else {
        window.close();
        return true;
    }
}
function setCountryOfOrigin(countryMap){
	var countryOriginMap=nlapiGetFieldValue('custpage_countrymap');
	countryOriginMap=(countryOriginMap.length==0)||(countryOriginMap.length==1)?'':countryOriginMap;
	if(countryOriginMap){
		countryOriginMap=JSON.parse(countryOriginMap);
		countryMap=JSON.parse(countryMap);
		if(Object.keys(countryMap).length>0){
			for ( var countrykey in countryMap) {
				if(!countryOriginMap[countrykey])
					countryOriginMap[countrykey]=countryMap[countrykey];
				else
					countryOriginMap[countrykey]=countryMap[countrykey];
			}
			nlapiSetFieldValue('custpage_countrymap', JSON.stringify(countryOriginMap), false);
		}
	}else{
		nlapiSetFieldValue('custpage_countrymap', countryMap, false);
	}
	return false;
}
function setExpirationDateWithMRdate(expirationMap){
	var expiryOriginMap=nlapiGetFieldValue('custpage_expirationmap');
	if(expiryOriginMap)
		expiryOriginMap=(expiryOriginMap.length==0)||(expiryOriginMap.length==1)?'':expiryOriginMap;
	if(expiryOriginMap){
		expiryOriginMap=JSON.parse(expiryOriginMap);
		expirationMap=JSON.parse(expirationMap);
		if(Object.keys(expirationMap).length>0){
			for ( var expirykey in expirationMap) {
				if(!expiryOriginMap[expirykey])
					expiryOriginMap[expirykey]=expirationMap[expirykey];
				else
					expiryOriginMap[expirykey]=expirationMap[expirykey];
			}
			nlapiSetFieldValue('custpage_expirationmap', JSON.stringify(expiryOriginMap), false);
		}
	}else{
		nlapiSetFieldValue('custpage_expirationmap', expirationMap, false);
	}
	return false;
}
function addStockPageInitFunction() {
  debugger;
    var hiddencheckfld = nlapiGetField('custpage_hiddencheckfld');
    if (hiddencheckfld != null && hiddencheckfld != undefined && hiddencheckfld != '') {
        if (window.opener != null && !(window.opener.closed)) {
            var itemlinecount = window.opener.nlapiGetLineItemCount('item');
            //alert(itemlinecount);
            if (itemlinecount == -1) {
                itemlinecount = window.opener.nlapiGetLineItemCount('custpage_itemsublist');
            }
            if (itemlinecount != null && itemlinecount != undefined && itemlinecount != '' && itemlinecount != 0 && itemlinecount != -1) {
                var lotNumberDetailMap = window.opener.nlapiGetCurrentLineItemValue('item', 'custcol_cntm_mapdescription');


                if (!lotNumberDetailMap) {
                    lotNumberDetailMap = window.opener.nlapiGetCurrentLineItemValue('custpage_itemsublist', 'custpage_itemwisepagedata');
                }
                lotNumberDetailMap = (lotNumberDetailMap.length == 1 || lotNumberDetailMap.length == 0) ? "" : lotNumberDetailMap;
                //alert(lotNumberDetailMap);
                if (lotNumberDetailMap != null && lotNumberDetailMap != undefined && lotNumberDetailMap != '') {
                    //alert('Inside map');
                    lotNumberDetailMap = JSON.parse(lotNumberDetailMap);
                    setAddStockLineItemValue(lotNumberDetailMap);
                }
            }
            //var inventoryLOTMapFld=window.opener.nlapiGetField('custpage_lotdetailsmap');
            // if(inventoryLOTMapFld)
            {
                //alert('Inside 888888');
                //pageinit WOC Transaction form
                var inventoryDetailsMap = window.opener.nlapiGetFieldValue('custpage_lotdetailsmap');
                if (!inventoryDetailsMap)
                    inventoryDetailsMap = window.opener.nlapiGetFieldValue('custbody_cntm_inventorylot_mapdetail');
                if (inventoryDetailsMap)
                    inventoryDetailsMap = (inventoryDetailsMap.length == 1 || inventoryDetailsMap.length == 0) ? "" : inventoryDetailsMap;
                // alert(inventoryDetailsMap);
                if (inventoryDetailsMap) {
                    inventoryDetailsMap = JSON.parse(inventoryDetailsMap);
                    setAddStockLineItemValue(inventoryDetailsMap);
                }
            }
        }
    }
}

function setAddStockLineItemValue(lotNumberDetailMap) {
  debugger;
	var item=nlapiGetFieldValue('custpage_itemname');
	var linecounter=1;
	var expirationDetails='';
	 var wocTranFld=nlapiGetField('custpage_wocfld');
     if(wocTranFld){
    	 expirationDetails=findComponentCountryExpirationDate();
		 if(!expirationDetails['expMR'] && !expirationDetails['expStd']){
    		 expirationDetails='';
    	 }
     }
	//alert(expirationDetails);
    for (var lotNumber in lotNumberDetailMap) {
        var lotnumberArray = lotNumber.split('|');
        if (lotnumberArray) {
            checkSelfLifeFlag = 'T';
            var lotArray = lotNumberDetailMap[lotNumber].split('__');
            nlapiSelectNewLineItem('custpage_lotnumberitem');
          // alert(lotArray[4]);
            //code to check the LOT Related transaction 
          /*  var receiptLineCount=window.opener.nlapiGetLineItemCount('item');
            if(receiptLineCount!=-1){
            	//alert(receiptLineCount);
            	var relatedTransation=checkLOTRelatedTransaction(item,lotnumberArray[0]);
            	//alert(relatedTransation);
                if(relatedTransation){
                	//jQuery('#custpage_inventorynumber_'+linecounter).prop('disabled', 'disabled');
                	//jQuery('#custpage_quantity').prop('disabled', 'disabled');
                }
            }*/
            nlapiSetCurrentLineItemValue('custpage_lotnumberitem', 'custpage_inventorynumber', lotnumberArray[0],false)
            nlapiSetCurrentLineItemValue('custpage_lotnumberitem', 'custpage_bin', lotArray[0],false);
            nlapiSetCurrentLineItemValue('custpage_lotnumberitem', 'custpage_status', lotArray[1],false);
            nlapiSetCurrentLineItemValue('custpage_lotnumberitem', 'custpage_quantity', lotArray[2],false);
            nlapiSetCurrentLineItemValue('custpage_lotnumberitem', 'custpage_curedate', lotArray[3],false);
           
            if(expirationDetails){
             	if(expirationDetails['Country'] || expirationDetails['Country']=='')
            		nlapiSetCurrentLineItemText('custpage_lotnumberitem', 'custpage_country', expirationDetails['Country'],false);
            	if(expirationDetails['expMR'])
            		nlapiSetCurrentLineItemValue('custpage_lotnumberitem', 'custpage_expdatemr', expirationDetails['expMR'],false);
            	if(expirationDetails['expStd'])
            		nlapiSetCurrentLineItemValue('custpage_lotnumberitem', 'custpage_expdate', expirationDetails['expStd'],false);
            	
            }else{
             // alert('else->'+lotArray[4]);
            	 nlapiSetCurrentLineItemValue('custpage_lotnumberitem', 'custpage_expdatemr', lotArray[9],false);
                 nlapiSetCurrentLineItemValue('custpage_lotnumberitem', 'custpage_expdate', lotArray[10],false);
                 nlapiSetCurrentLineItemValue('custpage_lotnumberitem', 'custpage_country', lotArray[4],false);  
            
            }
           //nlapiSetCurrentLineItemValue('custpage_lotnumberitem', 'custpage_expdatemr', lotArray[9]);
           //nlapiSetCurrentLineItemValue('custpage_lotnumberitem', 'custpage_expdate', lotArray[10]);
           //nlapiSetCurrentLineItemValue('custpage_lotnumberitem', 'custpage_country', lotArray[4]);
            nlapiSetCurrentLineItemValue('custpage_lotnumberitem', 'custpage_manufacturer', lotArray[5]);
            nlapiSetCurrentLineItemValue('custpage_lotnumberitem', 'custpage_vendor', lotArray[6]);
            nlapiSetCurrentLineItemValue('custpage_lotnumberitem', 'custpage_vendornumber', lotArray[7]);
            nlapiSetCurrentLineItemValue('custpage_lotnumberitem', 'custpage_mfgitemnum', lotArray[8]);
            nlapiSetCurrentLineItemValue('custpage_lotnumberitem', 'custpage_memo', lotArray[11]);
            nlapiCommitLineItem('custpage_lotnumberitem');
            
            checkSelfLifeFlag = 'F';
        }
        
        ++linecounter;
    }
    
}
function findComponentCountryExpirationDate(){
	
	 //find the components country
	var compodetailArray={};
	var assemblyLOTCountry='';
    var checkduplicateCountry={};
    var componentCountryMap=window.opener.nlapiGetFieldValue('custpage_countrymap');
    if(componentCountryMap)
    	componentCountryMap=(componentCountryMap.length==1)||(componentCountryMap.length==0)?'':componentCountryMap;
    if(componentCountryMap){
    	componentCountryMap=JSON.parse(componentCountryMap);
    	for ( var compkey in componentCountryMap) {
    		var itemCountryMap=componentCountryMap[compkey];
    		for ( var itemcontrykey in itemCountryMap) {
    			if(!checkduplicateCountry[itemcontrykey]){
    				checkduplicateCountry[itemcontrykey]=itemCountryMap[itemcontrykey];
    			}
    		}
    	}
    	if(Object.keys(checkduplicateCountry).length==1){
    		for ( var countrykey in checkduplicateCountry) {
    			assemblyLOTCountry=checkduplicateCountry[countrykey];
			}
    	}	
    }
    compodetailArray['Country']=assemblyLOTCountry;
    //get the less expiry date form components
    var expirationDate='';
    var expirationDateMR='';
    var expiryArray=[];
    var checkduplicateExpDate={};
    var expiryMap=window.opener.nlapiGetFieldValue('custpage_expirationmap');
  // alert(expiryMap);
    if(expiryMap)
    	expiryMap=(expiryMap.length==1)||(expiryMap.length==0)?'':expiryMap;
    if(expiryMap){
    //  alert(expiryMap);
    	expiryMap=JSON.parse(expiryMap);
    	for ( var expkey in expiryMap) {
    		var itemExpirationMap=expiryMap[expkey];
          // alert(itemExpirationMap);
    		for ( var itemexpkey in itemExpirationMap) {
    			if(!checkduplicateExpDate[itemexpkey]){
    				checkduplicateExpDate[itemexpkey]=itemExpirationMap[itemexpkey];
    				expiryArray.push(itemexpkey);
                   //alert(itemexpkey);
    			}
    		}
    	}
    	if(expiryArray.length>0){
    		var expiryDateStd=expiryArray[0];
    		for (var arrCount = 1; arrCount < expiryArray.length; ++arrCount) {
				if(new Date(expiryArray[arrCount])<new Date(expiryDateStd)){
					expiryDateStd=expiryArray[arrCount];
				}
			}
    		expirationDateMR=checkduplicateExpDate[expiryDateStd];
			expirationDate=expiryDateStd;
    	}
    	
    
    }
  //alert(expirationDateMR+','+expirationDate);
    compodetailArray['expMR']=expirationDateMR;
    compodetailArray['expStd']=expirationDate;
    return compodetailArray;

}
function checkLOTRelatedTransaction(item,lotNumber){

	var transactionSearch = nlapiSearchRecord("transaction",null,
			[
			   ["type","anyof","ItemShip","SalesOrd"], 
			   "AND", 
			   ["itemnumber.inventorynumber","is",lotNumber], 
			   "AND", 
			   ["itemnumber.item","anyof",item]
			], 
			[
			   new nlobjSearchColumn("internalid"), 
			   new nlobjSearchColumn("type").setSort(false)
			]
			);
	if(!transactionSearch){
		return '';
	}else{
		return transactionSearch.length;
	}

}
function selectLOTPopupPageInit() {
    var checkfld = nlapiGetField('custpage_itemname');

    if (checkfld != null && checkfld != undefined && checkfld != '') {
        if (window.opener != null && !(window.opener.closed)) {

        }
    }
}



function itemReceiptSaveRecord() {
    debugger;
    var lineCnt = nlapiGetLineItemCount('custpage_itemsublist');
    var selectedLineCnt = 0;
    for (var lineIndex = 1; lineIndex <= lineCnt; lineIndex++) {
        var isLineSelected = nlapiGetLineItemValue("custpage_itemsublist", "custpage_checkbox", lineIndex);
        var isInvAdded = nlapiGetLineItemValue("custpage_itemsublist", "custpage_itemwisepagedata", lineIndex);
        isInvAdded = isInvAdded ? isInvAdded.trim() : isInvAdded;
        if (isLineSelected == 'T') {
            if (isInvAdded == undefined || isInvAdded == null || isInvAdded == '') {
                alert("Please configure the inventory details for line " + lineIndex + ".");
                return false;
            } else {
                selectedLineCnt++;
            }
        }
    }
    if (selectedLineCnt == 0 && nlapiGetFieldValue('custpage_createdfrom')) {
        return true;
    } else if (selectedLineCnt == 0) {
        alert('Please select atleast one line to receive.');
        return false;
    } else {
        return true;
    }
}

function wocCustomFormSaverecord() {
    //check LOT created for finished Assembly
    var endingoperation = nlapiGetFieldValue('custpage_endoperation');
    var lastRoutingOperation = nlapiGetFieldValue('custpage_hiddenendop');
    //alert(endingoperation+':'+lastRoutingOperation);
    if (parseInt(endingoperation) == parseInt(lastRoutingOperation)) {
        var lotDetails = nlapiGetFieldValue('custpage_lotdetailsmap');
        //alert(lotDetails);
        if (lotDetails) {
            lotDetails = JSON.parse(lotDetails);
            if (!(Object.keys(lotDetails).length > 0)) {
                alert('Please configure the inventory detail for the assembly item.');
                return false;
            }else{
            	var originalLOTQuantity=nlapiGetFieldValue('custpage_originallotqty');
            	var completedQuantity=nlapiGetFieldValue('custpage_compltqty');
            	if(parseFloat(originalLOTQuantity).toFixed(8)!=parseFloat(completedQuantity).toFixed(8)){
            		alert('Please configure the inventory detail for the assembly item.');
            		return false;
            	}
            }
        } else {
            alert('Please configure the inventory detail for the assembly item.');
            return false;
        }
    }
    //if component items are LOT numbered then check component Item selected LOT
    var compoLineCount = nlapiGetLineItemCount('custpage_componentlist');
    if (compoLineCount != -1 && compoLineCount != 0) {
        for (var compoline = 1; compoline <= compoLineCount; compoline++) {
        	nlapiSelectLineItem('custpage_componentlist', compoline);
            var isLotNumberedItem = nlapiGetCurrentLineItemValue('custpage_componentlist', 'custpage_islotnumbered');
           // alert(isLotNumberedItem);
            if (isLotNumberedItem == 'T') {
               // alert(isLotNumberedItem);
            	var componentQuantity=nlapiGetCurrentLineItemValue('custpage_componentlist', 'custpage_quantity');
                if(componentQuantity>0){
                	var componentLOTDetails = nlapiGetLineItemValue('custpage_componentlist', 'custpage_complotdetails', compoline);
                    //alert(componentLOTDetails)
                	if(componentLOTDetails)
                    	componentLOTDetails=(componentLOTDetails.length==0)||(componentLOTDetails.length==1)?'':componentLOTDetails;
                    if (componentLOTDetails) {
                        componentLOTDetails = JSON.parse(componentLOTDetails);
                        if (!(Object.keys(componentLOTDetails).length > 0)) {
                            alert('Please configure the inventory detail in line ' + compoline+' of the component list');
                            return false;
                        }else{
                        	var originalSelectedLOTQty=nlapiGetCurrentLineItemValue('custpage_componentlist', 'custpage_originalselectedqty');
                        	if(parseFloat(originalSelectedLOTQty).toFixed(8)!=parseFloat(componentQuantity).toFixed(8)){
                        		alert('Please configure the inventory detail in line ' + compoline+' of the component list');
                                return false;
                        	}
                        }
                    } else {
                        alert('Please configure the inventory detail in line ' + compoline+' of the component list');
                        return false;
                    }
                }
            	
            }else if(isLotNumberedItem == 'F' || isLotNumberedItem==''){
            	var routingSequenceMap=getValidOperationSequence();
            	if(routingSequenceMap){
            		//alert(JSON.stringify(routingSequenceMap));
            		var opertionSeq=nlapiGetLineItemValue('custpage_componentlist', 'custpage_operation',compoline);
            		//alert(opertionSeq);
            		if(routingSequenceMap[opertionSeq]){
            			//alert(routingSequenceMap[opertionSeq]);
            			var compoQty=nlapiGetCurrentLineItemValue('custpage_componentlist', 'custpage_quantity');
            			//alert(compoQty);
                		if(!compoQty || compoQty<0){
                			alert('Please enter the quantity for component items(must be at least zero)');
                			return false;
                		}
            		}
            	}
            }
        }
    }
    //alert('Before true');
    return true;
}

function checkComponetOperationSequence(endOp) {
    if (endOp) {
        var completedQty = nlapiGetFieldValue('custpage_compltqty');
        var routingSequenceString = nlapiGetFieldValue('custpage_hiddenroutingseq');
        var routingSequenceArray;
        if (routingSequenceString)
            routingSequenceArray = routingSequenceString.split('|');
        var startRoutOperation = nlapiGetFieldValue('custpage_startoperation');
        if (routingSequenceArray && routingSequenceArray.length > 0) {
            var matchIndex = 0;
            var startOpindex = 0;
            var matchingSequenceArray = {};
            for (var routItr = 0; routItr < routingSequenceArray.length; routItr++) {
                if (routingSequenceArray[routItr] == endOp) {
                    matchIndex = routItr;
                }
                if (routingSequenceArray[routItr] == startRoutOperation) {
                    startOpindex = routItr;
                }
            }
            if (parseInt(startOpindex) == parseInt(matchIndex)) {
                //alert('Inside equal');
                var routingSequence = routingSequenceArray[matchIndex];
                matchingSequenceArray[routingSequence] = routingSequence;
                //alert(routingSequence);
            } else {
                for (var cnt = startOpindex; cnt <= matchIndex; cnt++) {
                    var routingSequence = routingSequenceArray[cnt];
                    matchingSequenceArray[routingSequence] = routingSequence;
                }
            }
            //alert(JSON.stringify(matchingSequenceArray));
            var componentlistLineCount = nlapiGetLineItemCount('custpage_componentlist');
            if (componentlistLineCount != -1) {
                for (var compCounter = 1; compCounter <= componentlistLineCount; compCounter++) {
                    nlapiSelectLineItem('custpage_componentlist', compCounter);
                    var compoItemName=nlapiGetCurrentLineItemValue('custpage_componentlist', 'custpage_component');
                    var operation = nlapiGetLineItemValue('custpage_componentlist', 'custpage_operation', compCounter);
                    if (!matchingSequenceArray[operation]) {
                        nlapiSetLineItemValue('custpage_componentlist', 'custpage_islotnumbered', compCounter, 'F');
                        nlapiSetLineItemValue('custpage_componentlist', 'custpage_quantity', compCounter, '');
                        nlapiSetLineItemValue('custpage_componentlist', 'custpage_complotdetails', compCounter, '');
                        nlapiSetLineItemValue('custpage_componentlist', 'custpage_compo_lotassignment', compCounter, 'F');

                        jQuery('#custpage_quantity' + compCounter + '_formattedValue').prop('disabled', 'disabled');
                        jQuery('#custpage_compo_lotassignment' + compCounter).prop('disabled', 'disabled');

                    } else {
                        if (completedQty) {
                            nlapiSetLineItemValue('custpage_componentlist', 'custpage_quantity', compCounter, completedQty);
                        }
                        var islotNumber=nlapiGetLineItemValue('custpage_componentlist', 'custpage_islotnumbered', compCounter);
                        var compoItemID=nlapiGetLineItemValue('custpage_componentlist', 'custpage_component',compCounter);
                        if(compoItemID){
                        	var fieldArray=[];
                        	fieldArray.push('islotitem');
                        	fieldArray.push('usebins');
                        	var fieldValues=nlapiLookupField('item', compoItemID, fieldArray);
                        	var isUseBinItem=fieldValues['usebins'];
                        	var islotNumbered=fieldValues['islotitem'];
                        	if(islotNumbered=='T' || isUseBinItem=='T'){
                        		nlapiSetLineItemValue('custpage_componentlist', 'custpage_islotnumbered', compCounter, 'T');
                           }
                        }
                        
                        jQuery('#custpage_quantity' + compCounter + '_formattedValue').prop('disabled', false);
                        jQuery('#custpage_compo_lotassignment' + compCounter).prop('disabled', false);
                        
                    }

                }
            }
        }
    }
}

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 *   
 * @param {String} type Sublist internal id
 * @returns {Void}
 */
function clientLineInit(type) {
    if (type == 'custpage_lotnumberitem') {
        var originalLOTQty=nlapiGetCurrentLineItemValue(type, 'custpage_quantity');
        if(originalLOTQty){
        	 /*var originalSelectedQty=nlapiGetFieldValue('custpage_selectedqty');
             if(parseFloat(originalSelectedQty)>0 && parseFloat(originalSelectedQty)>=parseFloat(originalLOTQty)){
             	var selectedQuantity=parseFloat(originalSelectedQty)-parseFloat(originalLOTQty);
             	nlapiSetFieldValue('custpage_selectedqty', parseFloat(selectedQuantity.toFixed(5)), false);     
             }*/
        	nlapiSetFieldValue('custpage_initiallotqty', parseFloat(originalLOTQty).toFixed(5), false);
        }
       
        //alert(originalLOTQty);
    }
}

/**
 * The recordType (internal id) corresponds to the "Applied To" record in your script deployment. 
 * @appliedtorecord recordType
 *   
 * @param {String} type Sublist internal id
 * @returns {Boolean} True to continue line item insert, false to abort insert
 */
function clientValidateInsert(type) {
    /*debugger;
    if (type == 'custpage_lotnumberitem') {
        //add to selected qty
        var quantity = nlapiGetCurrentLineItemValue('custpage_lotnumberitem', 'custpage_quantity')
        var totalSelectedQty = nlapiGetFieldValue('custpage_selectedqty');
        totalSelectedQty = parseFloat(totalSelectedQty) + parseFloat(quantity);
        nlapiSetFieldValue('custpage_selectedqty', parseFloat(totalSelectedQty.toFixed(5)), false);     
        
    }*/
    return false;
}

function refreshItemReceiptCustomPage() {
	//refresh page- Item Receipt
    var hiddenPOId = nlapiGetField('custpage_hiddenpoid');
    if (hiddenPOId != null && hiddenPOId != undefined && hiddenPOId != '') {
        var lineCount = nlapiGetLineItemCount('custpage_itemsublist');
        if (lineCount != -1) {
            var lotDetails;
            var memo = nlapiGetFieldValue('custpage_memo');
            for (var line = 1; line <= lineCount; line++) {
                nlapiSelectLineItem('custpage_itemsublist', line);
                lotDetails = nlapiGetCurrentLineItemValue('custpage_itemsublist', 'custpage_itemwisepagedata');
                lotDetails = (lotDetails.length == 1) || (lotDetails.length == 0) ? '' : lotDetails;
            }
            if (memo || lotDetails) {
                if (window.onbeforeunload) {
                    window.onbeforeunload = function() {
                        null;
                    };
                };
                window.location.reload();
            }
        }
    }
    //refresh Page- Work Order Completion
    var wocTransactionTypeFld= nlapiGetField('custpage_transaction');
    if(wocTransactionTypeFld){
     
    	var compoLineCount=nlapiGetLineItemCount('custpage_componentlist');
      
    	if(compoLineCount != -1){
    		var completedQuantity=nlapiGetFieldValue('custpage_compltqty');
        
    		var compodetails;
            var checkMapdetails;
    		for (var lineno = 1; lineno <= compoLineCount; lineno++) {
                nlapiSelectLineItem('custpage_componentlist', lineno);
                compodetails = nlapiGetCurrentLineItemValue('custpage_componentlist', 'custpage_complotdetails');
             
               if(compodetails){
                 compodetails = (compodetails.length == 1) || (compodetails.length == 0) ? '' : compodetails;
                  if(compodetails)
                 	checkMapdetails=compodetails;
               }
            }
    		if (completedQuantity || checkMapdetails) {
             
                if (window.onbeforeunload) {
                    window.onbeforeunload = function() {
                        null;
                    };
                };
                window.location.reload();
            }
    	}
    }
}

function getValidOperationSequence() {
    var routingSequenceString = nlapiGetFieldValue('custpage_hiddenroutingseq');
    var endOp = nlapiGetFieldValue('custpage_endoperation');
    var routingSequenceArray;
    if (routingSequenceString)
        routingSequenceArray = routingSequenceString.split('|');
    var startRoutOperation = nlapiGetFieldValue('custpage_startoperation');
    var matchingSequenceArray = {};
    if (routingSequenceArray && routingSequenceArray.length > 0) {
        var matchIndex = 0;
        var startOpindex = 0;

        for (var routItr = 0; routItr < routingSequenceArray.length; routItr++) {
            if (routingSequenceArray[routItr] == endOp) {
                matchIndex = routItr;
            }
            if (routingSequenceArray[routItr] == startRoutOperation) {
                startOpindex = routItr;
            }
        }
        if (parseInt(startOpindex) == parseInt(matchIndex)) {
            //alert('Inside equal');
            var routingSequence = routingSequenceArray[matchIndex];
            matchingSequenceArray[routingSequence] = routingSequence;
            //alert(routingSequence);
        } else {
            for (var cnt = startOpindex; cnt <= matchIndex; cnt++) {
                var routingSequence = routingSequenceArray[cnt];
                matchingSequenceArray[routingSequence] = routingSequence;
            }
        }
    }
    return matchingSequenceArray;
}
function checkComponentsLOTInformation(){
	debugger;
	var compoLineCount=nlapiGetLineItemCount('custpage_componentlist');
	if(compoLineCount != -1){
		var compodetails;
		var checkLOTDetailsFlag=true;
		for (var comLineno = 1; comLineno <= compoLineCount; comLineno++) {
            nlapiSelectLineItem('custpage_componentlist', comLineno);
            var lotnumberdItem=nlapiGetCurrentLineItemValue('custpage_componentlist', 'custpage_islotnumbered');
          // alert(lotnumberdItem);
            var componentQty=nlapiGetCurrentLineItemValue('custpage_componentlist', 'custpage_quantity');
            if(lotnumberdItem=='T'){
            	if(componentQty){
            		if(parseFloat(componentQty)>0){
            			compodetails = nlapiGetCurrentLineItemValue('custpage_componentlist', 'custpage_complotdetails');
                        compodetails = (compodetails.length == 1) || (compodetails.length == 0) ? '' : compodetails;
                      //  alert(compodetails);
                        if(compodetails)
                        	checkLOTDetailsFlag=true;
                        else
                        	checkLOTDetailsFlag=false;
            		}
            	}else{
            		checkLOTDetailsFlag=false;
            	}
                //alert('Before='+checkLOTDetailsFlag);
            }else if(lotnumberdItem=='F'){
            	checkLOTDetailsFlag==true;
            }
         }
		//alert('After='+checkLOTDetailsFlag);
		return checkLOTDetailsFlag;
	}
}
function wocStandardFormSave(){
	 var isBackFlush = nlapiGetFieldValue('isbackflush');
     if (isBackFlush == 'T') {
    	 var assemblyItem=nlapiGetFieldValue('item');
    	 if(assemblyItem){
    		 var isLOTNum=nlapiLookupField('item', assemblyItem, 'islotitem');
    		// alert(isLOTNum);
    		 if(isLOTNum=='T' || isLOTNum==true){
    			 var endingoperation = nlapiGetFieldText('endoperation');
    	         var manufact_routid = nlapiGetFieldValue('manufacturingrouting');
    	         if (manufact_routid) {
    	             var manufactroutingrec = nlapiLoadRecord('manufacturingrouting', manufact_routid);
    	             if (manufactroutingrec != null && manufactroutingrec != undefined && manufactroutingrec != '') {
    	                 var rountingcount = manufactroutingrec.getLineItemCount('routingstep');
    	                 var roun_oper = manufactroutingrec.getLineItemValue('routingstep', 'operationsequence', rountingcount);
    	                 if (parseInt(roun_oper) == parseInt(endingoperation)) {
    	                	 var lotDetails = nlapiGetFieldValue('custbody_cntm_inventorylot_mapdetail');
    	                		lotDetails=(lotDetails.length==1)||(lotDetails.length==0)?'':lotDetails;
                          
    	                	    if (lotDetails) {
    	                	        lotDetails = JSON.parse(lotDetails);
    	                	        if (!(Object.keys(lotDetails).length > 0)) {
    	                	            alert('Please configure the inventory detail for the assembly item.');
    	                	            return false;
    	                	        }else{
    	                	        	var originalLOTQuantity=nlapiGetFieldValue('custbody_cntm_original_complte_qty');
    	                	        	var completedQuantity=nlapiGetFieldValue('custbody_cntm_complted_qty');
                                      
    	                	        	if(parseFloat(originalLOTQuantity).toFixed(8)!=parseFloat(completedQuantity).toFixed(8)){
    	                	        		alert('Please configure the inventory detail for the assembly item.');
    	                	        		return false;
    	                	        	}
    	                	        }
    	                	    } else {
    	                	        alert('Please configure the inventory detail for the assembly item.');
    	                	        return false;
    	                	    }
    	                 }
    	             }
    	         }
    		 }
    	 }
     }
    return true;
}

function page_reset(){
localStorage.removeItem('windowOpened');
location.reload();
}
function diableSelectField(lineNumber,FieldNumber,ValueOfField){
	jQuery(jQuery('#custpage_lotnumberitem_row_'+lineNumber).children()[FieldNumber]).html('<div class="listinlinefocusedrowcell" style="-moz-box-sizing: border-box;" focusedrowcell="1">'+ValueOfField+'</div>');
}
function setupWarningForShelfLife(item){
	//custitem_mr_shelflifeyears
	var fieldArray=[];
	fieldArray.push('custitem_mr_shelflifeyears');
	fieldArray.push('itemid');
	var fieldValues=nlapiLookupField('item', item, fieldArray);
	var shelfLifeYrs=fieldValues['custitem_mr_shelflifeyears'];
	var itemName=fieldValues['itemid'];
	if(!shelfLifeYrs){
		alert('\''+itemName+'\' has no value specified for the field \'SHELF LIFE(YEARS)\'');
	}
}
function clientValidateDelete(type) {
    if (type == 'custpage_lotnumberitem') {
        var selectedQty = nlapiGetFieldValue('custpage_selectedqty');
        var lotNumberQuantity = nlapiGetCurrentLineItemValue(type, 'custpage_quantity');
        nlapiSetFieldValue('custpage_initiallotqty', 0, false);
        if (parseFloat(selectedQty) >= parseFloat(lotNumberQuantity)) {
            selectedQty = parseFloat(selectedQty) - parseFloat(lotNumberQuantity);
            if (selectedQty >= 0)
                nlapiSetFieldValue('custpage_selectedqty', parseFloat(selectedQty.toFixed(5)), false);
        }
    }
    return true;
}
function itemReceiptStandardRecordSave(){
	var receiptItemCount=nlapiGetLineItemCount('item');
	if(receiptItemCount!=-1 && receiptItemCount!=0){
		for (var itemLine = 1; itemLine <= receiptItemCount; itemLine++) {
			nlapiSelectLineItem('item', itemLine);
			//var item=nlapiGetCurrentLineItemValue('item', 'item');
			var item=nlapiGetLineItemValue('item', 'item', itemLine);
			if(item){
				var fieldArray=[];
				fieldArray.push('islotitem');
				fieldArray.push('usebins');
				var fieldValues=nlapiLookupField('item', item, fieldArray);
				var lotNumbered=fieldValues['islotitem'];
				var useBinItem=fieldValues['usebins'];
				if(lotNumbered=='T' || useBinItem=='T'){
					var originalItemQty=nlapiGetCurrentLineItemValue('item', 'custcol_cntm_original_item_qty');
					var customQuantity=nlapiGetCurrentLineItemValue('item', 'custcol_cntm_quantity_custom');
					
		          //alert(originalItemQty+'->'+customQuantity);
					if(parseFloat(originalItemQty)!=parseFloat(customQuantity)){
						alert('Please Configure the inventory details for the item on line '+itemLine);
						return false;
					}
				}
			}
			
		}
	}
	return true;
}